/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable @next/next/no-img-element */
/* eslint-disable @next/next/link-passhref */
import TradeName from "@/components/icons/TradeName";
import {
  Checkbox,
  Dialog,
  FormControlLabel,
  FormGroup,
  Grid,
  InputBase,
  Paper,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import AllCategories from "@/components/icons/AllCategiries";
import DownArrowGreen from "@/components/icons/Arrows/DownArrowGreen";
import Search from "@/components/icons/Search";
import { IPayloadProduct } from "@/pages/api/types";
import User from "@/components/icons/User";
import Basket from "@/components/icons/Basket";
import DrawerMenu from "@/components/drawerMenu/DrawerMenu";
import ListIcon from "@material-ui/icons/List";
import Login from "@/components/login/Login";
import DownArrowThinner from "@/components/icons/Arrows/DownArrowThinner";
import RightArrowBlack from "@/components/icons/Arrows/RightArrowBlack";
import Slider from "react-slick";
import RightArrowSlide from "@/components/icons/Arrows/RightArrowSlide";
import LeftArrowSlide from "@/components/icons/Arrows/LeftArrowSlide";
import LeftArrowGreen from "@/components/icons/Arrows/LeftArrowGreen";
import React, { useEffect, useState } from "react";
import SpaceForBlog from "@/components/spaceForBlog";
import BlogPostItem from "@/components/blogPostItem/BlogPostItem";
import useStyles from "./styles";
import PageTitle from "@/components/helper/pageTitle";
import { useDispatch, useSelector } from "react-redux";
import { fetchAllCategory } from "@/store/category/categoryActions";
import { RootState } from "@/store";
import { ICategory } from "@/config/types";
import { fetchProductByCategoryId } from "@/store/product/productActions";
import SortNav from "@/components/sortNav/SortNav";
import UncheckIcon from "@/components/icons/UnCheckIcon";
import UnCheckboxIcon from "@/components/icons/UnCheckboxIcon";
import CheckboxIcon from "@/components/icons/CheckboxIcon";
import DownArrowBlack from "@/components/icons/Arrows/DownArrowBlack";
import Link from "next/link";
import CheckIcon from "@/components/icons/CheckIcon";
import { useRouter } from "next/router";
import { fetchUserProfile } from "@/store/user/userActions";

interface ILi {
  name: string;
  href: string;
}
interface Data {
  h1: string;
  li: ILi[];
}

interface IProps {
  isNavBar: boolean | true;
  titleLink: string;
  hideNavBar: boolean | false;
}

const Layout: React.FC<IProps> = ({
  children,
  isNavBar,
  titleLink,
  hideNavBar,
}) => {
  const classes = useStyles();
  const router = useRouter();
  const { categoryList } = useSelector((state: RootState) => state.category);
  const { userProfile } = useSelector((state: RootState) => state.user);
  const [open, setOpen] = useState(false);
  const [inputValue, setInputValue] = useState<string | undefined>();
  const [isShowPopUp, setIsShowPopUp] = useState<boolean>(false);
  const [checkbox1, setCheckbox1] = useState<boolean>(false);
  const [checkbox2, setCheckbox2] = useState<boolean>(true);
  const [checkbox3, setCheckbox3] = useState<boolean>(true);
  const [isShowProfile, setIsShowProfile] = useState<boolean>(false);
  const [token, setToken] = useState<string | undefined>();
  const dispatch = useDispatch();

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1530,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 816,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const handleChangeInput = (e: any) => {
    setInputValue(e.target.value);
    setIsShowPopUp(true);
  };

  const onHandleClickItem = (val: IPayloadProduct) => {
    setInputValue(val.productTitle);
    setIsShowPopUp(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleFetchProductByCategoryId = (categoryId: number) => {
    dispatch(fetchProductByCategoryId({ params: categoryId }));
  };

  useEffect(() => {
    Promise.all([
      dispatch(fetchAllCategory({})),
      dispatch(fetchUserProfile({})),
    ]);
  }, [dispatch]);

  useEffect(() => {
    if (router.asPath && router.route) {
      setInputValue(router.query.q as string);
    }
  }, [router]);

  const showPopUp = (value: Array<IPayloadProduct>) => {
    if (value.length !== 0) {
      return (
        <div className={classes.popupProducts}>
          {value.map((item: IPayloadProduct, idex) => {
            return (
              <div
                key={idex}
                className={classes.itemPopup}
                onClick={() =>
                  onHandleClickItem(item as unknown as IPayloadProduct)
                }
              >
                {item.productTitle}
              </div>
            );
          })}
        </div>
      );
    } else {
      return null;
    }
  };
  const onShowProfile = () => {
    setIsShowProfile(true);
  };
  const onHideProfile = () => {
    setIsShowProfile(false);
  };

  useEffect(() => {
    setToken(window.localStorage.getItem("accessToken") as string);
  }, []);

  const handleSignout = () => {
    window.localStorage.clear();
    router.push("/sign-in");
  };

  const userPlace = () => {
    return (
      <div className={classes.tradeNameIcon}>
        {token ? (
          <>
            <div
              className={classes.profileBlock}
              onMouseOver={onShowProfile}
              onMouseLeave={onHideProfile}
            >
              <img
                src={userProfile.avatar}
                alt="avatar"
                style={{
                  width: 24,
                  height: 24,
                  borderRadius: "50%",
                }}
              />
              {isShowProfile && (
                <div>
                  <a href="/my-profile">My profile</a>
                  <p onClick={handleSignout}>Sign out</p>
                </div>
              )}
            </div>
          </>
        ) : (
          <a href="/sign-in">
            <User />
          </a>
        )}

        <a href="/check-out">
          <Basket />
        </a>
      </div>
    );
  };

  const contentDrawer = () => {
    return (
      <ul className={classes.catalogMenuResponsive}>
        {categoryList &&
          categoryList.map((item: ICategory) => {
            return (
              <li key={item.id}>
                {item.category_name} <DownArrowThinner />
              </li>
            );
          })}
      </ul>
    );
  };

  const listCatalog = () => {
    return (
      <div className={classes.catalog}>
        <ul className={classes.catalogMenu}>
          {categoryList &&
            categoryList.map((item: ICategory) => {
              return (
                <li key={item.id}>
                  <a href={`/list-product/${item.id}`}>
                    {item.category_name} <DownArrowThinner />
                  </a>
                </li>
              );
            })}
        </ul>
      </div>
    );
  };

  const catalogGridView = (value: Data) => {
    return (
      <div>
        <h1>{value.h1}</h1>
        <ul>
          {value.li.map((item: ILi, index: number) => {
            return (
              <li key={index}>
                <a href={item.href as string}>{item.name}</a>
              </li>
            );
          })}
        </ul>
        <button>
          More categories
          <RightArrowBlack />
        </button>
      </div>
    );
  };

  const feedBack = () => {
    return (
      <div className={classes.customerFeedback}>
        <p>
          “ This is an super space for your customers qoute. Don’t worry it
          works smooth as pie. You will get all what you need by writiing a text
          here “
        </p>
        <p>Name and Surname</p>
        <img src="/ellipse.png" alt="avatar" />
      </div>
    );
  };

  const renderTitle = (h2: string, button: string) => {
    return (
      <div className={classes.titleSlide}>
        <h2>{h2}</h2>
        <button>
          {button}
          <LeftArrowGreen />
        </button>
      </div>
    );
  };

  const renderSlide = () => {
    return (
      <div className={classes.wrapperFeedback}>
        {renderTitle("Our customers says", "Button")}
        <Slider {...settings} className={classes.customSlider}>
          {feedBack()}
          {feedBack()}
          {feedBack()}
          {feedBack()}
          {feedBack()}
        </Slider>
      </div>
    );
  };

  const listBlog = () => {
    return (
      <div className={classes.listBlog}>
        <div>
          <h2>Salat is kinda good start to your morning routines</h2>
          <div>
            <span>Author</span>
            <span>15. 6. 2020</span>
          </div>
        </div>
        <img src={"/Salat.png"} alt="" />
      </div>
    );
  };

  const readOutBlog = () => {
    return (
      <div className={classes.readOurBlog}>
        {renderTitle("Read our Blog posts", "Go to Blog")}
        <div className={classes.wrapperBlog}>
          <SpaceForBlog
            mainblog={"/MainBlog.png"}
            tags="Dinner tips"
            blogheadline="Our chef tips for a great and tasty dinner ready in 20 minutes"
            avatarauthor={"/Ellipse_3.png"}
            date="17. 6. 2020"
          />
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
          <div className={classes.wrapperBlogList}>
            {listBlog()}
            {listBlog()}
            {listBlog()}
          </div>
        </div>
      </div>
    );
  };

  const renderFooter = () => {
    return (
      <>
        <div className={classes.aboutShop}>
          {aboutShop.map((item: any, ind: number) => {
            return (
              <div key={ind}>
                <h1>{item.h1}</h1>
                {item.items.map((i: any, id: number) => {
                  return <p key={id}>{i}</p>;
                })}
              </div>
            );
          })}
        </div>
        <div className={classes.productTags}>
          <h1>Product Tags</h1>
          <div>
            {productTags.map((item, index) => {
              return <span key={index}>{item}</span>;
            })}
          </div>
        </div>
        <p className={classes.copyright}>Copyright © 2020 petrbilek.com</p>
      </>
    );
  };

  const StyledRadio = (props: any) => {
    return (
      <Radio
        disableRipple
        color="default"
        checkedIcon={<CheckIcon />}
        icon={<UncheckIcon />}
        {...props}
      />
    );
  };

  const handleChange = (state: boolean, func: Function) => {
    func(!state);
  };

  const formControlLable = (state: boolean, func: Function, text: string) => {
    return (
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              icon={<UnCheckboxIcon />}
              checkedIcon={<CheckboxIcon />}
              checked={state}
              onChange={() => handleChange(state, func)}
            />
          }
          label={text}
        />
      </FormGroup>
    );
  };

  const renderCheckboxOption = (
    state: boolean,
    func: Function,
    text: string
  ) => {
    return (
      <div className={classes["nbm-div"]}>
        {formControlLable(state, func, text)}
        <span>Nbm</span>
      </div>
    );
  };

  const renderCheckboxOptionSelect = () => {
    return (
      <div className={classes["option-checkbox-select"]}>
        <div>
          {formControlLable(checkbox3, setCheckbox3, "Filtre")}
          <span>12</span>dispatch
        </div>
        <div className={classes["select-option"]}>
          Select <DownArrowBlack />
        </div>
      </div>
    );
  };

  const renderSelectOption = () => {
    return (
      <div className={classes.wrapSelectOption}>
        <Grid container item xs={12}>
          <Grid item xs={12} className={classes["wrapper-option-div"]}>
            {renderCheckboxOption(checkbox1, setCheckbox1, "Filtre")}
            {renderCheckboxOption(checkbox2, setCheckbox2, "Filtre")}
            {renderCheckboxOptionSelect()}
          </Grid>
        </Grid>
      </div>
    );
  };

  const handleSearchProduct = (productName: string | undefined) => {
    if (!productName) {
      return;
    }
    router.push(`/search-product/?q=${productName}`);
  };

  return (
    <>
      <div className={classes.shopInfor}>
        <div>
          <p>Chat with us</p>
          <p>+420 336 775 664</p>
          <p>info@freshnesecom.com</p>
        </div>
        <div>
          <p>Blog</p>
          <p>About Us</p>
          <p>Careers</p>
        </div>
      </div>

      <div className={classes.shopTradeName}>
        <a href="/">
          <TradeName />
        </a>
        <div className={classes.containAutoSeach}>
          <Paper component="form" className={classes.input}>
            <div className={classes.customCategoriesInput}>
              <AllCategories />
              <DownArrowGreen />
            </div>
            <InputBase
              placeholder="Search Products, categories ..."
              inputProps={{ "aria-label": "search products" }}
              className={classes.inputBase}
              onChange={(e) => handleChangeInput(e)}
              value={inputValue}
            />
            <div
              className={classes.customSeachIcon}
              onClick={() => handleSearchProduct(inputValue)}
            >
              <Search />
            </div>
          </Paper>
          {/* {isShowPopUp ? showPopUp(filterProduct) : null} */}
        </div>
        <div className={classes.customUsePlace}>{userPlace()}</div>
      </div>

      <div className={classes.listCatalogIcon}>
        <div className={classes.listCatalogContainer}>
          <DrawerMenu
            listIcon={<ListIcon />}
            drawerType="left"
            content={contentDrawer}
          />
          {userPlace()}
        </div>
      </div>
      {listCatalog()}
      <PageTitle page={["HomePage", titleLink]} />
      <div className={classes.root}>
        <Grid
          container
          spacing={2}
          className={`${classes.wrapperMenu} ${classes.customWrapperMenu}`}
        >
          {hideNavBar ? (
            isNavBar ? (
              <Grid item xs={3} className={classes.menu}>
                {catalogGridView(bestSelling)}
                {catalogGridView(categories)}
                {catalogGridView(bestFromFarmer)}
              </Grid>
            ) : (
              <SortNav />
            )
          ) : null}

          {children}
        </Grid>
      </div>
      {/* {renderSlide()} */}
      {/* {readOutBlog()} */}
      {renderFooter()}
    </>
  );
};

function SimpleDialog(props: any) {
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <Login />
    </Dialog>
  );
}

const categories: Data = {
  h1: "Category menu",
  li: [
    {
      name: "Berries",
      href: "/list-product/1",
    },
    { name: "Pits", href: "/list-product/2" },
    { name: "Cores", href: "/list-product/3" },
    { name: "Melons", href: "/list-product/4" },
    { name: "Tropical Fruits", href: "/list-product/5" },
  ],
};
const bestSelling: Data = {
  h1: "Best selling products",
  li: [
    {
      name: "Berries",
      href: "/list-product/1",
    },
    { name: "Pits", href: "/list-product/2" },
    { name: "Cores", href: "/list-product/3" },
    { name: "Melons", href: "/list-product/4" },
    { name: "Tropical Fruits", href: "/list-product/5" },
  ],
};
const bestFromFarmer: Data = {
  h1: "Best from Farmers",
  li: [
    {
      name: "Berries",
      href: "/list-product/1",
    },
    { name: "Pits", href: "/list-product/2" },
    { name: "Cores", href: "/list-product/3" },
    { name: "Melons", href: "/list-product/4" },
    { name: "Tropical Fruits", href: "/list-product/5" },
  ],
};

const aboutShop = [
  {
    h1: "Get in touch",
    items: ["About Us", "Careers", "Press Releases", "Blog"],
  },
  {
    h1: "Connections",
    items: ["Facebook", "Twitter", "Instagram", "YouTube", "LinkedIn"],
  },
  {
    h1: "Earnings",
    items: ["Become an Affiliate", "Advertise your product", "Sell on Market"],
  },
  {
    h1: "Account",
    items: [
      "Your account",
      "Returns Centre",
      "100 % purchase protection",
      "Chat with us",
      "Help",
    ],
  },
];

const productTags = [
  "Beans",
  "Carrots",
  "Apples",
  "Garlic",
  "Mushrooms",
  "Tomatoes",
  "Chili Peppers",
  "Broccoli",
  "Watermelons",
  "Oranges",
  "Bananas",
  "Grapes",
  "Cherries",
  "Meat",
  "Seo Tags",
  "Fish",
  "Seo tag",
  "Fresh food",
  "Lemons",
];

function SampleNextArrow(props: any) {
  const { onClick, className } = props;
  return (
    <div className={className} onClick={onClick} style={{ right: 0 }}>
      <RightArrowSlide />
    </div>
  );
}

function SamplePrevArrow(props: any) {
  const { onClick, className } = props;
  return (
    <div
      className={className}
      onClick={onClick}
      style={{ left: 0, zIndex: 10 }}
    >
      <LeftArrowSlide />
    </div>
  );
}

export default Layout;
