export const BASE_URL: string = "http://localhost:3201/";

export const USER: string = "users";
export const CATEGORY: string = "categories";
export const CART: string = "cart";
export const PRODUCTS: string = "products";
export const DESCRIPTION: string = "description";
export const REVIEW: string = "reviews";
export const BUYTYPE: string = "buy-types";
export const COUNTRIES: string = "countries";
export const ORDER: string = "orders";
