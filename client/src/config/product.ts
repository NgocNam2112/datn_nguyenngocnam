import instance from "./axios.config";
import { PRODUCTS } from "./config.constant";

export const fetchAllProduct: () => Promise<any> = async () => {
  try {
    const result = await instance("", undefined).get(PRODUCTS);
    const { data } = result;
    return data;
  } catch (error) {
    handleError(error);
  }
};
