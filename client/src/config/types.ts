export interface IResponseAPI<T> {
  success?: boolean;
  // thanh cong
  data?: T;
  // that bai
  message?: string;
  status?: number | string;
}

export interface ValidationErrors {
  status?: number;
  errorMessage: string;
}

export interface ICategory {
  id: number;
  category_name: string;
}

export interface IProduct {
  id: number;
  image: string;
  selling_price: number;
  content: string;
  discount: number;
  space_for_discription: string;
  name: string;
  stock: number;
  farm: string;
  delivery_area: string;
  categoryId: number;
  rate: number;
}

export interface IDescription {
  id: number;
  origin: string;
  cook: string;
  vitamins: string;
  first_image: string;
  second_image: string;
  productId: number;
}

export interface IReview {
  id: number;
  content: string;
  user: {
    id: number;
    avatar: string;
    first_name: string;
    last_name: string;
  };
  product: {
    id: number;
  };
  updated_at: string;
}

export interface IBuyType {
  id: number;
  unit_type: string;
  created_at: string;
  updated_at: string;
}

export interface ICart {
  id: number;
  quantity: number;
  created_at: string;
  updated_at: string;
  userId: number;
  buyTypeId: number;
  product: {
    id: number;
    image: string;
    selling_price: number;
    content: string;
    discount: number;
    space_for_discription: string;
    name: string;
    stock: number;
    farm: string;
    delivery_area: string;
    categoryId: number;
    rate: number;
    cloudinary_id: string | null;
  };
}

export interface ICountry {
  id: number;
  country_name: string;
  created_at: string;
  upadted_at: string;
}

export interface IUser {
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
  avatar: string;
  rolesId: number;
  isProfile: boolean;
}

export interface IOderDetail {
  id: number;
  quantity: number;
  buyTypesId: number;
  product: {
    id: number;
    image: string;
    selling_price: number;
    discount: number;
    name: string;
  };
}

export interface IOrder {
  id: number;
  created_at: "";
  order_details: IOderDetail[];
}
