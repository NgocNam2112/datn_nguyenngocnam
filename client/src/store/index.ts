import { buyTypeReducer } from "./buyType/buyTypeSlice";
import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import { combineReducers } from "redux";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistReducer,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE,
} from "redux-persist";
import { apiErrorReducer } from "./apiError/apiErrorSlice";
import { cartReducer } from "./cart/cartSlice";
import { categoryReducer } from "./category/categorySlice";
import { descriptionReducer } from "./description/descriptionSlice";
import { productReducer } from "./product/productSlice";
import { reviewReducer } from "./review/reviewSlice";
import { userReducer } from "./user/userSlice";
import { orderReducer } from "./order/orderSlice";

const store = configureStore({
  reducer: {
    buyType: buyTypeReducer,
    cart: cartReducer,
    category: categoryReducer,
    description: descriptionReducer,
    product: productReducer,
    review: reviewReducer,
    order: orderReducer,
    user: userReducer,
    apiError: apiErrorReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

const makeStore = () => store;
export type AppStore = ReturnType<typeof makeStore>;
export const wrapper = createWrapper<AppStore>(makeStore);
export type RootState = ReturnType<typeof store.getState>;

export default store;
