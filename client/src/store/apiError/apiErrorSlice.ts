import { Dispatch } from "redux";

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AsyncThunkPayloadCreatorReturnValue } from "@reduxjs/toolkit/dist/createAsyncThunk";
import FailedToCallApiError from "@/config/FailToCallApiError";
import { redirectThenClientError } from "@/config/redirectThenClientError";

const initialState: FailedToCallApiError = {
  name: "",
  message: "",
  messages: [],
};

export const apiErrorSlice = createSlice({
  name: "apiError",
  initialState,
  reducers: {
    setApiError: (state, action: PayloadAction<FailedToCallApiError>) => {
      state.message = action.payload.message;
      state.statusCode = action.payload.statusCode;
      state.isError = action.payload.isError;
      state.reason = action.payload.reason;
      state.messages = action.payload.messages;
    },
  },
});

export const { setApiError } = apiErrorSlice.actions;
export const apiErrorReducer = apiErrorSlice.reducer;

export const clearApiError = (dispatch: Dispatch): (() => void) => {
  return () => dispatch(setApiError(initialState));
};

export interface ValidationErrors {
  status?: number;
  errorMessage: string;
}

export const defaultCallApiErrorHandler = (
  err: unknown,
  thunkAPI: {
    dispatch: Dispatch;
    rejectWithValue: (value: ValidationErrors) => any;
  },
  errorMessage = "An unexpected error has occurred"
): AsyncThunkPayloadCreatorReturnValue<any, any> => {
  if (err instanceof FailedToCallApiError) {
    thunkAPI.dispatch(
      setApiError({
        name: err.name,
        message: err.message,
        statusCode: err.statusCode,
        isError: err.isError,
        reason: err.reason,
        messages: err.messages,
      })
    );
    redirectThenClientError(err);
    return thunkAPI.rejectWithValue({ errorMessage: err.message });
  }
  return thunkAPI.rejectWithValue({
    status: -1,
    errorMessage,
  });
};
