import { createAsyncThunk } from "@reduxjs/toolkit";
import instance from "@/config/axios.config";
import { PRODUCTS } from "@/config/config.constant";
import { IProduct, IResponseAPI } from "@/config/types";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "@/store/apiError/apiErrorSlice";

export const fetchAllProduct = createAsyncThunk<
  IProduct[],
  {},
  { rejectValue: ValidationErrors }
>("product/fetchAllProduct", async ({}, { rejectWithValue, dispatch }) => {
  clearApiError(dispatch)();
  try {
    const result = await instance("", undefined).get(PRODUCTS);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const fetchProductByCategoryId = createAsyncThunk<
  IProduct[],
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "product/fetchProductByCategoryId",
  async ({ params }, { rejectWithValue, dispatch }) => {
    try {
      const result = await instance("", undefined).get(
        `${PRODUCTS}/by-category/${params}`
      );
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const fetchProductById = createAsyncThunk<
  IProduct,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "product/fetchProductById",
  async ({ params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const { data } = await instance("", undefined).get(
        `${PRODUCTS}/${params}`
      );
      return data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const searchProduct = createAsyncThunk<
  IProduct[],
  string,
  { rejectValue: ValidationErrors }
>(
  "product/searchProduct",
  async (productName, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const { data } = await instance("", undefined).get(
        `${PRODUCTS}/search/${productName}`
      );
      return data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
