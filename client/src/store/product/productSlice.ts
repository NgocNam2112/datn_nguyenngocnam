import { IProduct } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import {
  fetchAllProduct,
  fetchProductByCategoryId,
  fetchProductById,
  searchProduct,
} from "./productActions";

interface IProductList {
  productList: IProduct[] | IProduct;
  productListByCategoryId: IProduct[];
  error?: string;
}

const initialState: IProductList = {
  productList: [
    {
      id: 0,
      image: "",
      selling_price: 0,
      discount: 0,
      space_for_discription: "",
      name: "",
      stock: 0,
      farm: "",
      content: "",
      delivery_area: "",
      categoryId: 0,
      rate: 0,
    },
  ],
  productListByCategoryId: [
    {
      id: 0,
      image: "",
      selling_price: 0,
      discount: 0,
      space_for_discription: "",
      name: "",
      stock: 0,
      farm: "",
      delivery_area: "",
      categoryId: 0,
      content: "",
      rate: 0,
    },
  ],
  error: "",
};

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAllProduct.fulfilled, (state, action) => {
      state.productList = action.payload;
    });
    builder.addCase(fetchAllProduct.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
    builder.addCase(fetchProductByCategoryId.fulfilled, (state, action) => {
      state.productListByCategoryId = action.payload;
    });

    builder.addCase(fetchProductByCategoryId.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
    builder.addCase(fetchProductById.fulfilled, (state, action) => {
      state.productList = action.payload;
    });
    builder.addCase(fetchProductById.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(searchProduct.fulfilled, (state, action) => {
      state.productList = action.payload;
    });
    builder.addCase(searchProduct.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const productReducer = productSlice.reducer;
