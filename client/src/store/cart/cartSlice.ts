import { ICart } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import { addProductToCart, fetchAllProductInCart } from "./cartActions";

interface ICartAddition {
  id: number;
  userId: number;
  productId: number;
  buyTypeId: number;
  created_at: "";
  updated_at: "";
}

interface ICartList {
  cartList: ICart[];
  error?: string;
  isOpenCartDialogSuccess: boolean;
  isOpenCartDialogFail: boolean;
  responseProductAddition: ICartAddition;
}

const initialState: ICartList = {
  cartList: [
    {
      id: 0,
      quantity: 0,
      created_at: "",
      updated_at: "",
      userId: 0,
      buyTypeId: 0,
      product: {
        id: 0,
        image: "",
        selling_price: 0,
        content: "",
        discount: 0,
        space_for_discription: "",
        name: "",
        stock: 0,
        farm: "",
        delivery_area: "",
        categoryId: 0,
        rate: 0,
        cloudinary_id: "",
      },
    },
  ],
  responseProductAddition: {
    id: 0,
    userId: 0,
    productId: 0,
    buyTypeId: 0,
    created_at: "",
    updated_at: "",
  },
  error: "",
  isOpenCartDialogSuccess: false,
  isOpenCartDialogFail: false,
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    openCartDialog: (state, action) => {
      state.isOpenCartDialogSuccess = true;
      state.isOpenCartDialogFail = false;
    },
    closeCartDialog: (state, action) => {
      state.isOpenCartDialogSuccess = false;
    },
    openAuthDialogFail: (state, action) => {
      state.isOpenCartDialogFail = true;
      state.isOpenCartDialogSuccess = false;
    },
    closeAuthDialogFail: (state, action) => {
      state.isOpenCartDialogFail = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(addProductToCart.fulfilled, (state, action) => {
      state.responseProductAddition = action.payload;
    });

    builder.addCase(addProductToCart.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
    builder.addCase(fetchAllProductInCart.fulfilled, (state, action) => {
      state.cartList = action.payload;
    });

    builder.addCase(fetchAllProductInCart.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const {
  openCartDialog,
  closeCartDialog,
  openAuthDialogFail,
  closeAuthDialogFail,
} = cartSlice.actions;

export const cartReducer = cartSlice.reducer;
