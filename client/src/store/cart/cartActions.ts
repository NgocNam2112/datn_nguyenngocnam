import {
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "./../apiError/apiErrorSlice";
import { ICart } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import instance from "@/config/axios.config";
import { CART } from "@/config/config.constant";
import { openAuthDialogFail, openCartDialog } from "./cartSlice";

export interface IProductInfo {
  productId: number;
  buyTypeId: number;
  quantity: number;
}

export const addProductToCart = createAsyncThunk<
  any,
  {
    productId: number;
    buyTypeId: number;
    quantity: number;
  },
  { rejectValue: ValidationErrors }
>("cart/addProductToCart", async (product, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).post(
      `${CART}/add-to-cart`,
      { ...product }
    );
    dispatch(openCartDialog(true));
    return result.data;
  } catch (error: any) {
    if (error.code === "ERR_BAD_REQUEST") {
      console.log("error", error);
      dispatch(openAuthDialogFail(true));
    }
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const fetchAllProductInCart = createAsyncThunk<
  ICart[],
  {},
  { rejectValue: ValidationErrors }
>("cart/fetchAllProductInCart", async ({}, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(CART);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const removeProductInCart = createAsyncThunk<
  any,
  number,
  { rejectValue: ValidationErrors }
>("cart/removeProductInCart", async (params, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    await instance(token, undefined).delete(
      `${CART}/delete-cart-item/${params}`
    );
    dispatch(fetchAllProductInCart({}));
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});
