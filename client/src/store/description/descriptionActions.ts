import instance from "@/config/axios.config";
import { IDescription } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "@/store/apiError/apiErrorSlice";
import { DESCRIPTION } from "@/config/config.constant";

export const fetchDescriptionByProductId = createAsyncThunk<
  IDescription,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "description/fetchDescriptionByProductId",
  async ({ params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const result = await instance("", undefined).get(
        `${DESCRIPTION}/${params}`
      );
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
