import { IDescription } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import { fetchDescriptionByProductId } from "./descriptionActions";

interface IDescriptionList {
  descriptionByProductId: IDescription;
  error?: string;
}

const initialState: IDescriptionList = {
  descriptionByProductId: {
    id: 0,
    origin: "",
    cook: "",
    vitamins: "",
    first_image: "",
    second_image: "",
    productId: 0,
  },
  error: "",
};

export const descriptionSlice = createSlice({
  name: "description",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchDescriptionByProductId.fulfilled, (state, action) => {
      state.descriptionByProductId = action.payload;
    });
    builder.addCase(fetchDescriptionByProductId.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const descriptionReducer = descriptionSlice.reducer;
