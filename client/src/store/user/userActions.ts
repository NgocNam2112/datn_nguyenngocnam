import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "./../apiError/apiErrorSlice";
import { IUser } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import instance from "@/config/axios.config";
import { BASE_URL, USER } from "@/config/config.constant";
import axios from "axios";
import { openCartDialog } from "../cart/cartSlice";

export interface IInputUser {
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
  files?: any;
}

export const fetchUserProfile = createAsyncThunk<
  IUser,
  {},
  { rejectValue: ValidationErrors }
>("user/fetchUserProfile", async ({}, { rejectWithValue, dispatch }) => {
  clearApiError(dispatch)();
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(`${USER}/profile`);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const updateProfile = createAsyncThunk<
  IUser,
  { userProfile: IInputUser },
  { rejectValue: ValidationErrors }
>(
  "user/updateProfile",
  async ({ userProfile }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    const form = new FormData();
    form.append("first_name", userProfile.first_name);
    form.append("last_name", userProfile.last_name);
    form.append("phone_number", userProfile.phone_number);
    form.append("address", userProfile.address);
    form.append("town", userProfile.town);
    try {
      if (userProfile.files) {
        form.append("files", userProfile.files);
        const result = await axios.put(
          `${BASE_URL}users/update-profile-with-image`,
          form,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `Bearer ${token}`,
            },
          }
        );
        dispatch(fetchUserProfile({}));
        dispatch(openCartDialog(true));
        return result.data;
      }

      const result = await instance(token, undefined).put(
        `${USER}/update-profile-without-image`,
        form
      );
      if (result.status === 200) {
        dispatch(fetchUserProfile({}));
        dispatch(openCartDialog(true));
      }
      return result.data;
    } catch (error) {
      console.log("error", error);
    }
  }
);
