import { IUser } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import { fetchUserProfile } from "./userActions";

interface IUserList {
  userProfile: IUser;
  error?: string;
}

const initialState: IUserList = {
  userProfile: {
    first_name: "",
    last_name: "",
    phone_number: "",
    address: "",
    town: "",
    avatar: "",
    rolesId: 0,
    isProfile: false,
  },
  error: "",
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchUserProfile.fulfilled, (state, action) => {
      state.userProfile = action.payload;
    });

    builder.addCase(fetchUserProfile.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const userReducer = userSlice.reducer;
