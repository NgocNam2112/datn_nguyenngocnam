import { IOrder } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import { createNewOrder, fetchOrderById } from "./orderActions";

interface IOrderList {
  orderList: IOrder;
  orderId: number;
  error?: string;
}

const initialState: IOrderList = {
  orderList: {
    id: 0,
    created_at: "",
    order_details: [
      {
        id: 0,
        quantity: 0,
        buyTypesId: 0,
        product: { id: 0, image: "", selling_price: 0, discount: 0, name: "" },
      },
    ],
  },
  orderId: 0,
  error: "",
};

export const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchOrderById.fulfilled, (state, action) => {
      state.orderList = action.payload;
    });
    builder.addCase(fetchOrderById.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
    builder.addCase(createNewOrder.fulfilled, (state, action) => {
      state.orderId = action.payload;
    });

    builder.addCase(createNewOrder.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const orderReducer = orderSlice.reducer;
