import instance from "@/config/axios.config";
import { ORDER } from "@/config/config.constant";
import { IOrder, ValidationErrors } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { defaultCallApiErrorHandler } from "../apiError/apiErrorSlice";

export const fetchOrderById = createAsyncThunk<
  IOrder,
  { params: number },
  { rejectValue: ValidationErrors }
>("order/fetchOrderById", async ({ params }, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(
      `${ORDER}/fetch-newest-order/${+params}`
    );
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const createNewOrder = createAsyncThunk<
  any,
  {},
  { rejectValue: ValidationErrors }
>("order/createNewOrder", async ({}, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(
      `${ORDER}/add-new-order`
    );
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});
