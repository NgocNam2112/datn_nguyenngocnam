import { createSlice } from "@reduxjs/toolkit";
import { ICategory } from "./../../config/types";
import { fetchAllCategory, fetchCategoryById } from "./categoryActions";

interface ICategoryList {
  categoryList: ICategory[];
  categoryById: ICategory;
  error?: string;
}

const initialState: ICategoryList = {
  categoryList: [
    {
      id: 0,
      category_name: "",
    },
  ],
  categoryById: {
    id: 0,
    category_name: "",
  },
  error: "",
};

export const categorySlice = createSlice({
  name: "category",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAllCategory.fulfilled, (state, action) => {
      state.categoryList = action.payload;
    });
    builder.addCase(fetchAllCategory.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(fetchCategoryById.fulfilled, (state, action) => {
      state.categoryById = action.payload;
    });
    builder.addCase(fetchCategoryById.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const categoryReducer = categorySlice.reducer;
