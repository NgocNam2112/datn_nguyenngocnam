import { CATEGORY } from "./../../config/config.constant";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "@/store/apiError/apiErrorSlice";
import { ICategory } from "./../../config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import instance from "@/config/axios.config";

export const fetchAllCategory = createAsyncThunk<
  ICategory[],
  {},
  { rejectValue: ValidationErrors }
>("category/fetchAllCategory", async ({}, { rejectWithValue, dispatch }) => {
  clearApiError(dispatch)();
  try {
    const result = await instance("", undefined).get(CATEGORY);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const fetchCategoryById = createAsyncThunk<
  ICategory,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "category/fetchCategoryById",
  async ({ params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const result = await instance("", undefined).get(`${CATEGORY}/${params}`);
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
