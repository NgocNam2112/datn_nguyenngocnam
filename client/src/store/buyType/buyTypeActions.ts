import { createAsyncThunk } from "@reduxjs/toolkit";
import { IBuyType } from "./../../config/types";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "@/store/apiError/apiErrorSlice";
import { BUYTYPE } from "@/config/config.constant";
import instance from "@/config/axios.config";

export const fetchAllBuyType = createAsyncThunk<
  IBuyType[],
  {},
  { rejectValue: ValidationErrors }
>("buyType/fetchAllBuyType", async ({}, { rejectWithValue, dispatch }) => {
  clearApiError(dispatch)();
  try {
    const result = await instance("", undefined).get(BUYTYPE);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});
