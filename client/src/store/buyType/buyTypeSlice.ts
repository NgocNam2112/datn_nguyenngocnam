import { IBuyType } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import { fetchAllBuyType } from "./buyTypeActions";

interface IBuyTypeList {
  buyTypeList: IBuyType[];
  error?: string;
}

const initialState: IBuyTypeList = {
  buyTypeList: [
    {
      id: 0,
      unit_type: "",
      created_at: "",
      updated_at: "",
    },
  ],
  error: "",
};

export const buyTypeSlice = createSlice({
  name: "buyType",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAllBuyType.fulfilled, (state, action) => {
      state.buyTypeList = action.payload;
    });

    builder.addCase(fetchAllBuyType.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const buyTypeReducer = buyTypeSlice.reducer;
