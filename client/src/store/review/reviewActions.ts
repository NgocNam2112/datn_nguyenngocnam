import { openAuthDialogFail } from "@/store/cart/cartSlice";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "./../apiError/apiErrorSlice";
import { IReview } from "./../../config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { REVIEW } from "@/config/config.constant";
import instance from "@/config/axios.config";
import { openCartDialog } from "../cart/cartSlice";

export const fetchReviewByProductId = createAsyncThunk<
  IReview[],
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "review/fetchReviewByProductId",
  async ({ params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const result = await instance("", undefined).get(`${REVIEW}/${params}`);
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const removeUserReview = createAsyncThunk<
  any,
  {
    reviewId: number;
    productId: number;
  },
  { rejectValue: ValidationErrors }
>(
  "reivew/deleteUserReview",
  async ({ reviewId, productId }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    const token = window.localStorage.getItem("accessToken");
    if (!token) {
      dispatch(openAuthDialogFail(true));
      return;
    }
    try {
      const result = await instance(token, undefined).delete(
        `${REVIEW}/delete-review/${reviewId}`
      );
      if (result.status === 200) {
        dispatch(openCartDialog(true));
        dispatch(fetchReviewByProductId({ params: productId }));
      }
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const addNewReview = createAsyncThunk<
  IReview,
  { content: string; productId: number },
  { rejectValue: ValidationErrors }
>(
  "review/addNewReview",
  async ({ content, productId }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    const token = window.localStorage.getItem("accessToken");
    if (!token) {
      dispatch(openAuthDialogFail(true));
      return;
    }

    try {
      const result = await instance(token, undefined).post(
        `${REVIEW}/add-new-review`,
        { content, productId }
      );
      if (result.status === 201) {
        dispatch(fetchReviewByProductId({ params: productId }));
      }
      return result.data;
    } catch (error) {
      dispatch(openAuthDialogFail(true));
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
