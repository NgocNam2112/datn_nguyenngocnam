import { IReview } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import {
  addNewReview,
  fetchReviewByProductId,
  removeUserReview,
} from "./reviewActions";

interface IReviewList {
  reviewList: IReview[];
  error?: string;
}

const initialState: IReviewList = {
  reviewList: [
    {
      id: 0,
      content: "",
      user: {
        id: 0,
        avatar: "",
        first_name: "",
        last_name: "",
      },
      product: {
        id: 0,
      },
      updated_at: "",
    },
  ],
  error: "",
};

export const reviewSlice = createSlice({
  name: "review",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchReviewByProductId.fulfilled, (state, action) => {
      state.reviewList = action.payload;
    });
    builder.addCase(fetchReviewByProductId.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(removeUserReview.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(addNewReview.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const reviewReducer = reviewSlice.reducer;
