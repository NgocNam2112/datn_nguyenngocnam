import AddToCart from "@/components/helper/addToCart/AddToCart";
import AuthFail from "@/components/helper/authFail/AuthFail";
import MyProfile from "@/components/myProfile/MyProfile";
import Layout from "@/layouts/Layout";
import { RootState } from "@/store";
import React from "react";
import { useSelector } from "react-redux";

const MyProfilePage = () => {
  const { userProfile } = useSelector((state: RootState) => state.user);
  const { isOpenCartDialogSuccess, isOpenCartDialogFail } = useSelector(
    (state: RootState) => state.cart
  );

  return (
    <>
      <Layout isNavBar titleLink="" hideNavBar={true}>
        <MyProfile userProfile={userProfile} />
      </Layout>
      {isOpenCartDialogSuccess && <AddToCart open={isOpenCartDialogSuccess} />}
      {isOpenCartDialogFail && (
        <AuthFail open={isOpenCartDialogFail} message="Unauthorization" />
      )}
    </>
  );
};

export default MyProfilePage;
