import Blog from "@/components/blog";
import Layout from "@/layouts/Layout";
import React from "react";

const BlogPage = () => {
  return (
    <Layout isNavBar={false}>
      <Blog />
    </Layout>
  );
};

export default BlogPage;
