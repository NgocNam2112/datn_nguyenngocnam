import FruitAndVegetable from "@/components/fruitAndVegetable";
import Layout from "@/layouts/Layout";
import React, { useEffect, useState } from "react";
import { RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { fetchProductByCategoryId } from "@/store/product/productActions";
import { fetchCategoryById } from "@/store/category/categoryActions";

const ListProduct = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [categoryId, setCategoryId] = useState<number | undefined>();
  const { productListByCategoryId } = useSelector(
    (state: RootState) => state.product
  );
  const { categoryById } = useSelector((state: RootState) => state.category);

  useEffect(() => {
    if (router.asPath !== router.route) {
      setCategoryId(+(router.query.id as string));
    }
  }, [router]);

  useEffect(() => {
    if (categoryId) {
      Promise.all([
        dispatch(fetchProductByCategoryId({ params: categoryId })),
        dispatch(fetchCategoryById({ params: categoryId })),
      ]);
    }
  }, [categoryId, dispatch]);

  return (
    <Layout
      isNavBar
      hideNavBar={true}
      titleLink={`list-product / ${categoryById.category_name}`}
    >
      <FruitAndVegetable product={productListByCategoryId} />
    </Layout>
  );
};

export default ListProduct;
