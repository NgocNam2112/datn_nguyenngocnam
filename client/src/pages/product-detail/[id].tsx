import AddToCart from "@/components/helper/addToCart/AddToCart";
import AuthFail from "@/components/helper/authFail/AuthFail";
import ProductDetail from "@/components/productDetail";
import { IProduct } from "@/config/types";
import Layout from "@/layouts/Layout";
import { RootState } from "@/store";
import { fetchAllBuyType } from "@/store/buyType/buyTypeActions";
import { addProductToCart } from "@/store/cart/cartActions";
import { fetchDescriptionByProductId } from "@/store/description/descriptionActions";
import { fetchProductById } from "@/store/product/productActions";
import { fetchReviewByProductId } from "@/store/review/reviewActions";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const ProductDetailPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const [productId, setProductId] = useState();

  const { descriptionByProductId } = useSelector(
    (state: RootState) => state.description
  );
  const { productList } = useSelector((state: RootState) => state.product);
  const { reviewList } = useSelector((state: RootState) => state.review);
  const { buyTypeList } = useSelector((state: RootState) => state.buyType);
  const { isOpenCartDialogSuccess, isOpenCartDialogFail } = useSelector(
    (state: RootState) => state.cart
  );

  useEffect(() => {
    if (router.asPath !== router.route) {
      setProductId(router.query.id as any);
      Promise.all([
        dispatch(
          fetchDescriptionByProductId({ params: +(router.query.id as any) })
        ),
        dispatch(fetchProductById({ params: +(router.query.id as any) })),
        dispatch(fetchReviewByProductId({ params: +(router.query.id as any) })),
        dispatch(fetchAllBuyType({})),
      ]);
    }
  }, [dispatch, router]);

  const handleAddProductToCart = (buyTypeId: number, quantity: number) => {
    dispatch(
      addProductToCart({
        productId: productId as unknown as number,
        buyTypeId: buyTypeId,
        quantity: quantity,
      })
    );
  };

  return (
    <>
      <Layout isNavBar={false} titleLink="product-detail" hideNavBar={false}>
        <ProductDetail
          descriptionByProductId={descriptionByProductId}
          productList={productList as IProduct}
          reviewList={reviewList}
          buyTypeList={buyTypeList}
          handleAddProductToCart={handleAddProductToCart}
          productId={productId as unknown as string}
        />
      </Layout>
      {isOpenCartDialogSuccess && <AddToCart open={isOpenCartDialogSuccess} />}
      {isOpenCartDialogFail && (
        <AuthFail open={isOpenCartDialogFail} message="Unauthorization" />
      )}
    </>
  );
};

export default ProductDetailPage;
