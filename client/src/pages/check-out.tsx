import Checkout from "@/components/checkout";
import AuthFail from "@/components/helper/authFail/AuthFail";
import instance from "@/config/axios.config";
import { COUNTRIES } from "@/config/config.constant";
import { ICountry } from "@/config/types";
import Layout from "@/layouts/Layout";
import { RootState } from "@/store";
import {
  fetchAllProductInCart,
  removeProductInCart,
} from "@/store/cart/cartActions";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

export async function getServerSideProps(context: any) {
  const result = await instance("", undefined).get(COUNTRIES);
  return {
    props: {
      countryList: result.data,
    },
  };
}

interface IProps {
  countryList: ICountry;
}

const CheckoutPage: React.FC<IProps> = ({ countryList }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { cartList, isOpenCartDialogFail } = useSelector(
    (state: RootState) => state.cart
  );
  const { userProfile } = useSelector((state: RootState) => state.user);

  useEffect(() => {
    dispatch(fetchAllProductInCart({}));
  }, [dispatch]);

  const removeProduct = (id: number) => {
    dispatch(removeProductInCart(id));
  };

  useEffect(() => {
    const token = window.localStorage.getItem("accessToken");
    if (!token) {
      router.push("/");
    }
  });

  return (
    <>
      <Layout isNavBar={true} titleLink="check-out" hideNavBar={true}>
        <Checkout
          cartList={cartList}
          removeProductInCart={removeProduct}
          countryList={countryList as unknown as ICountry[]}
          userProfile={userProfile}
        />
      </Layout>
      {isOpenCartDialogFail && (
        <AuthFail open={isOpenCartDialogFail} message="No product in cart" />
      )}
    </>
  );
};

export default CheckoutPage;
