import OrderDetail from "@/components/orderDetail/OrderDetail";
import Layout from "@/layouts/Layout";
import { RootState } from "@/store";
import { fetchOrderById } from "@/store/order/orderActions";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const OrderDetailPage = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { orderList } = useSelector((state: RootState) => state.order);
  const { userProfile } = useSelector((state: RootState) => state.user);

  useEffect(() => {
    if (router.asPath && router.route) {
      dispatch(
        fetchOrderById({ params: +(router.query.id as unknown as number) })
      );
    }
  }, [router, dispatch]);

  return (
    <Layout isNavBar titleLink="order-detail" hideNavBar={true}>
      <OrderDetail orderList={orderList} userProfile={userProfile} />
    </Layout>
  );
};

export default OrderDetailPage;
