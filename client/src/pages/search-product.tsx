import SearchProduct from "@/components/searchProduct";
import { IProduct } from "@/config/types";
import Layout from "@/layouts/Layout";
import { RootState } from "@/store";
import { searchProduct } from "@/store/product/productActions";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IPayloadProduct } from "./api/types";

const SearchProductPage = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const { productList } = useSelector((state: RootState) => state.product);

  useEffect(() => {
    if (router.asPath && router.route) {
      dispatch(searchProduct(router.query.q as string));
    }
  }, [router, dispatch]);

  return (
    <Layout isNavBar titleLink=" / search-product" hideNavBar={true}>
      <SearchProduct productList={productList as IProduct[]} />
    </Layout>
  );
};

const currentProduct: IPayloadProduct[] = [
  {
    image:
      "https://bizweb.dktcdn.net/thumb/large/100/269/612/products/brioche.jpg",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/large/100/269/612/products/brioche.jpg",
      img2: "https://bizweb.dktcdn.net/thumb/large/100/269/612/products/brioche.jpg",
    },
    productTitle: "Brioche Burger Buns 80g - 8cm ",
    spaceforDescription:
      "Brioche bread is made with butter, eggs, milk and a touch of sugar. ",
    price: 198800,
    discount: 10,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Brioche bread is made with butter, eggs, milk and a touch of sugar. These simple ingredients bring so much flavor, as well as a soft crumb. Toasted or as is, a brioche bun will make your burger the best it can be.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },
  {
    image:
      "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/677088f9-286e-4ef7-8d52-00389939577c.jpg?v=1630933738523",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/677088f9-286e-4ef7-8d52-00389939577c.jpg?v=1630933738523",
      img2: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/677088f9-286e-4ef7-8d52-00389939577c.jpg?v=1630933738523",
    },
    productTitle: "SWEET SANDWICH 1200G 12CMX12CM",
    spaceforDescription:
      "Using unbleached white flour, wheat germ, salt, sugar",
    price: 85000,
    discount: 5,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Using unbleached white flour, wheat germ, salt, sugar, vegetable oil, bakers dry yeast and bread improver, then blend with raisins carefully baked to retain moisture and softness for you to enjoy for breakfast or anytime.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },
  {
    image:
      "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/efd7e10c-cc07-443b-a0ef-b16e69e45417.jpg?v=1630933852547",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/a76cbb81-9e4b-4cc8-9fd4-c3e1287e6e5e.jpg?v=1630933853500",
      img2: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/a76cbb81-9e4b-4cc8-9fd4-c3e1287e6e5e.jpg?v=1630933853500",
    },
    productTitle: "FRUIT LOAF 475GR",
    spaceforDescription:
      "Using unbleached white flour, wheat germ, salt, sugar, vegetable oil, bakers dry yeast and bread improver",
    price: 44850,
    discount: 10,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Using unbleached white flour, wheat germ, salt, sugar, vegetable oil, bakers dry yeast and bread improver, then blend with raisins carefully baked to retain moisture and softness for you to enjoy for breakfast or anytime.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },
  {
    image:
      "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/cf748cb9-bb87-4564-8723-4305935497b6.png?v=1630933935543",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/cf748cb9-bb87-4564-8723-4305935497b6.png?v=1630933935543",
      img2: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/cf748cb9-bb87-4564-8723-4305935497b6.png?v=1630933935543",
    },
    productTitle: "FLAT WHOLE WHEAT PANINI 115G",
    spaceforDescription:
      "Using unbleach wholemeal flour, wheat germ, salt, vegetable oil, bakers dry yeast and bread improver",
    price: 69000,
    discount: 8,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Using unbleach wholemeal flour, wheat germ, salt, vegetable oil, bakers dry yeast and bread improver. A healthier options.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },

  {
    image:
      "https://bizweb.dktcdn.net/thumb/large/100/269/612/products/brioche.jpg",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/large/100/269/612/products/brioche.jpg",
      img2: "https://bizweb.dktcdn.net/thumb/large/100/269/612/products/brioche.jpg",
    },
    productTitle: "Brioche Burger Buns 80g - 8cm ",
    spaceforDescription:
      "Brioche bread is made with butter, eggs, milk and a touch of sugar. ",
    price: 198800,
    discount: 10,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Brioche bread is made with butter, eggs, milk and a touch of sugar. These simple ingredients bring so much flavor, as well as a soft crumb. Toasted or as is, a brioche bun will make your burger the best it can be.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },
  {
    image:
      "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/677088f9-286e-4ef7-8d52-00389939577c.jpg?v=1630933738523",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/677088f9-286e-4ef7-8d52-00389939577c.jpg?v=1630933738523",
      img2: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/677088f9-286e-4ef7-8d52-00389939577c.jpg?v=1630933738523",
    },
    productTitle: "SWEET SANDWICH 1200G 12CMX12CM",
    spaceforDescription:
      "Using unbleached white flour, wheat germ, salt, sugar",
    price: 85000,
    discount: 5,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Using unbleached white flour, wheat germ, salt, sugar, vegetable oil, bakers dry yeast and bread improver, then blend with raisins carefully baked to retain moisture and softness for you to enjoy for breakfast or anytime.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },
  {
    image:
      "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/efd7e10c-cc07-443b-a0ef-b16e69e45417.jpg?v=1630933852547",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/a76cbb81-9e4b-4cc8-9fd4-c3e1287e6e5e.jpg?v=1630933853500",
      img2: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/a76cbb81-9e4b-4cc8-9fd4-c3e1287e6e5e.jpg?v=1630933853500",
    },
    productTitle: "FRUIT LOAF 475GR",
    spaceforDescription:
      "Using unbleached white flour, wheat germ, salt, sugar, vegetable oil, bakers dry yeast and bread improver",
    price: 44850,
    discount: 10,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Using unbleached white flour, wheat germ, salt, sugar, vegetable oil, bakers dry yeast and bread improver, then blend with raisins carefully baked to retain moisture and softness for you to enjoy for breakfast or anytime.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },
  {
    image:
      "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/cf748cb9-bb87-4564-8723-4305935497b6.png?v=1630933935543",
    relatedImage: {
      img1: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/cf748cb9-bb87-4564-8723-4305935497b6.png?v=1630933935543",
      img2: "https://bizweb.dktcdn.net/thumb/grande/100/269/612/products/cf748cb9-bb87-4564-8723-4305935497b6.png?v=1630933935543",
    },
    productTitle: "FLAT WHOLE WHEAT PANINI 115G",
    spaceforDescription:
      "Using unbleach wholemeal flour, wheat germ, salt, vegetable oil, bakers dry yeast and bread improver",
    price: 69000,
    discount: 8,
    categoryID: "6232defbd78912075a341f17",
    stars: 4,
    detailsIngredients:
      "Using unbleach wholemeal flour, wheat germ, salt, vegetable oil, bakers dry yeast and bread improver. A healthier options.",
    detailDiscription: "",
    shipping: "",
    reviews: "",
    questions: "",
  },
];

export default SearchProductPage;
