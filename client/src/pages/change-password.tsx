import ChangePassword from "@/components/changePassword/ChangePassword";
import AddToCart from "@/components/helper/addToCart/AddToCart";
import Layout from "@/layouts/Layout";
import { RootState } from "@/store";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const ChangePasswordComponent = () => {
  const { isOpenCartDialogSuccess } = useSelector(
    (state: RootState) => state.cart
  );
  const router = useRouter();
  useEffect(() => {
    const token = window.localStorage.getItem("accessToken");
    if (!token) {
      router.push("/");
    }
  }, [router]);

  return (
    <>
      <Layout isNavBar titleLink="" hideNavBar={true}>
        <ChangePassword />
      </Layout>
      {isOpenCartDialogSuccess && <AddToCart open={isOpenCartDialogSuccess} />}
    </>
  );
};

export default ChangePasswordComponent;
