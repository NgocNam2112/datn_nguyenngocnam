import HomePage from "@/components/homePage";
import { IProduct } from "@/config/types";
import Layout from "@/layouts/Layout";
import { RootState } from "@/store";
import { fetchAllProduct } from "@/store/product/productActions";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

const Home: NextPage = () => {
  const { query } = useRouter();
  const dispatch = useDispatch();
  const { productList } = useSelector((state: RootState) => state.product);

  useEffect(() => {
    dispatch(fetchAllProduct({}));
  }, [dispatch]);

  return (
    <Layout isNavBar titleLink="" hideNavBar={true}>
      <HomePage productList={productList as IProduct[]} />
    </Layout>
  );
};

export default Home;
