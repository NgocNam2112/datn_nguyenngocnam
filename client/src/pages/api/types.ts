export interface IInfor {
  email: string;
  password: string;
}

export interface IPayloadUser {}

export interface IPayloadCatalog {
  categoryDescription: string;
  categoryName: string;
  _id: string;
}
export interface IDetailProduct {
  how_to_cook: Array<string> | [];
  origin: Array<string> | [];
  vitamins: Array<string> | [];
}
export interface IDetailIngridient {
  method: Array<string> | [];
  state: Array<string> | [];
}
export interface IPayloadProduct {
  categoryID: string;
  detailDiscription: string;
  discount: number;
  image: string;
  introduceProduct?: string;
  price: number;
  productTitle: string;
  relatedImage: {
    img1: string;
    img2: string;
  };
  shipping: string;
  spaceforDescription: string;
  stars: number;
  _id?: string;
  detailsIngredients?: string;
  reviews?: string;
  questions?: string;
}
