/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import { IUser } from "@/config/types";
import { updateProfile } from "@/store/user/userActions";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import useStyles from "./styles";

interface FormValues {
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
}

interface IProps {
  userProfile: IUser;
}

const MyProfile: React.VFC<IProps> = ({ userProfile }) => {
  const classes = useStyles();
  const router = useRouter();
  const dispatch = useDispatch();
  const [files, setFiles] = useState();
  const [image, setImage] = useState<any>();

  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm<FormValues>({
    defaultValues: {
      first_name: userProfile.first_name,
      last_name: userProfile.last_name,
      address: userProfile.address,
      town: userProfile.town,
      phone_number: userProfile.phone_number,
    },
  });

  const onChangeFile = (e: any) => {
    setFiles(e.target.files[0]);
    setImage(URL.createObjectURL(e.target.files[0]));
  };

  const onSubmit: SubmitHandler<FormValues> = (formValue: FormValues) => {
    dispatch(updateProfile({ userProfile: { ...formValue, files } }));
  };

  useEffect(() => {
    reset(userProfile);
  }, [reset, userProfile]);

  useEffect(() => {
    const token = window.localStorage.getItem("accessToken");
    if (!token) {
      router.push("/");
    }
  }, [router]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={classes.root}>
        <div>
          <label htmlFor="input-file">
            <img
              src={`${files ? image : userProfile.avatar}`}
              alt="avatar"
              style={{ width: 160, height: 160, objectFit: "contain" }}
            />
          </label>
          <input
            type="file"
            id="input-file"
            onChange={(e) => onChangeFile(e)}
          />
        </div>
        <div>
          <div className={classes.wrapperName}>
            <div className={classes.customInputField}>
              <span>First name: </span>
              <input
                {...register("first_name", {
                  required: {
                    value: true,
                    message: "Please enter your first name",
                  },
                  maxLength: {
                    value: 50,
                    message: "Enter the name from 1 to 50 characters.",
                  },
                })}
                type="text"
                placeholder="First name"
              />
              {errors.first_name && (
                <ErrorMessage
                  errorMessage={errors.first_name.message as string}
                />
              )}
            </div>
            <div className={classes.customInputField}>
              <span>Last name: </span>
              <input
                {...register("last_name", {
                  required: {
                    value: true,
                    message: "Please enter your last name",
                  },
                  maxLength: {
                    value: 50,
                    message: "Enter the name from 1 to 50 characters.",
                  },
                })}
                type="text"
                placeholder="Last name"
              />
              {errors.last_name && (
                <ErrorMessage
                  errorMessage={errors.last_name.message as string}
                />
              )}
            </div>
          </div>
          <div className={classes.wrapperName}>
            <div className={classes.customInputFieldFull}>
              <span>Phone number</span>
              <input
                {...register("phone_number", {
                  required: {
                    value: true,
                    message: "Please enter your phone number",
                  },
                  maxLength: {
                    value: 20,
                    message: "Phone number require less than 20 characters",
                  },
                  pattern: /\d{1,4}-\d{1,4}-\d{4}/,
                })}
                type="text"
                placeholder={"Phone number"}
              />
              {errors.phone_number && (
                <ErrorMessage
                  errorMessage={errors.phone_number.message as string}
                />
              )}
            </div>
          </div>
        </div>
      </div>
      <div className={classes.wrapperInput}>
        <div>
          <h2>Address</h2>
          <input
            {...register("address", {
              required: {
                value: true,
                message: "Please enter your address",
              },
              maxLength: 45,
            })}
            type="text"
            placeholder={"Address"}
          />
          {errors.address && (
            <ErrorMessage errorMessage={errors.address.message as string} />
          )}
        </div>

        <div>
          <h2>Town / City</h2>
          <input
            {...register("town", {
              required: {
                value: true,
                message: "Please enter your address",
              },
              maxLength: 45,
            })}
            type="text"
            placeholder={"Town or city"}
          />
          {errors.town && (
            <ErrorMessage errorMessage={errors.town.message as string} />
          )}
        </div>
      </div>
      <div className={classes.wrapFooterProfile}>
        <div className={classes.btnUpdate}>
          <button type="submit">UPDATE</button>
        </div>
        <div className={classes.wrapLinkContent}>
          <Link href="/change-password">
            <span>Change password</span>
          </Link>
        </div>
      </div>
    </form>
  );
};

export default MyProfile;
