/* eslint-disable @next/next/no-img-element */
import { IProduct } from "@/config/types";
import { Grid } from "@material-ui/core";
import useStyles from "./styles";
import React, { useState } from "react";
import ContentPagination from "../helper/contentPagination/ContentPagination";

interface IProps {
  product: IProduct[];
}
const FruitAndVegetable: React.VFC<IProps> = ({ product }) => {
  const classes = useStyles();

  const [pageNumber, setPageNumber] = useState<number>(1);

  return (
    <>
      <Grid item xs={12} md={12} lg={9} sm={12}>
        {product && (
          <ContentPagination products={product} setPageNumber={setPageNumber} />
        )}
      </Grid>
    </>
  );
};

export default FruitAndVegetable;
