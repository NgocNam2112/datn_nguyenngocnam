/* eslint-disable @next/next/no-html-link-for-pages */
import React, { useState } from "react";
import TradeName from "../icons/TradeName";
import useStyles from "./styles";
import { SubmitHandler, useForm } from "react-hook-form";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import instance from "@/config/axios.config";
import { USER } from "@/config/config.constant";
import { useRouter } from "next/router";
import CircularProgress from "@material-ui/core/CircularProgress";

interface FormValues {
  mail_address: string;
  password: string;
}

const SignIn = () => {
  const router = useRouter();
  const classes = useStyles();
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<FormValues>();
  const [loading, setLoading] = useState(false);
  const [existingError, setExistingError] = useState("");

  const onSubmit: SubmitHandler<FormValues> = async (formData) => {
    try {
      setLoading(true);
      const result = await instance("", undefined).post(`${USER}/sign-in`, {
        mail_address: formData.mail_address,
        password: formData.password,
      });
      window.localStorage.setItem("accessToken", result.data.accessToken);
      router.push("/");
      setLoading(false);
    } catch (error: any) {
      setExistingError(error.message);
    }
  };
  if (loading) {
    return (
      <div className={classes.circular}>
        <CircularProgress />
      </div>
    );
  }
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={classes.signIn}>
        <div className={classes.signInform}>
          <a href="/">
            <TradeName />
          </a>
          <p>SIGN IN </p>
          <ErrorMessage errorMessage={existingError} />
          <div>
            <span>Email: </span>
            <input
              {...register("mail_address", {
                required: { value: true, message: "Email is not empty" },
                maxLength: {
                  value: 256,
                  message:
                    "Please enter one or more and 256 characters or less for the email.",
                },
                pattern: {
                  value:
                    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message: "Some characters cannot be used.",
                },
              })}
              type="text"
              placeholder="Email"
            />
            {errors.mail_address && (
              <ErrorMessage
                errorMessage={errors.mail_address.message as string}
              />
            )}
          </div>
          <div>
            <span>Password:</span>
            <input
              {...register("password", {
                required: {
                  value: true,
                  message: "Password is not empty",
                },
                minLength: {
                  value: 6,
                  message: "Please enter the password from 6 to 20 characters.",
                },
                maxLength: {
                  value: 20,
                  message: "Please enter the password from 8 to 20 characters.",
                },
              })}
              type="password"
              placeholder="Password"
            />
            {errors.password && (
              <ErrorMessage errorMessage={errors.password.message as string} />
            )}
          </div>

          <button type="submit">Sign In</button>
        </div>
        <div className={classes.infoGroup}>
          <a href="/register">Sign up</a>
          <a href="/forgot-password">Forgot password?</a>
        </div>
      </div>
    </form>
  );
};

export default SignIn;
