import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  wrapperInput: {
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
    minWidth: 600,
    "& div": {
      display: "flex",
      flexDirection: "column",
      width: "100%",
      "& h2": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#151515",
      },
      "& input": {
        background: "#F9F9F9",
        border: "1px solid #D1D1D1",
        boxSizing: "border-box",
        borderRadius: "12px",
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        color: "#A9A9A9",
        width: "100%",
        height: 42,
        padding: "11px 16px 12px 21px",
        outline: "none",
      },
    },
  },
  wrapButton: {
    textAlign: "center",
    marginTop: 20,
    "& button": {
      width: 130,
      height: 36,
      color: "#fff",
      background: "#6A983C",
      outline: "none",
      border: "none",
      borderRadius: 12,
    },
  },
  wrapMatchPasswordContent: {
    textAlign: "left",
    marginTop: 12,
  },
}));

export default useStyles;
