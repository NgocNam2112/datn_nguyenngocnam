import instance from "@/config/axios.config";
import { USER } from "@/config/config.constant";
import { openCartDialog } from "@/store/cart/cartSlice";
import { Typography } from "@material-ui/core";
import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import useStyles from "./styles";

interface FormValues {
  old_password: string;
  new_password: string;
  confirm_new_password: string;
}

const ChangePassword = () => {
  const classes = useStyles();
  const [error, setError] = useState("");
  const [resultChanging, setResultChanging] = useState<any>("");
  const dispatch = useDispatch();
  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm<FormValues>();

  const onSubmit: SubmitHandler<FormValues> = async (formValues) => {
    setResultChanging("");
    if (formValues.new_password !== formValues.confirm_new_password) {
      setError("New password must be matched with confirm new password");
      return;
    }
    setError("");
    try {
      const token = window.localStorage.getItem("accessToken");
      const result = await instance(token, undefined).post(
        `${USER}/change-password`,
        {
          old_password: formValues.old_password,
          new_password: formValues.new_password,
        }
      );
      if (result.status === 201) {
        dispatch(openCartDialog(true));
      }
    } catch (error: any) {
      setResultChanging(error.response.data);
    }
  };

  console.log("resultChanging", resultChanging);

  return (
    <form>
      <div className={classes.wrapperInput}>
        <Typography variant="h4">Change password</Typography>
        <div>
          <h2>Old password</h2>
          <input
            {...register("old_password", {
              required: {
                value: true,
                message: "Old password is not empty",
              },
              minLength: {
                value: 6,
                message: "Please enter the password from 8 to 20 characters.",
              },
              maxLength: {
                value: 20,
                message: "Please enter the password from 8 to 20 characters.",
              },
            })}
            type="password"
            placeholder={"Old password"}
          />
          {errors.old_password && (
            <ErrorMessage
              errorMessage={errors.old_password.message as string}
            />
          )}
        </div>

        <div>
          <h2>New password</h2>
          <input
            {...register("new_password", {
              required: {
                value: true,
                message: "New password is not empty",
              },
              minLength: {
                value: 6,
                message: "Please enter the password from 6 to 20 characters.",
              },
              maxLength: {
                value: 20,
                message: "Please enter the password from 6 to 20 characters.",
              },
            })}
            type="password"
            placeholder={"New password"}
          />
          {errors.new_password && (
            <ErrorMessage
              errorMessage={errors.new_password.message as string}
            />
          )}
        </div>

        <div>
          <h2>Confirm new password</h2>
          <input
            {...register("confirm_new_password", {
              required: {
                value: true,
                message: "New password is not empty",
              },
              minLength: {
                value: 6,
                message: "Please enter the password from 6 to 20 characters.",
              },
              maxLength: {
                value: 20,
                message: "Please enter the password from 6 to 20 characters.",
              },
            })}
            type="password"
            placeholder={"New password"}
          />
          {errors.confirm_new_password && (
            <ErrorMessage
              errorMessage={errors.confirm_new_password.message as string}
            />
          )}
        </div>
      </div>
      <div className={classes.wrapMatchPasswordContent}>
        {error && <ErrorMessage errorMessage={error} />}
      </div>
      <div className={classes.wrapMatchPasswordContent}>
        {resultChanging.message && (
          <ErrorMessage errorMessage={resultChanging.message} />
        )}
      </div>
      <div className={classes.wrapButton}>
        <button type="submit" onClick={handleSubmit(onSubmit)}>
          Change
        </button>
      </div>
    </form>
  );
};

export default ChangePassword;
