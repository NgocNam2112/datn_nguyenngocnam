/* eslint-disable @next/next/no-img-element */
import React from "react";
import useStyles from "./styles";

interface IProps {
  mainblog: string;
  tags: string;
  blogheadline: string;
  avatarauthor: string;
  date: string;
}

const SpaceForBlog: React.FC<IProps> = ({
  mainblog,
  tags,
  blogheadline,
  avatarauthor,
  date,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.dinnerTips}>
      <img src={mainblog} alt="" />
      <button>{tags}</button>
      <h2>{blogheadline}</h2>
      <div>
        <img src={avatarauthor} alt="" />
        <span>Author</span>
        <span>{date}</span>
      </div>
    </div>
  );
};

export default SpaceForBlog;
