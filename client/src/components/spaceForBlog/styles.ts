import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  dinnerTips: {
    position: "relative",
    width: "48%",
    height: 400,
    objectFit: "contain",
    "& > img": {
      width: "100%",
      height: "100%",
    },
    "& button": {
      position: "absolute",
      top: 24,
      left: 24,
      background: "#F4F8EC",
      borderRadius: "12px",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "12px",
      lineHeight: "18px",
      color: "#6A983C",
      border: "none",
      padding: "0 8px",
      cursor: "pointer",
      outlint: "none",
    },
    "& h2": {
      position: "absolute",
      top: 250,
      left: 24,
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "22px",
      lineHeight: "33px",
      color: "#FFFFFF",
      width: 380,
      height: 66,
    },
    "& div": {
      position: "absolute",
      bottom: 24,
      left: 24,
      display: "flex",
      alignItems: "center",
      "& img": {
        marginRight: 8,
        objectFit: "contain",
      },
      "& span": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "12px",
        lineHeight: "16px",
        color: "#FFFFFF",
        marginRight: 16,
      },
    },
  },
  [theme.breakpoints.down(924)]: {
    dinnerTips: {
      display: "none",
    },
  },
  [theme.breakpoints.down(728)]: {
    dinnerTips: {
      display: "block",
      width: "100%",
    },
  },
}));

export default useStyles;
