/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import instance from "@/config/axios.config";
import { ORDER } from "@/config/config.constant";
import { ICart, ICountry, IUser } from "@/config/types";
import { openAuthDialogFail } from "@/store/cart/cartSlice";
import {
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid,
  Menu,
  MenuItem,
} from "@material-ui/core";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import DrawerMenu from "../drawerMenu/DrawerMenu";
import AdormentInput from "../helper/adormentInput/AdormentInput";
import ApplyNow from "../icons/ApplyNow";
import DownArrowBlack from "../icons/Arrows/DownArrowBlack";
import CloseSign from "../icons/CloseSign";
import CompareCheckout from "../icons/CompareCheckout";
import FavouriteCheckout from "../icons/FavouriteCheckout";
import RingStar from "../icons/RingStar";
import SecuritySafely from "../icons/SecuritySafely";
import Stars from "../icons/Stars";
import useStyles from "./styles";

interface IProps {
  cartList: ICart[];
  removeProductInCart: (id: number) => void;
  countryList: ICountry[];
  userProfile: IUser;
}

interface FormValues {
  first_name: string;
  last_name: string;
  phone_number: string;
  town: string;
  address: string;
  country: string;
}

const Checkout: React.VFC<IProps> = ({
  cartList,
  removeProductInCart,
  countryList,
  userProfile,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useRouter();

  const [checkbox, setCheckbox] = useState<boolean>(false);
  const [agreeNoSpam, setAgreeNoSpam] = useState<boolean>(true);
  const [agreePolicy, setAgreePolicy] = useState<boolean>(true);
  const [country, setCountry] = useState<ICountry | undefined>();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleChange = (state: boolean, func: Function) => {
    func(!state);
  };

  const formControlLable = (
    icon: JSX.Element,
    checkIcon: JSX.Element,
    text: String,
    state: boolean,
    func: Function
  ) => {
    return (
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              icon={icon}
              checkedIcon={checkIcon}
              checked={state}
              onChange={() => handleChange(state, func)}
            />
          }
          label={text}
        />
      </FormGroup>
    );
  };

  const renderWrapperInput = () => {
    return (
      <>
        <div>
          <img
            src={userProfile.avatar}
            alt="avatar"
            style={{ width: 200, height: 200, objectFit: "contain" }}
          />
        </div>
        <div className={classes.wrapperInput}>
          <div>
            <h2>First name </h2>
            <input
              type="text"
              placeholder={"First Name"}
              defaultValue={userProfile.first_name || ""}
              disabled
            />
          </div>
          <div>
            <h2>Last name</h2>
            <input
              type="text"
              placeholder={"Last Name"}
              defaultValue={userProfile.last_name || ""}
              disabled
            />
          </div>
        </div>
        <div className={classes.wrapperInput}>
          <div>
            <h2>Phone number</h2>
            <input
              type="text"
              defaultValue={userProfile.phone_number || ""}
              placeholder={"Phone number"}
              disabled
            />
          </div>
          <div>
            <h2>State / Country</h2>
            <Button
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={handleClick}
            >
              <input
                type="text"
                defaultValue={country?.country_name}
                placeholder={"State / Country"}
                disabled
              />
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              {countryList &&
                countryList.map((item: ICountry, index: number) => (
                  <MenuItem
                    onClick={() => {
                      setCountry(item);
                      handleClose();
                    }}
                    key={index}
                  >
                    {item.country_name}
                  </MenuItem>
                ))}
            </Menu>
          </div>
        </div>
        <div className={classes.wrapperInput}>
          <div>
            <h2>Address</h2>
            <input
              type="text"
              defaultValue={userProfile.address || ""}
              placeholder={"Address"}
              disabled
            />
          </div>

          <div>
            <h2>Town / City</h2>
            <input
              type="text"
              defaultValue={userProfile.town || ""}
              placeholder={"Town or city"}
            />
          </div>
        </div>
      </>
    );
  };

  const renderRules = (
    icon: JSX.Element,
    checkIcon: JSX.Element,
    text: String,
    state: boolean,
    func: Function
  ) => {
    return (
      <div className={classes.blockPolicy}>
        {formControlLable(icon, checkIcon, text, state, func)}
      </div>
    );
  };

  const handleCompleteOrder = async () => {
    if (!cartList.length) {
      dispatch(openAuthDialogFail(true));
      return;
    }
    const token = window.localStorage.getItem("accessToken");
    try {
      const result = await instance(token, undefined).post(
        `${ORDER}/add-new-order`
      );
      if (result.status === 201) {
        router.push(`/order-detail/${result.data.orderId}`);
      }
    } catch (error) {
      console.log("error", error);
    }
  };

  const renderFifthstep = () => {
    return (
      <>
        <div className={classes.fifthStep}>
          <button onClick={() => handleCompleteOrder()}>Complete order</button>
        </div>
        <div className={classes.secritySafely}>
          <SecuritySafely />
          <div>
            <h2>All your data are safe</h2>
            <p>
              We are using the most advanced security to provide you the best
              experience ever.
            </p>
          </div>
        </div>
        <div className={classes.footer}>Copyright © 2020 Freshnesecom</div>
      </>
    );
  };

  const renderStar = (stars: number, ringStars: number) => {
    let arr = [];
    for (let i = 0; i < stars; i++) {
      if (i < ringStars)
        arr.push(
          <div key={i}>
            <RingStar />
          </div>
        );
      else
        arr.push(
          <div key={i}>
            <Stars />
          </div>
        );
    }
    return arr;
  };

  const getTotalPrice = () => {
    let totalPrice = 0;
    if (cartList.length) {
      cartList.forEach((item) => {
        const discoutPrice =
          Math.floor(
            item.product.selling_price * (100 - item.product.discount)
          ) / 100;
        totalPrice += discoutPrice;
      });
      return totalPrice;
    }
  };

  // contain: Subtotal,Tax, Shipping
  const renderRemainCart = () => {
    return (
      <>
        <AdormentInput placeholder="Apply promo code" element={<ApplyNow />} />
        <div className={classes.wrapperCartFooter}>
          <div className={classes.totalOrder}>
            <h2>Total Order</h2>
          </div>
          <div className={classes.finalPrice}>{getTotalPrice()} USD</div>
        </div>
      </>
    );
  };

  const renderCart = () => {
    return (
      <div className={classes.cotainerCartItem}>
        <div className={classes.cartTitle}>
          <h2>Order Summary</h2>
          <p>
            Price can change depending on shipping method and taxes of your
            state.
          </p>
        </div>
        {cartList.length ? (
          <>
            {cartList.map((item: ICart, index: number) => {
              return (
                <div key={item.id} className={classes.wrapperProductCart}>
                  <div className={classes.image}>
                    <div>
                      <a href={`product-detail/${item.product.id}`}>
                        <img src={item.product.image} alt="product in cart" />
                      </a>
                    </div>
                    <div>
                      <FavouriteCheckout />
                      <span>Wishlist</span>
                    </div>
                    <div>
                      <CompareCheckout />
                      <span>Compare</span>
                    </div>
                    <div onClick={() => removeProductInCart(item.product.id)}>
                      <CloseSign />
                      <span>Remove</span>
                    </div>
                  </div>
                  <Link href={`product-detail/${item.product.id}`}>
                    <div className={classes.productCartInfo}>
                      <h2>{item.product.name}</h2>
                      <div className={classes.productList}>
                        <span>Farm:</span>
                        <span>Freshness:</span>
                      </div>
                      <div className={classes.productDetail}>
                        <span>{item.product.farm}</span>
                        <span>1 day old</span>
                      </div>
                      <div style={{ display: "flex", marginTop: 10 }}>
                        {renderStar(5, +item.product.rate)}
                      </div>
                      <div className={classes.productWrapper}>
                        <div className={classes.priceProductCart}>
                          <span>
                            {Math.floor(
                              item.product.selling_price *
                                (100 - item.product.discount)
                            ) / 100}
                            USD
                          </span>
                          <span>{item.product.selling_price} USD</span>
                        </div>
                        <div className={classes.pcs}>
                          <span>{item.quantity}</span>
                          <span>
                            Pcs
                            <DownArrowBlack />
                          </span>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              );
            })}
            {renderRemainCart()}
          </>
        ) : (
          <div
            style={{ width: "100%", display: "flex", justifyContent: "center" }}
          >
            <h1>No product in cart</h1>
          </div>
        )}
      </div>
    );
  };

  const renderButtonResponseCart = () => {
    return <>Shopping cart here!</>;
  };

  return (
    <Grid item xs={12} md={12} lg={9} sm={12}>
      <div className={classes.root}>
        <Grid
          container
          item
          xs={12}
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <Grid item xl={7} lg={7} md={12} xs={12}>
            <div className={classes.firstStep}>
              <h1>Billing info</h1>
            </div>
            {renderWrapperInput()}
            {renderFifthstep()}
          </Grid>
          <Grid item xl={4} lg={4} md={12} xs={12} className={classes.cart}>
            {renderCart()}
          </Grid>
          <Grid item xs={12} className={classes.cartResponse}>
            <DrawerMenu
              drawerType="bottom"
              blockContent={renderButtonResponseCart}
              content={renderCart}
            />
          </Grid>
        </Grid>
      </div>
    </Grid>
  );
};

export default Checkout;
