import React from "react";

const CloseSign = () => {
  return (
    <svg
      width="8"
      height="8"
      viewBox="0 0 8 8"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M7.18031 7.18004L0.820312 0.820038"
        stroke="#151515"
        strokeLinecap="round"
        strokeLinejoin="bevel"
      />
      <path
        d="M7.18031 0.820038L0.820312 7.18004"
        stroke="#151515"
        strokeLinecap="round"
        strokeLinejoin="bevel"
      />
    </svg>
  );
};

export default CloseSign;
