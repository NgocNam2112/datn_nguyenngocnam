import React from "react";

const CheckOutIcon = () => {
  return (
    <svg
      width="22"
      height="22"
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10.8105 20.8701C16.3334 20.8701 20.8105 16.3929 20.8105 10.8701C20.8105 5.34721 16.3334 0.870056 10.8105 0.870056C5.2877 0.870056 0.810547 5.34721 0.810547 10.8701C0.810547 16.3929 5.2877 20.8701 10.8105 20.8701Z"
        fill="white"
        stroke="#D1D1D1"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="bevel"
      />
    </svg>
  );
};

export default CheckOutIcon;
