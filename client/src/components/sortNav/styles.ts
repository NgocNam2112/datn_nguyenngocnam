import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  "wrapper-category-name": {
    "& h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
      marginBottom: 16,
    },
  },
  "item-category": {
    display: "flex",
    justifyContent: "space-between",
    "& span": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      color: "#151515",
      marginBottom: 12,
      "&:last-child": {
        padding: "0 8px",
        background: "#F4F8EC",
        borderRadius: "12px",
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#6A983C",
      },
    },
  },
  "brand-name-wapper": {
    marginTop: 48,
    "& h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
      marginBottom: 18,
    },
  },
  "form-control": {
    margin: "0 0 17px 0",
    "& span": {
      "&:first-child": {
        padding: 0,
        marginRight: 10,
      },
      "&:last-child": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        color: "#151515",
      },
    },
  },
  "filed-set": {
    "& legend": {
      paddingTop: 50,
      paddingBottom: 18,
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515 !important",
    },
    "& div": {
      "& label": {
        margin: "0 0 16px 0",
        "& span": {
          padding: 0,
          "&:last-child": {
            marginRight: 11.31,
          },
        },
      },
    },
  },

  "express-value-slider": {
    marginTop: 129,
  },
  "custom-slider": {
    "& h1": {
      margin: "50px 0 23px 0",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
    },
  },
  "getting-slider-value": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 23,
    "& div": {
      "& p": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#151515",
      },
      "& span": {
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        color: "#A9A9A9",
      },
      "& div": {
        width: 109,
        height: 42,
        background: "#F9F9F9",
        border: "1px solid #D1D1D1",
        boxSizing: "border-box",
        borderRadius: "12px",
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        color: "#A9A9A9",
        padding: "11px 16px 12px 21px",
      },
    },
  },
  "button-apply": {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 31,
    "& button": {
      boxSizing: "border-box",
      borderRadius: "12px",
      width: "78px",
      height: "48px",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      cursor: "pointer",
      background: "#FFFFFF",
      lineHeight: "22px",
      color: "#A9A9A9",
      border: "none",
      outline: "none",
      "&:first-child": {
        background: "#6A983C",
        lineHeight: "22px",
        color: "#FFFFFF",
        outline: "none",
        border: "2px solid #46760A",
      },
    },
  },
  "filter-product": {
    display: "block",
    paddingRight: 20,
  },
  "filter-product-responsive": {
    display: "none",
    position: "fixed",
    left: "50%",
    transform: "translate('-50%')",
    zIndex: 10,
    bottom: 0,
  },
  "div-respon": {
    display: "grid",
    gridTemplateColumns: "auto auto",
    gridGap: "10px",
    "& > div": {
      padding: "10px 20px",
    },
    "& fieldset": {
      padding: "10px 20px",
    },
  },
  [theme.breakpoints.down(1280)]: {
    "filter-product": {
      display: "none",
    },
    "filter-product-responsive": {
      display: "block",
    },
    "brand-name-wapper": {
      marginTop: 0,
    },
    "filed-set": {
      "& legend": {
        paddingTop: 0,
      },
    },
    "div-respon": {
      "& div": {
        padding: "0 20px",
      },
    },
    "custom-slider": {
      "& h1": {
        margin: 0,
      },
    },
  },
}));

export default useStyles;
