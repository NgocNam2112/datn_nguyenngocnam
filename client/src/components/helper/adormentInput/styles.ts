import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  "promo-code": {
    position: "relative",
    marginTop: 32,
    "& input": {
      marginTop: 16,
      padding: "11px 100px 12px 21px",
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      color: "#A9A9A9",
      width: "100%",
      height: 42,
      background: "#F9F9F9",
      border: "1px solid #D1D1D1",
      boxSizing: "border-box",
      borderRadius: "12px",
      outline: "none",
    },
    "& svg": {
      position: "absolute",
      top: "50%",
      right: 16,
      cursor: "pointer",
    },
  },
});

export default useStyles;
