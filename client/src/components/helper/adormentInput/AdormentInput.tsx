import React from "react";
import useStyles from "./styles";

interface IProps {
  placeholder: string;
  element: JSX.Element;
}

const AdormentInput: React.FC<IProps> = ({ placeholder, element }) => {
  const classes = useStyles();
  return (
    <div className={classes["promo-code"]}>
      <input type="text" placeholder={placeholder} />
      {element}
    </div>
  );
};

export default AdormentInput;
