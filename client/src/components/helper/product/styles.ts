import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  itemDiv: {
    position: "relative",
    width: 269,
    background: "#FFFFFF",
    border: "1px solid #D1D1D1",
    boxSizing: "border-box",
    borderRadius: "12px",
    padding: "0 16px",
    marginBottom: 73,
    "& a": {
      textDecoration: "none",
      "& img": {
        "&:first-child": {
          margin: "16px 0",
        },
        marginBottom: 17.6,
      },
      "& > h2": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: 700,
        fontSize: "15px",
        lineHeight: "22px",
        color: "#151515",
        marginTop: 25,
      },
      "& p": {
        marginTop: 4,
        marginBottom: 13.62,
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "12px",
        lineHeight: "16px",
        color: "#575757",
      },
    },
  },
  discountPercent: {
    position: "absolute",
    top: 28,
    left: 36,
    fontFamily: "Poppins",
    fontStyle: "normal",
    fontSize: "12px",
    lineHeight: "18px",
    color: "#6A983C",
  },
  discountBlog: {
    display: "flex",
    justifyContent: "space-around",
    '& a': {
      textDecoration: "none",
      "& div": {
        display: "flex",
        flexDirection: "column",
        marginBottom: 0,
        "& h2": {
          fontFamily: "Poppins",
          fontStyle: "normal",
          fontWeight: "600",
          fontSize: "18px",
          color: "#151515",
          margin: 0,
        },
        "& p": {
          fontFamily: "Poppins",
          fontWeight: "600",
          fontSize: 12,
          color: "#A9A9A9",
          textDecoration: "line-through",
        },
      },
    },
    "& button": {
      width: "90px",
      height: "36px",
      right: "0px",
      bottom: "0px",
      background: "#6A983C",
      border: "2px solid #46760A",
      boxSizing: "border-box",
      borderRadius: "12px",
      outline: "none",
      cursor: "pointer",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#FFFFFF",
    },
  },
  blogImage: {
    width: 236,
    height: 180,
  },
});

export default useStyles;
