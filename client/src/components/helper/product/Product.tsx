/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable @next/next/link-passhref */
import Rectangle from "@/components/icons/Rectangle";
import Link from "next/link";
import React from "react";
import useStyles from "./styles";

interface IProps {
  titleProduct: String;
  description: String;
  price: number;
  discount: number;
  stars?: JSX.Element[];
  image?: JSX.Element | string;
  isLoad?: boolean;
  keyInd?: number;
  id?: string;
}

const Product: React.FC<IProps> = ({
  titleProduct,
  description,
  price,
  discount,
  stars,
  image,
  isLoad,
  keyInd,
  id,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.itemDiv} key={keyInd}>
      <Link href={`/product-detail/${id}`}>
        <a>
          <div className={classes.blogImage}>
            {image}
            {isLoad ? null : <Rectangle />}
          </div>
          <h2>{titleProduct}</h2>
          <p>{description}</p>
          {stars}
        </a>
      </Link>
      <div className={classes.discountBlog}>
        <a href={`/product-detail/${id}`}>
          <div>
            <h2>{(price * (100 - discount)) / 100} USD</h2>
            <p>{price} USD</p>
          </div>
        </a>
        <button>Buy Now</button>
      </div>
      {discount !== 0 ? (
        <h4 className={classes.discountPercent}>-{discount}%</h4>
      ) : null}
    </div>
  );
};

export default Product;
