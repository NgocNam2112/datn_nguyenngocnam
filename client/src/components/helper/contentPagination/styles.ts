import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  wrapperProduct: {
    display: "grid",
    gridTemplateColumns: "auto auto auto auto",
    flexWrap: "wrap",
    justifyContent: "space-between",
    justifyItems: "center",
    [theme.breakpoints.down(1530)]: {
      gridTemplateColumns: "auto auto auto auto",
    },
    [theme.breakpoints.down(1529)]: {
      gridTemplateColumns: "auto auto auto",
    },
    [theme.breakpoints.down(1200)]: {
      gridTemplateColumns: "auto auto auto",
    },
    [theme.breakpoints.down(1000)]: {
      gridTemplateColumns: "auto auto auto",
    },
    [theme.breakpoints.down(900)]: {
      gridTemplateColumns: "auto auto",
      justifyContent: "stretch",
      wrapperProduct: {
        width: "100%",
      },
    },
    [theme.breakpoints.down(635)]: {
      gridTemplateColumns: "auto",
      justifyContent: "stretch",
      wrapperProduct: {
        width: "100%",
      },
    },
  },
}));

export default useStyles;
