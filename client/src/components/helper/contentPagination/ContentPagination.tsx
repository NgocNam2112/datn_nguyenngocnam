/* eslint-disable @next/next/no-img-element */
import { Grid } from "@material-ui/core";
import { useRouter } from "next/router";
import React, { useState, useEffect } from "react";
import Product from "../product/Product";
import useStyles from "./styles";

interface IProps {
  products: Array<any>;
  setPageNumber: Function;
  renderStars?: (stars: number, ringStars: number) => JSX.Element[];
}

const ContentPagination: React.FC<IProps> = ({
  products,
  setPageNumber,
  renderStars,
}) => {
  const router = useRouter();
  const classes = useStyles();
  const [isLoad, setIsLoad] = useState<boolean>(false);
  const query = new URLSearchParams(router.query?.search as string);
  const LoadingImage = () => {
    setIsLoad(true);
  };
  const ErrorLoading = () => {
    setIsLoad(false);
  };

  useEffect(() => {
    LoadingImage();
  });
  useEffect(() => {
    if (query.get("page") === null) {
      setPageNumber(1);
    } else {
      setPageNumber(parseInt(query.get("page") as string));
    }
  });
  return (
    <Grid item xs={12} sm={12} md={12} className={classes.wrapperProduct}>
      {products.map((item: any, index: number) => {
        return (
          <Product
            titleProduct={item.name}
            description={item.space_for_discription}
            price={item.selling_price}
            image={
              <img
                src={item.image}
                alt=""
                onLoad={LoadingImage}
                onError={ErrorLoading}
                style={{ width: 237, height: 180, borderRadius: 12 }}
              />
            }
            id={item.id}
            isLoad={isLoad}
            keyInd={index}
            discount={item.discount}
            stars={renderStars ? renderStars(5, +item.rate) : undefined}
            key={index}
          />
        );
      })}
    </Grid>
  );
};

export default ContentPagination;
