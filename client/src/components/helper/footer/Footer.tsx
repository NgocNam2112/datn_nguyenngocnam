import React from "react";
import useStyles from "./styles";

const Footer = () => {
  const classes = useStyles();
  const aboutShop = [
    {
      h1: "Get in touch",
      items: ["About Us", "Careers", "Press Releases", "Blog"],
    },
    {
      h1: "Connections",
      items: ["Facebook", "Twitter", "Instagram", "YouTube", "LinkedIn"],
    },
    {
      h1: "Earnings",
      items: [
        "Become an Affiliate",
        "Advertise your product",
        "Sell on Market",
      ],
    },
    {
      h1: "Account",
      items: [
        "Your account",
        "Returns Centre",
        "100 % purchase protection",
        "Chat with us",
        "Help",
      ],
    },
  ];

  const productTags = [
    "Beans",
    "Carrots",
    "Apples",
    "Garlic",
    "Mushrooms",
    "Tomatoes",
    "Chili Peppers",
    "Broccoli",
    "Watermelons",
    "Oranges",
    "Bananas",
    "Grapes",
    "Cherries",
    "Meat",
    "Seo Tags",
    "Fish",
    "Seo tag",
    "Fresh food",
    "Lemons",
  ];
  return (
    <>
      <div className={classes.aboutShop}>
        {aboutShop.map((item: any, index: number) => {
          return (
            <div key={index}>
              <h1>{item.h1}</h1>
              {item.items.map((i: any, ind: number) => {
                return <p key={ind}>{i}</p>;
              })}
            </div>
          );
        })}
      </div>
      <div className={classes.productTags}>
        <h1>Product Tags</h1>
        <div>
          {productTags.map((item: any, index: number) => {
            return <span key={index}>{item}</span>;
          })}
        </div>
      </div>
      <p className={classes.copyright}>Copyright © 2020 petrbilek.com</p>
    </>
  );
};

export default Footer;
