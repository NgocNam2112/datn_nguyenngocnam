import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  aboutShop: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 64,
    "& h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
    },
    "& p": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      color: "#6A983C",
      marginTop: 16,
    },
  },
  productTags: {
    marginTop: 48,
    "& h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
    },
    "& div": {
      display: "flex",
      flexWrap: "wrap",
      "& span": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#151515",
        background: "#F5F5F5",
        borderRadius: "12px",
        padding: "4px 10px",
        margin: "16px 48px 0 0",
        cursor: "pointer",
      },
    },
  },
  copyright: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "14px",
    lineHeight: "19px",
    color: "#151515",
    margin: "52px 0 64px 0",
  },
});

export default useStyles;
