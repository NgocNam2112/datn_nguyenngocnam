import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  modalContainer: {
    width: 500,
    height: 300,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    "& h1": {
      marginTop: 30,
    },
    "& svg": {
      "&:last-child": {
        width: 150,
        height: 150,
        color: "#6A983C",
      },
    },
  },
}));

export default useStyles;
