import TradeName from "@/components/icons/TradeName";
import { Dialog, DialogTitle } from "@material-ui/core";
import React from "react";
import useStyles from "./styles";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { useDispatch } from "react-redux";
import { closeCartDialog } from "@/store/cart/cartSlice";

interface IProps {
  open: boolean;
}

const AddToCart: React.FC<IProps> = ({ open }) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleClose = () => {
    dispatch(closeCartDialog(false));
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <div className={classes.modalContainer}>
        <TradeName />
        <CheckCircleIcon />
      </div>
    </Dialog>
  );
};

export default AddToCart;
