import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  errorMessage: {
    fontSize: 12,
    fontFamily: "Poppins",
    color: "red",
    fontWeight: 700,
  },
}));

export default useStyles;
