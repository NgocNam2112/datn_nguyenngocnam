import React from "react";
import { FieldError } from "react-hook-form";
import useStyles from "./styles";

interface IProps {
  errorMessage: string;
}

const ErrorMessage: React.VFC<IProps> = ({ errorMessage }) => {
  const classes = useStyles();

  return <div className={classes.errorMessage}>{errorMessage}</div>;
};

export default ErrorMessage;
