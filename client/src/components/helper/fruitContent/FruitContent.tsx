import CheckboxIcon from "@/components/icons/CheckboxIcon";
import RingStar from "@/components/icons/RingStar";
import UnCheckboxIcon from "@/components/icons/UnCheckboxIcon";
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { Stars } from "@material-ui/icons";
import { withStyles } from "@material-ui/styles";
import React, { useState } from "react";
import useStyles from "./styles";
import Slider from "@material-ui/core/Slider";
import DrawerMenu from "@/components/drawerMenu/DrawerMenu";
import PublishIcon from "@material-ui/icons/Publish";

const FruitContent = () => {
  const classes = useStyles();
  const [checkboxBrand1, setCheckboxBrand1] = useState<boolean>(false);
  const [checkboxBrand2, setCheckboxBrand2] = useState<boolean>(true);
  const [checkboxBrand3, setCheckboxBrand3] = useState<boolean>(true);
  const [checkboxBrand4, setCheckboxBrand4] = useState<boolean>(false);
  const [checkboxBrand5, setCheckboxBrand5] = useState<boolean>(false);
  const [valueSlider, setValueSlider] = useState([20, 40]);

  const [getStars, setGetStar] = useState<String>("5 stars");

  const handleChange = (state: boolean, func: Function) => {
    func(!state);
  };

  const renderCateGories = () => {
    return (
      <div className={classes["wrapper-category-name"]}>
        <h1>Categories</h1>
        <div className={classes["item-category"]}>
          <span>Category name</span>
          <span>320</span>
        </div>
        <div className={classes["item-category"]}>
          <span>Category name</span>
          <span>320</span>
        </div>
        <div className={classes["item-category"]}>
          <span>Category name</span>
          <span>320</span>
        </div>
        <div className={classes["item-category"]}>
          <span>Category name</span>
          <span>320</span>
        </div>
      </div>
    );
  };

  const formControlLable = (state: boolean, func: Function, text: string) => {
    return (
      <FormGroup row>
        <FormControlLabel
          className={classes["form-control"]}
          control={
            <Checkbox
              icon={<UnCheckboxIcon />}
              checkedIcon={<CheckboxIcon />}
              checked={state}
              onChange={() => handleChange(state, func)}
            />
          }
          label={text}
        />
      </FormGroup>
    );
  };

  const renderBrand = () => {
    return (
      <div className={classes["brand-name-wapper"]}>
        <h1>Brand</h1>
        {formControlLable(
          checkboxBrand1,
          setCheckboxBrand1,
          "Filtre by brand item"
        )}
        {formControlLable(
          checkboxBrand2,
          setCheckboxBrand2,
          "Filtre by brand item"
        )}
        {formControlLable(
          checkboxBrand3,
          setCheckboxBrand3,
          "Filtre by brand item"
        )}
        {formControlLable(
          checkboxBrand4,
          setCheckboxBrand4,
          "Filtre by brand item"
        )}
        {formControlLable(
          checkboxBrand5,
          setCheckboxBrand5,
          "Filtre by brand item"
        )}
      </div>
    );
  };

  function StyledRadio(props: any) {
    return (
      <Radio
        disableRipple
        color="default"
        checkedIcon={<CheckboxIcon />}
        icon={<UnCheckboxIcon />}
        {...props}
      />
    );
  }

  const Rating = (stars: number, ringStars: number) => {
    let arr = [];
    for (let i = 0; i < stars; i++) {
      if (i < ringStars) arr.push(<RingStar />);
      else arr.push(<Stars />);
    }
    return arr;
  };

  const handleChangeStars = (e: any, newValue: String) => {
    setGetStar(newValue);
  };

  const renderRating = () => {
    return (
      <FormControl component="fieldset" className={classes["filed-set"]}>
        <FormLabel component="legend">Rating</FormLabel>
        <RadioGroup
          defaultValue="5 stars"
          aria-label="gender"
          name="customized-radios"
          onChange={handleChangeStars}
        >
          <FormControlLabel
            value="5 stars"
            control={<StyledRadio />}
            label={Rating(5, 5)}
          />
          <FormControlLabel
            value="4 stars"
            control={<StyledRadio />}
            label={Rating(5, 4)}
          />
          <FormControlLabel
            value="3 stars"
            control={<StyledRadio />}
            label={Rating(5, 3)}
          />
          <FormControlLabel
            value="2 stars"
            control={<StyledRadio />}
            label={Rating(5, 2)}
          />
          <FormControlLabel
            value="1 stars"
            control={<StyledRadio />}
            label={Rating(5, 1)}
          />
        </RadioGroup>
      </FormControl>
    );
  };

  const handleChangeValueSlider = (e: any, newValue: any) => {
    setValueSlider(newValue);
  };

  const renderSliderValue = () => {
    return (
      <div className={classes["getting-slider-value"]}>
        <div>
          <p>Min</p>
          <div>{valueSlider[0]}</div>
        </div>
        <span>-</span>
        <div>
          <p>Max</p>
          <div>{valueSlider[1]}</div>
        </div>
      </div>
    );
  };

  const renderButtonApply = () => {
    return (
      <div className={classes["button-apply"]}>
        <button>Apply</button>
        <button>Reset</button>
      </div>
    );
  };

  const renderSlider = () => {
    return (
      <div className={classes["wrapper-render-slider"]}>
        <div className={classes["custom-slider"]}>
          <h1>Price</h1>
          <AirbnbSlider
            ThumbComponent={AirbnbThumbComponent}
            getAriaLabel={(index: any) =>
              index === 0 ? "Minimum price" : "Maximum price"
            }
            onChange={handleChangeValueSlider}
            defaultValue={[20, 40]}
          />
        </div>
        {renderSliderValue()}
        {renderButtonApply()}
      </div>
    );
  };

  const renderFilterResponsive = () => {
    return (
      <div className={classes["div-respon"]}>
        {renderRating()}
        {renderBrand()}
        {renderCateGories()}

        {renderSlider()}
      </div>
    );
  };

  const renderFilterResponsiveICon = () => {
    return (
      <DrawerMenu
        drawerType="bottom"
        listIcon={<PublishIcon />}
        content={renderFilterResponsive}
      />
    );
  };

  function AirbnbThumbComponent(props: any) {
    return <span {...props} />;
  }
  return (
    <>
      <Grid item md={3} lg={3} xl={3} className={classes["filter-product"]}>
        {renderCateGories()}
        {renderBrand()}
        {renderRating()}
        {renderSlider()}
      </Grid>
      <div className={classes["filter-product-responsive"]}>
        {renderFilterResponsiveICon()}
      </div>
    </>
  );
};

const AirbnbSlider = withStyles({
  root: {
    color: "#6A983C",
    height: 6,
    padding: "13px 0",
  },
  thumb: {
    height: 20,
    width: 20,
    backgroundColor: "#fff",
    border: "1px solid #D1D1D1",
    marginTop: -8.5,
    marginLeft: -13,
    boxShadow: "#ebebeb 0 2px 2px",
    "&:focus, &:hover, &$active": {
      boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.15)",
    },
  },
  active: {},
  track: {
    height: 6,
  },
  rail: {
    color: "#EBEBEB;",
    opacity: 1,
    height: 6,
  },
})(Slider);

export default FruitContent;
