import TradeName from "@/components/icons/TradeName";
import { Dialog, DialogTitle } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./styles";
import BlockIcon from "@material-ui/icons/Block";
import { useDispatch } from "react-redux";
import { closeAuthDialogFail } from "@/store/cart/cartSlice";

interface IProps {
  open: boolean;
  message: string;
}

const AuthFail: React.FC<IProps> = ({ open, message }) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleClose = () => {
    dispatch(closeAuthDialogFail(false));
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <div className={classes.modalContainer}>
        <TradeName />
        <h1>{message}</h1>
        <BlockIcon />
      </div>
    </Dialog>
  );
};

export default AuthFail;
