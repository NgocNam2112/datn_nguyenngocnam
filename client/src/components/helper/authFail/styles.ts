import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  modalContainer: {
    width: 500,
    height: 300,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    "& h1": {
      marginTop: 30,
      fontFamily: "Poppins",
      fontWeight: "900",
    },
    "& svg": {
      "&:last-child": {
        width: 150,
        height: 150,
        color: "#e84118",
      },
    },
  },
}));

export default useStyles;
