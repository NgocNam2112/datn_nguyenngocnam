import React from "react";
import useStyles from "./styles";

interface IProps {
  page?: string[];
}

const PageTitle: React.FC<IProps> = ({ page }) => {
  const classes = useStyles();
  return (
    <>
      {page && (
        <div className={classes.pageTitle}>
          <p>{page.join(" / ")}</p>
        </div>
      )}
    </>
  );
};

export default PageTitle;
