import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  pageTitle: {
    padding: "0 45px",
    height: 48,
    display: "flex",
    alignItems: "center",
    "& p": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "12px",
      lineHeight: "16px",
      color: "#D1D1D1",
    },
  },
});

export default useStyles;
