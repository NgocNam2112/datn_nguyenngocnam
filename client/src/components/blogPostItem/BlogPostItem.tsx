/* eslint-disable @next/next/no-img-element */
import React from "react";
import useStyles from "./styles";

interface IProps {
  blogitemimage: string;
  quotetitle: string;
  quotedescription: string;
  date: string;
}

const BlogPostItem: React.FC<IProps> = ({
  blogitemimage,
  quotetitle,
  quotedescription,
  date,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.vegetableBlog}>
      <img src={blogitemimage} alt="" />
      <button>{quotetitle}</button>
      <h2>{quotedescription}</h2>
      <div>
        <span>Author</span>
        <span>{date}</span>
      </div>
    </div>
  );
};

export default BlogPostItem;
