import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  vegetableBlog: {
    maxWidth: 269,
    "& button": {
      margin: "24px 0 8px 0",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "12px",
      lineHeight: "18px",
      color: "#6A983C",
      width: "80px",
      height: "18px",
      background: "#F4F8EC",
      borderRadius: "12px",
      border: "none",
      outline: "none",
      padding: "0 8px",
      cursor: "pointer",
    },
    "& h2": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
      marginTop: "8px",
    },
    "& div": {
      display: "flex",
      alignItems: "center",
      marginTop: 16,
      "& span": {
        marginRight: 16,
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "12px",
        lineHeight: "16px",
        color: "#151515",
      },
    },
  },
  [theme.breakpoints.down(1280)]: {
    vegetableBlog: {
      display: "none",
    },
  },
  [theme.breakpoints.down(924)]: {
    vegetableBlog: {
      display: "block",
    },
  },
  [theme.breakpoints.down(728)]: {
    vegetableBlog: {
      display: "none",
    },
  },
}));

export default useStyles;
