import instance from "@/config/axios.config";
import { USER } from "@/config/config.constant";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import TradeName from "../icons/TradeName";
import useStyles from "./styles";

interface FormValues {
  mail_address: string;
  new_password: string;
  confirm_password: string;
  otp: string;
}

const ForgotPassword = () => {
  const classes = useStyles();
  const router = useRouter();
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<FormValues>();
  const [isSendMail, setIsSendMail] = useState<boolean>(false);
  const [existingError, setExistingError] = useState<string | undefined>();
  const [loading, setLoading] = useState<boolean>(false);

  const handleSendMail = async (mail_address: string) => {
    try {
      setLoading(true);
      setIsSendMail(false);
      const resultSendMail = await instance("", undefined).post(
        `${USER}/forgot-password`,
        { mail_address }
      );
      if (resultSendMail.status === 201) {
        setIsSendMail(true);
        setLoading(false);
      }
    } catch (error: any) {
      setExistingError(error.message);
    }
  };

  const handleResetPassword = async (
    otp: string,
    newPassword: string,
    confirmNewPassword: string
  ) => {
    if (newPassword !== confirmNewPassword) {
      setExistingError(
        "New password must be matched with confirm new password"
      );
      return;
    }
    try {
      setExistingError("");
      const resultReset = await instance("", undefined).put(
        `${USER}/reset-password/${otp}`,
        {
          newPassword: newPassword,
        }
      );
      if (resultReset.status === 200) {
        router.push("/sign-in");
      }
    } catch (error: any) {
      setExistingError(error.message);
    }
  };

  if (!isSendMail) {
    return (
      <form>
        <div className={classes.signIn}>
          <div className={classes.signInform}>
            <TradeName />
            <p>FORGOT PASSWORD</p>
            <div>
              <span>Email: </span>
              <input
                {...register("mail_address", {
                  required: { value: true, message: "Email is not empty" },
                  maxLength: {
                    value: 256,
                    message:
                      "Please enter one or more and 256 characters or less for the email.",
                  },
                  pattern: {
                    value:
                      /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                    message: "Some characters cannot be used.",
                  },
                })}
                type="text"
                placeholder="Email"
              />
              {errors.mail_address && (
                <ErrorMessage
                  errorMessage={errors.mail_address.message as string}
                />
              )}
            </div>
            <button
              onClick={handleSubmit((formData) =>
                handleSendMail(formData.mail_address)
              )}
            >
              Send mail!
            </button>
          </div>
        </div>
      </form>
    );
  }
  return (
    <form>
      <div className={classes.signIn}>
        <div className={classes.signInform}>
          <TradeName />
          <p>FORGOT PASSWORD</p>

          <div>
            <span>Otp: </span>
            <input
              {...register("otp", {
                required: {
                  value: true,
                  message: "Opt is not empty",
                },
              })}
              type="text"
              placeholder="OTP"
            />
            {errors.otp && (
              <ErrorMessage errorMessage={errors.otp.message as string} />
            )}
          </div>

          <div>
            <span>Password: </span>
            <input
              {...register("new_password", {
                required: {
                  value: true,
                  message: "Password is not empty",
                },
                minLength: {
                  value: 8,
                  message: "Please enter the password from 8 to 20 characters.",
                },
                maxLength: {
                  value: 20,
                  message: "Please enter the password from 8 to 20 characters.",
                },
              })}
              type="password"
              placeholder="Password"
            />
            {errors.new_password && (
              <ErrorMessage
                errorMessage={errors.new_password.message as string}
              />
            )}
          </div>
          <div>
            <span>New password: </span>
            <input
              {...register("confirm_password", {
                required: {
                  value: true,
                  message: "Password is not empty",
                },
                minLength: {
                  value: 8,
                  message: "Please enter the password from 8 to 20 characters.",
                },
                maxLength: {
                  value: 20,
                  message: "Please enter the password from 8 to 20 characters.",
                },
              })}
              type="password"
              placeholder="New password"
            />
            {errors.confirm_password && (
              <ErrorMessage
                errorMessage={errors.confirm_password.message as string}
              />
            )}
          </div>
          <button
            onClick={handleSubmit((formData) =>
              handleResetPassword(
                formData.otp,
                formData.new_password,
                formData.confirm_password
              )
            )}
          >
            Reset password
          </button>
        </div>
      </div>
    </form>
  );
};

export default ForgotPassword;
