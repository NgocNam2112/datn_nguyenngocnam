import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  signIn: {
    width: 500,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: "120px auto",
    boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
    borderRadius: "12px",
    paddingBottom: 50,
  },
  signInform: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 50,
    width: "90%",
    boxSizing: "border-box",
    "& p": {
      fontFamily: "Poppins",
      fontWeight: 700,
      fontSize: 20,
    },
    "& div": {
      width: "100%",
      marginBottom: 5,
      "& span": {
        marginLeft: 5,
      },
      "& input": {
        marginTop: 12,
        width: "90%",
        height: 36,
        fontSize: 16,
        fontFamily: "Poppins",
        padding: "0 12px",
        borderRadius: 12,
        border: "1px solid #D1D1D1",
        outline: "none",
      },
    },
    "& button": {
      width: 164,
      height: 47,
      backgroundColor: "#6A983C",
      fontSize: 16,
      fontFamily: "Poppins",
      fontWeight: "700",
      cursor: "pointer",
      border: "none",
      borderRadius: "12px",
      color: "#fff",
      outline: "none",
      "&:focus": {
        outline: "none",
      },
    },
  },
}));

export default useStyles;
