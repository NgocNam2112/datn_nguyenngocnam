/* eslint-disable @next/next/no-img-element */
import { IOderDetail, IOrder, IUser } from "@/config/types";
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { useRouter } from "next/router";
import React from "react";
import useStyles from "./styles";
import Moment from "moment";

interface IProps {
  orderList: IOrder;
  userProfile: IUser;
}

const OrderDetail: React.VFC<IProps> = ({ orderList, userProfile }) => {
  const classes = useStyles();
  const router = useRouter();

  const getTotalOrder = () => {
    let total = 0;
    orderList.order_details.forEach((item) => {
      total += Math.floor(
        (item.quantity *
          item.product.selling_price *
          (100 - item.product.discount)) /
          100
      );
    });
    return total;
  };

  return (
    <Grid item xs={12} md={12} lg={9} sm={12}>
      <div className={classes.orderTitle}>User Info</div>
      <TableContainer component={Paper} className={classes.tableContainer}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>
                {userProfile.first_name} {userProfile.last_name}
              </TableCell>
              <TableCell align="right">{userProfile.phone_number}</TableCell>
              <TableCell align="right">{userProfile.address}</TableCell>
              <TableCell align="right">{userProfile.town}</TableCell>
              <TableCell align="right">
                {Moment(orderList.created_at).format("MMMM Do, YYYY")}
              </TableCell>
            </TableRow>
          </TableHead>
        </Table>
      </TableContainer>
      <div className={classes.orderTitle}>Product</div>
      <TableContainer component={Paper} className={classes.tableContainer}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Image</TableCell>
              <TableCell align="right">Quantity</TableCell>
              <TableCell align="right">Price</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orderList &&
              orderList.order_details &&
              orderList.order_details.map((item: IOderDetail) => (
                <TableRow
                  key={item.id}
                  onClick={() =>
                    router.push(`/product-detail/${item.product.id}`)
                  }
                >
                  <TableCell component="th" scope="row">
                    {item.product.name}
                  </TableCell>
                  <TableCell align="right">
                    <img
                      src={item.product.image}
                      alt="product"
                      style={{ width: 50, height: 50 }}
                    />
                  </TableCell>
                  <TableCell align="right">{item.quantity}</TableCell>
                  <TableCell align="right">
                    {Math.floor(
                      item.product.selling_price * (100 - item.product.discount)
                    ) / 100}
                    USD
                  </TableCell>
                </TableRow>
              ))}

            <TableRow>
              <TableCell component="th" scope="row"></TableCell>
              <TableCell align="right"></TableCell>
              <TableCell align="right"></TableCell>
              <TableCell align="right">Total: {getTotalOrder()} USD</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Grid>
  );
};

export default OrderDetail;
