import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
    boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
  },
  tableContainer: {
    boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
  },
  orderTitle: {
    fontFamily: "Poppins",
    fontWeight: "bold",
    padding: "20px 0",
  },
}));

export default useStyles;
