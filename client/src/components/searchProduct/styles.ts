import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "0 45px",
  },
  "wrapper-menu": {
    margin: "64px 0 0 0 !important",
    width: "100% !important",
    "& > div": {
      padding: "0 !important",
    },
  },
  menu: {
    "& h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
    },
    "& ul": {
      marginTop: 16,
      "& li": {
        listStyle: "none",
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        textDecorationLine: "underline",
        color: "#6A983C",
        marginBottom: 12,
        cursor: "pointer",
      },
    },
    "& button": {
      width: "179px",
      height: "48px",
      background: "#F5F5F5",
      borderRadius: "12px",
      outline: "none",
      border: "none",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      cursor: "pointer",
      "& svg": {
        marginLeft: 12.53,
      },
      marginTop: 36,
      marginBottom: 64,
    },
  },
  "wrapper-block": {
    display: "flex",
    justifyContent: "space-between",
  },
  "space-item": {
    position: "relative",
    width: "48%",
    height: 280,
    background: "#F4F8EC",
    borderRadius: "12px",
    marginBottom: 66,
    "& p": {
      position: "absolute",
      top: 48,
      left: 33,
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "12px",
      lineHeight: "18px",
      color: "#6A983C",
    },
    "& h1": {
      position: "absolute",
      top: 74,
      left: 33,
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 900,
      fontSize: "22px",
      lineHeight: "33px",
      color: "#151515",
    },
    "& button": {
      position: "absolute",
      top: 201,
      left: 33,
      width: "163px",
      height: "47px",
      border: "2px solid #92C064",
      boxSizing: "border-box",
      borderRadius: "12px",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      outline: "none",
      cursor: "pointer",
    },
  },
  "first-heading": {
    display: "block",
  },
  "second-heading": {
    display: "block",
  },
  [theme.breakpoints.down(1280)]: {
    menu: {
      display: "none",
    },
  },
  [theme.breakpoints.down(924)]: {
    "wrapper-blog-list": {
      display: "none",
    },
    "second-heading": {
      display: "none",
    },
    "first-heading": {
      width: "100%",
    },
  },
  [theme.breakpoints.down(870)]: {
    menu: {
      display: "none",
    },
  },
}));
export default useStyles;
