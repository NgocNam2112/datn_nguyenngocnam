import { IPayloadProduct } from "@/pages/api/types";
import { Grid } from "@material-ui/core";
import React, { useState } from "react";
import RightArrowBlack from "../icons/Arrows/RightArrowBlack";
import useStyles from "./styles";
import ContentPagination from "../helper/contentPagination/ContentPagination";
import { IProduct } from "@/config/types";
import { Pagination } from "@material-ui/lab";
import { useRouter } from "next/router";

interface Data {
  h1: string;
  li: Record<string, any>;
}

interface IProps {
  productList: IProduct[];
}

const SearchProduct: React.FC<IProps> = ({ productList }) => {
  const classes = useStyles();
  const router = useRouter();
  const [page, setPage] = React.useState(1);
  const [paginationProduct, setPaginationProduct] = useState();
  const [pageNumber, setPageNumber] = useState<number>(1);

  const catalogGridView = (value: Data) => {
    return (
      <div>
        <h1>{value.h1}</h1>
        <ul>
          {value.li.map((item: string, index: number) => {
            return <li key={index}>{item}</li>;
          })}
        </ul>
        <button>
          More categories
          <RightArrowBlack />
        </button>
      </div>
    );
  };

  const handleChangePage = (event: any, newPage: number) => {
    if (productList) {
      setPage(newPage);
      setPaginationProduct(
        productList.slice((newPage - 1) * 8, newPage * 8) as any
      );
      router.push(`?page=${newPage}`);
    }
  };
  if (productList.length) {
    return (
      <Grid item xs={12} md={12} lg={9} sm={12}>
        <div className={classes.root}>
          <ContentPagination
            products={productList}
            setPageNumber={setPageNumber}
          />
        </div>
        {productList.length > 8 && (
          <div style={{ margin: "0 auto" }}>
            <Pagination count={10} page={page} onChange={handleChangePage} />
          </div>
        )}
      </Grid>
    );
  }
  return <h1>No product match with that key</h1>;
};

export default SearchProduct;
