/* eslint-disable @next/next/no-img-element */
import { IBuyType, IDescription, IProduct, IReview } from "@/config/types";
import { Grid, Typography } from "@material-ui/core";
import React, { useState } from "react";
import AddProductIcon from "../icons/AddProductIcon";
import DownArrowBlack from "../icons/Arrows/DownArrowBlack";
import RingStar from "../icons/RingStar";
import Stars from "../icons/Stars";
import useStyles from "./styles";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Moment from "moment";
import { addNewReview, removeUserReview } from "@/store/review/reviewActions";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

interface IDetailsProduct {
  title: string;
  details: string[];
}
interface IProductAddToCart {
  buyTypeId: number;
  quantity: number;
}

interface IProps {
  descriptionByProductId: IDescription;
  productList: IProduct;
  reviewList: IReview[];
  buyTypeList: IBuyType[];
  handleAddProductToCart: (buyTypeId: number, quantity: number) => any;
  productId: string;
}

const ProductDetail: React.VFC<IProps> = ({
  descriptionByProductId,
  productList,
  reviewList,
  buyTypeList,
  handleAddProductToCart,
  productId,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [selectOption, setSelectOption] = useState<String>("des");
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorElBuyType, setAnchorElBuyType] = useState(null);
  const [unitItem, setUnitItem] = useState(buyTypeList && buyTypeList[0]);
  const [quantity, setQuantity] = useState(1);
  const [reviewContent, setReviewContent] = useState<string | undefined>();

  const renderProductImage = () => {
    return (
      <>
        <Grid
          item
          xs={12}
          lg={6}
          md={12}
          className={classes.wrapperDetailsPage}
        >
          <div className={classes.customImageProduct}>
            <img src={productList.image} alt="product detail" />
            <div>
              <span>{productList.discount}</span>
              <span>Free shipping</span>
            </div>
          </div>
          <div>
            <img
              src={descriptionByProductId.first_image}
              alt="product detail"
            />
            <img
              src={descriptionByProductId.second_image}
              alt="product detail"
            />
          </div>
          {/* <ThumbImageProduct /> */}
        </Grid>
      </>
    );
  };

  const renderStar = (stars: number, ringStars: number | undefined) => {
    let arr = [];
    for (let i = 0; i < stars; i++) {
      if (i < (ringStars as number)) arr.push(<RingStar />);
      else arr.push(<Stars />);
    }
    return arr;
  };

  const methodAndFormSale = (option: any, productData: any) => {
    return (
      <div>
        <div>
          {option.map((item: string, index: number) => {
            return <span key={index}>{item}</span>;
          })}
        </div>
        <div>
          {productData
            ? productData.map((item: string, index: number) => {
                return <span key={index}>{item}</span>;
              })
            : null}
        </div>
      </div>
    );
  };

  const renderProductRelated = () => {
    return (
      <div>
        <h1>{productList.name}</h1>
        <div className={classes.customReview}>
          {renderStar(5, productList.rate)}
          <p>({reviewList.length} customer review)</p>
        </div>
        <p className={classes.customIntroduceProduct}>{productList.content}</p>
      </div>
    );
  };

  const handleClickBuyType = (event: any) => {
    setAnchorElBuyType(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorElBuyType(null);
  };

  const handleSelectUnitItem = (item: IBuyType) => {
    setUnitItem(item);
  };
  const handleChangeQuantity = (e: any) => {
    setQuantity(+e.target.value);
  };

  const renderPrice = () => {
    return (
      <>
        <div className={classes.productPrice}>
          <div>
            <h2>
              {Math.round(
                productList.selling_price * (100 - productList.discount)
              ) / 100}
              <span style={{ marginLeft: 5 }}>USD</span>
            </h2>
            <p>
              {productList.selling_price}
              <span style={{ marginLeft: 5 }}>USD</span>
            </p>
          </div>
          <div className={classes.pcs}>
            <input
              type="number"
              value={quantity}
              onChange={(e) => handleChangeQuantity(e)}
              className={classes.quantityProduct}
            />
            <div className={classes.pcsMenu}>
              <Typography
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClickBuyType}
              >
                <span>
                  {unitItem.unit_type}
                  <DownArrowBlack />
                </span>
              </Typography>
              <Menu
                id="simple-menu"
                anchorEl={anchorElBuyType}
                keepMounted
                open={Boolean(anchorElBuyType)}
                onClose={handleCloseMenu}
              >
                {buyTypeList &&
                  buyTypeList.map((item) => {
                    return (
                      <MenuItem
                        onClick={() => {
                          handleCloseMenu();
                          handleSelectUnitItem(item);
                        }}
                        key={item.id}
                      >
                        {item.unit_type}
                      </MenuItem>
                    );
                  })}
              </Menu>
            </div>
          </div>
          <button
            onClick={() => {
              handleAddProductToCart(+unitItem.id || 1, quantity);
            }}
          >
            <AddProductIcon />
            Add to cart
          </button>
        </div>
      </>
    );
  };

  const nutritionProduction = (option: IDetailsProduct) => {
    return (
      <div className={classes.customDetailsProduct}>
        <h3>{option.title}</h3>
        <ul>
          {option.details.map((item: any, index: number) => {
            return <li key={index}>{item}</li>;
          })}
        </ul>
      </div>
    );
  };

  const funcChangeValue = (
    setState: (value: string) => void,
    value: string
  ) => {
    setState(value);
  };

  const handleClickReivew = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseReviewOption = () => {
    setAnchorEl(null);
  };

  const submitReview = async (review: string) => {
    if (reviewContent) {
      dispatch(addNewReview({ content: review, productId: +productId }));
      setReviewContent("");
    }
  };

  const handleChangeReview = (e: any) => {
    setReviewContent(e.target.value);
  };

  const reviewBlock = () => {
    return (
      <div className={classes.reviewDetail}>
        <div className={classes.inputReview}>
          <input
            onChange={(e) => handleChangeReview(e)}
            type="text"
            value={reviewContent}
            placeholder={"Please comment here!"}
          />
          <button onClick={() => submitReview(reviewContent as string)}>
            Comment!
          </button>
        </div>
        {reviewList.length ? (
          reviewList.map((item) => {
            return (
              <div className={classes.userReviews} key={item.id}>
                <div>
                  <div>
                    <img src={item.user.avatar} alt="user-image" />
                    <div>
                      <span>{`${item.user.first_name} ${item.user.last_name}`}</span>
                      <span>
                        {Moment(item.updated_at).format("MMMM Do, YYYY")}
                      </span>
                    </div>
                  </div>
                  <div>
                    <Button
                      aria-controls="simple-menu"
                      aria-haspopup="true"
                      onClick={handleClickReivew}
                    >
                      <MoreVertIcon />
                    </Button>
                    <Menu
                      id="simple-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleCloseReviewOption}
                    >
                      <MenuItem
                        onClick={() => {
                          dispatch(
                            removeUserReview({
                              reviewId: item.id,
                              productId: +productId,
                            })
                          );
                          handleCloseReviewOption();
                        }}
                      >
                        <span style={{ color: "red" }}>Delete</span>
                      </MenuItem>
                    </Menu>
                  </div>
                </div>
                <div>{item.content}</div>
              </div>
            );
          })
        ) : (
          <h1>Please leave your comment here...!</h1>
        )}
      </div>
    );
  };

  const renderProductReview = () => {
    return (
      <div className={classes.selectOptionRelatedProduct}>
        <ul>
          <li
            onClick={() => funcChangeValue(setSelectOption, "des")}
            style={
              selectOption === "des"
                ? { borderBottom: "1px solid #6A983C" }
                : { borderBottom: "1px solid #F5F5F5" }
            }
          >
            Description
          </li>
          <li
            onClick={() => funcChangeValue(setSelectOption, "rev")}
            style={
              selectOption === "rev"
                ? { borderBottom: "1px solid #6A983C" }
                : { borderBottom: "1px solid #F5F5F5" }
            }
          >
            Reviews <span>{reviewList.length}</span>
          </li>
          <li
            onClick={() => funcChangeValue(setSelectOption, "ques")}
            style={
              selectOption === "ques"
                ? { borderBottom: "1px solid #6A983C" }
                : { borderBottom: "1px solid #F5F5F5" }
            }
          ></li>
        </ul>
        {selectOption === "des" ? (
          <div className={classes.productDetails}>
            <h3>Origins</h3>
            <p>{descriptionByProductId.origin}</p>
            <h3>How to cook</h3>
            <p>{descriptionByProductId.cook}</p>
            <h3>Full of Vitamins!</h3>
            <h3>{descriptionByProductId.vitamins}</h3>
          </div>
        ) : selectOption === "rev" ? (
          reviewBlock()
        ) : null}
      </div>
    );
  };

  const renderProductInfo = () => {
    return (
      <Grid item xs={12} lg={6} md={12} className={classes.wrapperProductInfor}>
        {renderProductRelated()}
        {renderPrice()}
        {renderProductReview()}
      </Grid>
    );
  };

  const renderProductDetails = () => {
    return (
      <>
        <Grid container item xs={12} className={classes.containerProduct}>
          {renderProductImage()}
          {renderProductInfo()}
        </Grid>
      </>
    );
  };

  return (
    <>
      <div>
        {productList && descriptionByProductId && (
          <div className={classes.root}>{renderProductDetails()}</div>
        )}
      </div>
    </>
  );
};

export default ProductDetail;
