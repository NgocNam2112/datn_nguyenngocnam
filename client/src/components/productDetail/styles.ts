import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "0 45px",
  },
  containerProduct: {
    display: "flex",
    justifyContent: "space-between",
  },
  wrapperDetailsPage: {
    display: "flex",
    flexDirection: "column",
    padding: "16px 0",
    "& > svg": {
      marginTop: 32,
    },
    "& img": {
      marginBottom: 32,
      width: 569,
      height: 436,
      borderRadius: 12,
    },
  },
  customImageProduct: {
    position: "relative",
    "& div": {
      position: "absolute",
      top: 16,
      left: 16,
      "& span": {
        padding: "4px 10px",
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#6A983C",
        marginRight: 12,
      },
    },
  },
  wrapperProductInfor: {
    "& > div": {
      "& h1": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: "32px",
        lineHeight: "140%",
        color: "#151515",
        marginBottom: 10,
      },
      "& > p": {
        marginTop: 42,

        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "17px",
        lineHeight: "23px",
        color: "#151515",
      },
    },
  },
  customReview: {
    display: "flex",
    "& p": {
      margin: "0 0 0 10px",
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      lineHeight: "16px",
      color: "#A9A9A9",
    },
  },
  customIntroduceProduct: {
    textAlign: "justify",
  },
  relatedProduct: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 40,
    "& > div": {
      display: "flex",
      justifyContent: "space-between",
      "& div": {
        width: "60%",
        display: "flex",
        flexDirection: "column",
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "16px",
        lineHeight: "19px",
        "&:first-child": {
          width: 120,
          "& span": {
            color: "#A9A9A9",
          },
        },
        "& span": {
          marginBottom: 12,
        },
      },
    },
  },

  detailsProductCoponent: {
    marginTop: 40,
    display: "flex",
    flexDirection: "column",
    marginRight: 39,
    "& span": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      color: "#A9A9A9",
      marginBottom: 12,
    },
    "& p": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      color: "#151515",
      marginBottom: 12,
    },
  },
  category: {
    textDecoration: "underline",
  },
  stock: {
    color: "#6A983C !important",
  },
  productPrice: {
    height: 89,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: 40,
    "& > div": {
      "& h2": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "26px",
        lineHeight: "39px",
        color: "#6A983C",
      },
      "& p": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        display: "flex",
        alignItems: "flex-end",
        textDecorationLine: "line-through",
        color: "#A9A9A9",
      },
    },
    "& button": {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      padding: "12px 16px",
      background: "#6A983C",
      border: "2px solid #46760A",
      boxSizing: "border-box",
      borderRadius: "12px",
      outline: "none",
      cursor: "pointer",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#FFFFFF",
      fontWeight: "bold",
    },
  },
  quantityProduct: {
    outline: "none",
    maxWidth: "50px",
    border: "none",
    backgroundColor: "#F9F9F9",
  },
  pcs: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: "14px 16px",
    background: "#F9F9F9",
    border: "1px solid #D1D1D1",
    boxSizing: "border-box",
    borderRadius: "12px",
    cursor: "pointer",
    position: "relative",
    "& span": {
      color: "#D1D1D1",
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      borderRight: "1px solid #D1D1D1",
      paddingRight: 16,
      "&:last-child": {
        marginLeft: 16,
        color: "#151515",
        borderRight: "none",
        "& svg": {
          marginBottom: 2,
          marginLeft: 7.61,
        },
      },
    },
  },
  pcsMenu: {
    "& p": {
      textDecoration: "none !important",
    },
  },
  customButtonProduct: {
    height: 35,
    marginTop: 24,
    display: "flex",
    "& button": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      height: "100%",
      display: "flex",
      alignItems: "center",
      paddingRight: 12,
      background: "#FFFFFF",
      borderRadius: "12px",
      outline: "none",
      border: "none",
      cursor: "pointer",
      marginRight: 16,
      "& svg": {
        marginRight: 7.33,
      },
    },
  },
  selectOptionRelatedProduct: {
    marginTop: 62,
    "& ul": {
      display: "flex",
      justifyContent: "space-between",
      "& li": {
        width: "50%",
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "18px",
        lineHeight: "27px",
        color: "#151515",
        listStyle: "none",
        padding: "16px 0",
        cursor: "pointer",
        "& span": {
          background: "#F4F8EC",
          borderRadius: "12px",
          fontFamily: "Poppins",
          fontStyle: "normal",
          fontWeight: "600",
          fontSize: "12px",
          lineHeight: "18px",
          color: "#6A983C",
          padding: "0 8px",
          marginLeft: 8,
        },
      },
    },
  },
  productDetails: {
    "& h3": {
      "&:first-child": {
        marginTop: 48,
      },
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 800,
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      marginTop: 32,
    },
    "& p": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "500",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      marginTop: 8,
      textAlign: "justify",
    },
  },
  reviewDetail: {
    display: "flex",
    flexDirection: "column",
  },
  inputReview: {
    width: "100%",
    display: "flex",
    "& input": {
      width: "100%",
      border: "1px solid #D1D1D1",
      height: 42,
      outline: "none",
      padding: "0px 12px",
      borderRadius: "12px",
    },
    "& button": {
      border: "2px solid #46760A",
      background: "#6A983C",
      borderRadius: "12px",
      color: "#FFFFFF",
      width: 130,
      marginLeft: 20,
    },
  },
  userReviews: {
    marginTop: 24,
    fontSize: 12,
    borderBottom: "1px solid #D1D1D1",
    paddingBottom: 8,
    fontFamily: "Poppins",
    boxSizing: "border-box",
    "& > div": {
      fontWeight: "900",
      "&:first-child": {
        display: "flex",
        justifyContent: "space-between",
        "& div": {
          boxSizing: "border-box",
          display: "flex",
          "& img": {
            width: 48,
            height: 48,
            borderRadius: "50%",
          },
          "& div": {
            marginLeft: 12,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          },
        },
      },
      "&:last-child": {
        marginLeft: 20,
        marginTop: 8,
      },
    },
  },
  wrapperDetailsProduct: {
    display: "flex",
    justifyContent: "space-between",
  },
  customDetailsProduct: {
    display: "flex",
    flexDirection: "column",
    width: "34%",
    margin: "0 auto",
    "& h3": {
      marginTop: 23,
      paddingBottom: 7,
      borderBottom: "1px solid #EBEBEB",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "12px",
      lineHeight: "18px",
      color: "#151515",
      marginBottom: 11,
    },
    "& ul": {
      display: "flex",
      flexDirection: "column",
      "& li": {
        width: "100%",
        fontFamily: "Open Sans",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "14px",
        lineHeight: "19px",
        color: "#575757",
        marginBottom: 21,
        padding: 0,
      },
    },
  },
  titleSlide: {
    marginTop: 70,
    marginBottom: 11.5,
    display: "flex",
    justifyContent: "space-between",
    "& h2": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
    },
    "& button": {
      height: "35px",
      background: "#FFFFFF",
      borderRadius: "12px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      outline: "none",
      border: "none",
      cursor: "pointer",
      "& svg": {
        marginLeft: 12.53,
      },
    },
  },
  listItem: {
    marginTop: 37,
    display: "flex",
    justifyContent: "space-between",
  },
  [theme.breakpoints.down(1280)]: {
    containerProduct: {
      flexDirection: "column",
    },
    wrapperDetailsPage: {
      flexDirection: "row",
      margin: "0 auto",
      "& > div": {
        "&:last-child": {
          display: "flex",
          flexDirection: "column",
          width: "40%",
          "& img": {
            width: "100%",
            height: "50%",
            padding: "0 0 0 4%",
            boxSizing: "border-box",
            marginBottom: 0,
            "&:first-child": {
              paddingBottom: "4%",
            },
            "&:last-child": {
              paddingTop: "4%",
            },
          },
        },
      },
    },
    customImageProduct: {
      width: "60%",
      "& img": {
        width: "100%",
        height: "100%",
      },
    },
    wrapperProductInfor: {
      margin: "0 auto",
    },
  },
}));

export default useStyles;
