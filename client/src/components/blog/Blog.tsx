import { Grid } from "@material-ui/core";
import React, { useState } from "react";
import BlogPostItem from "../blogPostItem";
import AdormentInput from "../helper/adormentInput/AdormentInput";
import Footer from "../helper/footer";
import PageTitle from "../helper/pageTitle";
import DownWhiteArrow from "../icons/Arrows/DownWhiteArrow";
import GridView from "../icons/GridView";
import ListView from "../icons/ListView";
import Subscribe from "../icons/Subscribe";
import SpaceForBlog from "../spaceForBlog";
import useStyles from "./styles";

const Blog = () => {
  const classes = useStyles();
  const [gridView, setGridView] = useState<boolean>(false);

  const blogList = [
    {
      h1: "Archives",
      list: [
        "March 2020",
        "February 2020",
        "January 2020",
        "November 2019",
        "December 2019",
      ],
    },
    {
      h1: "Category",
      list: [
        "Food",
        "Chefs specialities",
        "Vegetable",
        "Meat",
        "Recommendations",
      ],
    },
  ];

  const handleChangeListView = () => {
    setGridView(true);
  };

  const renderBlogTitle = () => {
    return (
      <Grid container item xs={12} className={classes["page-title"]}>
        <h1>Fruit and vegetables</h1>
        <div className={classes["page-sort"]}>
          <div onClick={handleChangeListView}>
            <GridView />
            Grid View
          </div>
          <div onClick={handleChangeListView}>
            <ListView />
            List View
          </div>
          <div>
            <p>117</p>
            <p>Products</p>
          </div>
        </div>
      </Grid>
    );
  };

  const renderMainBlog = () => {
    return (
      <>
        <PageTitle page={["Home", "Fruit and vegetables", "Product title"]} />
        <Grid
          container
          item
          xs={12}
          style={{ justifyContent: "space-between" }}
        >
          <SpaceForBlog
            mainblog={"/BlogImage.png"}
            tags="tags"
            blogheadline="Our chef tips for a great and tasty dinner ready in 20 minutes"
            avatarauthor={"/Ellipse.png"}
            date="17. 6. 2020"
          />
          <SpaceForBlog
            mainblog={"/BlogImage.png"}
            tags="tags"
            blogheadline="Our chef tips for a great and tasty dinner ready in 20 minutes"
            avatarauthor={"/Ellipse.png"}
            date="17. 6. 2020"
          />
        </Grid>
      </>
    );
  };

  const renderBlogListCategory = () => {
    return (
      <Grid item xs={2} className={classes["list-blog-category"]}>
        {blogList.map((item, index: number) => {
          return (
            <div key={index}>
              <h1>{item.h1}</h1>
              <ul>
                {item.list.map((i, ind: number) => {
                  return <li key={ind}>{i}</li>;
                })}
              </ul>
            </div>
          );
        })}
        <div className={classes["subscribe-to-read-blog"]}>
          <h1>Join our list</h1>
          <p>
            Signup to be the first to hear about exclusive deals, special
            offers, recepies from our masters and others.
          </p>
          <AdormentInput
            placeholder="Your email address"
            element={<Subscribe />}
          />
        </div>
      </Grid>
    );
  };

  const renderBlogListItem = () => {
    return (
      <Grid item xs={9} className={classes["wrapper-blog-list-item"]}>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
        <div>
          <BlogPostItem
            blogitemimage={"/Vegetable.png"}
            quotetitle="Vegetable"
            quotedescription="Which vegetable your family will love and want’s eat each day"
            date="15. 6. 2020"
          />
        </div>
      </Grid>
    );
  };

  const renderListBlog = () => {
    return (
      <Grid
        container
        item
        xs={12}
        style={{ display: "flex", justifyContent: "space-between" }}
      >
        {renderBlogListCategory()}
        {renderBlogListItem()}
      </Grid>
    );
  };

  const renderMoreProduct = () => {
    return (
      <Grid item xs={12} className={classes["pagnition-page"]}>
        <div>
          <p>Page</p>
          <p>1</p>
          <p>2</p>
          <p>3</p>
          <p>4</p>
        </div>
        <button>
          Show more products <DownWhiteArrow />
        </button>
        <div>
          <span>336</span>
          <span>Products</span>
        </div>
      </Grid>
    );
  };
  return (
    <div className={classes["root"]}>
      {renderBlogTitle()}
      {renderMainBlog()}
      {renderListBlog()}
      {renderMoreProduct()}
      <Footer />
    </div>
  );
};

export default Blog;
