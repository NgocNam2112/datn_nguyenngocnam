import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  root: {
    padding: "0 45px",
  },
  "page-title": {
    height: 64,
    display: "flex",
    justifyContent: "space-between",

    "& > h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "32px",
      lineHeight: "48px",
      color: "#151515",
    },
  },
  "page-sort": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "auto 0",
    width: 275,
    height: 18,
    "& div": {
      display: "flex",
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "12px",
      lineHeight: "16px",
      color: "#151515",
      cursor: "pointer",
      "& p": {
        marginLeft: 12,
        "&:first-child": {
          color: "#6A983C",
        },
      },
      "& svg": {
        marginRight: 5.33,
      },
    },
  },
  "list-blog-category": {
    padding: "48px 0",
    "& div": {
      marginBottom: 48,
      "& h1": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "18px",
        lineHeight: "27px",
        color: "#151515",
        marginBottom: 4,
      },
      "& ul": {
        listStyle: "none",
        "& li": {
          marginTop: 12,
          fontFamily: "Open Sans",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "14px",
          lineHeight: "19px",
          textDecorationLine: "underline",
          color: "#6A983C",
        },
      },
      "&:last-child": {
        "& ul": {
          "& li": {
            color: "#A9A9A9",
          },
        },
      },
    },
  },
  "subscribe-to-read-blog": {
    "& h1": {
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "18px",
      lineHeight: "27px",
      color: "#151515",
    },
    "& p": {
      fontFamily: "Open Sans",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19px",
      color: "#A9A9A9",
      marginTop: 8,
      marginBottom: 24,
    },
  },
  "wrapper-blog-list-item": {
    display: "grid",
    gridTemplateColumns: "auto auto auto auto",
    justifyContent: "space-between",
    flexWrap: "wrap",
    padding: "48px 0",
    "& > div": {
      marginBottom: 32,
    },
  },
  "pagnition-page": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 64,
    padding: "16px 0",
    "& div": {
      "& span": {
        fontFamily: "Poppins",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: "12px",
        lineHeight: "18px",
        color: "#A9A9A9",
        marginLeft: 12,
        "&:first-child": {
          color: "#6A983C",
        },
      },
      "&:first-child": {
        display: "flex",
        "& p": {
          marginRight: 8,
          fontFamily: "Open Sans",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "12px",
          lineHeight: "16px",
          color: "#151515",
          "&:last-child": {
            color: "#6A983C",
          },
        },
      },
    },
    "& button": {
      width: "214px",
      height: "47px",
      background: "#6A983C",
      border: "2px solid #46760A",
      boxSizing: "border-box",
      borderRadius: "12px",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#FFFFFF",
      outline: "none",
      cursor: "pointer",
    },
  },
});

export default useStyles;
