import React from "react";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import useStyles from "./styles";
import { IPayloadCatalog } from "@/pages/api/types";

interface IState {
  top?: boolean;
  bottom?: boolean;
  left?: boolean;
  right?: boolean;
}
interface IProps {
  listIcon?: JSX.Element;
  drawerType: string;
  listContent?: Array<IPayloadCatalog>;
  content?: () => JSX.Element;
  blockContent?: () => JSX.Element;
}

const DrawerMenu: React.FC<IProps> = ({
  listIcon,
  drawerType,
  listContent,
  blockContent,
  content,
}) => {
  const [state, setState] = React.useState<IState>({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const classes = useStyles();

  const toggleDrawer = (anchor: string, open: boolean) => (event: any) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor: any) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
    >
      {content ? content() : undefined}
    </div>
  );

  return (
    <div>
      <React.Fragment key={drawerType}>
        <Button
          onClick={toggleDrawer(drawerType, true)}
          style={{ background: "#ecf0f1" }}
        >
          {listIcon}
          {blockContent && blockContent()}
        </Button>
        <Drawer
          anchor={drawerType as any}
          open={state[drawerType as keyof IState]}
          onClose={toggleDrawer(drawerType, false)}
        >
          {list(drawerType)}
        </Drawer>
      </React.Fragment>
    </div>
  );
};

export default DrawerMenu;
