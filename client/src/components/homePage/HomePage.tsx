/* eslint-disable @next/next/no-img-element */
import { IProduct } from "@/config/types";
import { Grid } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import ContentPagination from "../helper/contentPagination/ContentPagination";
import RightArrowBlack from "../icons/Arrows/RightArrowBlack";
import useStyles from "./styles";
import Pagination from "@material-ui/lab/Pagination";
import { useRouter } from "next/router";

interface IProps {
  productList: IProduct[];
}

interface Data {
  h1: string;
  li: Record<string, any>;
}

const HomePage: React.VFC<IProps> = ({ productList }) => {
  const classes = useStyles();
  const router = useRouter();
  const [page, setPage] = React.useState(1);

  const [pageNumber, setPageNumber] = useState<number>(1);
  const [paginationProduct, setPaginationProduct] = useState();

  const spaceForHeading = () => {
    return (
      <div className={`${classes.spaceItem} ${classes.heading}`}>
        <p>Banner subfocus</p>
        <h1>Space for heading</h1>
        <button>Read recepies</button>
      </div>
    );
  };

  const catalogGridView = (value: Data) => {
    return (
      <div>
        <h1>{value.h1}</h1>
        <ul>
          {value.li.map((item: string, index: number) => {
            return <li key={index}>{item}</li>;
          })}
        </ul>
        <button>
          More categories
          <RightArrowBlack />
        </button>
      </div>
    );
  };

  const handleChangePage = (event: any, newPage: number) => {
    if (productList) {
      setPage(newPage);
      setPaginationProduct(
        productList.slice((newPage - 1) * 8, newPage * 8) as any
      );
      router.push(`?page=${newPage}`);
    }
  };

  useEffect(() => {
    if (productList) {
      setPaginationProduct(productList.slice(0, 8) as any);
    }
  }, [productList]);

  return (
    <>
      <Grid item xs={12} md={12} lg={9} sm={12}>
        {/* <Grid item xs={12} className={classes.wrapperBlock}>
          {spaceForHeading()}
          {spaceForHeading()}
        </Grid> */}
        {productList && paginationProduct && (
          <ContentPagination
            products={paginationProduct}
            setPageNumber={setPageNumber}
          />
        )}
        <div style={{ margin: "0 auto" }}>
          <Pagination count={10} page={page} onChange={handleChangePage} />
        </div>
      </Grid>
    </>
  );
};

export default HomePage;
