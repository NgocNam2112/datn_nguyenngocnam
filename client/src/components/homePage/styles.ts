import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "0 45px",
  },
  "space-for-heading": {
    display: "flex",
    justifyContent: "space-between",
  },
  spaceItem: {
    position: "relative",
    width: "48%",
    height: 280,
    background: "#F4F8EC",
    borderRadius: "12px",
    marginBottom: 66,
    "& p": {
      position: "absolute",
      top: 48,
      left: 33,
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "12px",
      lineHeight: "18px",
      color: "#6A983C",
    },
    "& h1": {
      position: "absolute",
      top: 74,
      left: 33,
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: 900,
      fontSize: "22px",
      lineHeight: "33px",
      color: "#151515",
    },
    "& button": {
      position: "absolute",
      top: 201,
      left: 33,
      width: "163px",
      height: "47px",
      border: "2px solid #92C064",
      boxSizing: "border-box",
      borderRadius: "12px",
      fontFamily: "Poppins",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "15px",
      lineHeight: "22px",
      color: "#151515",
      outline: "none",
      cursor: "pointer",
    },
  },
  heading: {
    display: "block",
  },
  wrapperBlock: {
    display: "flex",
    justifyContent: "space-between",
  },
  [theme.breakpoints.down(1280)]: {
    menu: {
      display: "none",
    },
  },
  [theme.breakpoints.down(924)]: {
    "wrapper-blog-list": {
      display: "none",
    },
    heading: {
      width: "100%",
    },
  },
}));

export default useStyles;
