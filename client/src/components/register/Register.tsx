/* eslint-disable @next/next/no-html-link-for-pages */
import instance from "@/config/axios.config";
import { BASE_URL, USER } from "@/config/config.constant";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import TradeName from "../icons/TradeName";
import useStyles from "./styles";
import CircularProgress from "@material-ui/core/CircularProgress";

interface FormValues {
  mail_address: string;
  password: string;
  first_name: string;
  last_name: string;
}

const Register = () => {
  const classes = useStyles();
  const router = useRouter();
  const [files, setFiles] = useState<any>();
  const [loading, setLoading] = useState(false);
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<FormValues>();
  const onSubmit: SubmitHandler<FormValues> = async (formData) => {
    const form = new FormData();
    form.append("mail_address", formData.mail_address);
    form.append("password", formData.password);
    form.append("first_name", formData.first_name);
    form.append("last_name", formData.last_name);
    form.append("rolesId", "3");
    if (files) {
      form.append("files", files);
    }
    try {
      setLoading(true);
      const result = await axios.post(`${BASE_URL}users/register`, form, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      setLoading(false);

      window.localStorage.setItem("accessToken", result.data.accessToken);
      router.push("/");
    } catch (error) {
      console.log("error", error);
    }
  };

  const onChangeFile = (e: any) => {
    setFiles(e.target.files[0]);
  };

  if (loading) {
    return (
      <div className={classes.circular}>
        <CircularProgress />
      </div>
    );
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={classes.signIn}>
        <div className={classes.formSignUp}>
          <TradeName />
          <p className={classes.title}>Sign Up </p>
          <div className={classes.wrapCustomInputField}>
            <div className={classes.customInputField}>
              <span>First name: </span>
              <input
                {...register("first_name", {
                  required: {
                    value: true,
                    message: "Please enter your first name",
                  },
                  maxLength: {
                    value: 50,
                    message: "Enter the name from 1 to 50 characters.",
                  },
                })}
                type="text"
                placeholder="First name"
              />
              {errors.first_name && (
                <ErrorMessage
                  errorMessage={errors.first_name.message as string}
                />
              )}
            </div>
            <div className={classes.customInputField}>
              <span>Last name: </span>
              <input
                {...register("last_name", {
                  required: {
                    value: true,
                    message: "Please enter your last name",
                  },
                  maxLength: {
                    value: 50,
                    message: "Enter the name from 1 to 50 characters.",
                  },
                })}
                type="text"
                placeholder="Last name"
              />
              {errors.last_name && (
                <ErrorMessage
                  errorMessage={errors.last_name.message as string}
                />
              )}
            </div>
          </div>
          <div className={classes.inputField}>
            <span>Email: </span>
            <input
              {...register("mail_address", {
                required: { value: true, message: "Email is not empty" },
                maxLength: {
                  value: 256,
                  message:
                    "Please enter one or more and 256 characters or less for the email.",
                },
                pattern: {
                  value:
                    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message: "Some characters cannot be used.",
                },
              })}
              type="text"
              placeholder="Email"
            />
            {errors.mail_address && (
              <ErrorMessage
                errorMessage={errors.mail_address.message as string}
              />
            )}
          </div>
          <div className={classes.inputField}>
            <span>Password: </span>
            <input
              {...register("password", {
                required: {
                  value: true,
                  message: "Password is not empty",
                },
                minLength: {
                  value: 6,
                  message: "Please enter the password from 8 to 20 characters.",
                },
                maxLength: {
                  value: 20,
                  message: "Please enter the password from 8 to 20 characters.",
                },
              })}
              type="password"
              placeholder="Password"
            />
            {errors.password && (
              <ErrorMessage errorMessage={errors.password.message as string} />
            )}
          </div>
          <div style={{ marginTop: 20 }}>
            <span style={{ marginRight: 12 }}>Avatar:</span>
            <input type="file" onChange={onChangeFile} />
            {/* {errors.files && (
              <ErrorMessage errorMessage={errors.files.message as string} />
            )} */}
          </div>
          <button type="submit">Sign Up</button>
          <div className={classes.infoGroup}>
            <a href="/sign-in">Sign in</a>
            <a href="/forgot-password">Forgot Password?</a>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Register;
