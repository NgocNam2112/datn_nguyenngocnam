import React, { useState } from "react";
import {
  Divider,
  Button,
  Card,
  CardContent,
  Typography,
  Table,
  TableBody,
  TableCell,
  Box,
  Container,
  TableContainer,
  TableHead,
  Paper,
  TableRow,
  Grid,
  MenuItem,
  Menu,
} from "@mui/material";
import { ICategory } from "@/config/types";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import useStyles from "./styles";
import { useDispatch } from "react-redux";
import {
  deleteCategoryById,
  fetchCategoryById,
} from "@/store/category/categoryActions";
import { openCategoryPopup } from "@/store/category/categorySlice";

interface IProps {
  categoryList: ICategory[];
}

const CategoriesComponent: React.FC<IProps> = ({ categoryList }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickCategoryById = (id: number) => {
    dispatch(fetchCategoryById({ params: id }));
    dispatch(openCategoryPopup(true));
  };

  const handleDeleteCategory = (id: number) => {
    dispatch(deleteCategoryById({ params: id }));
  };
  return (
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8,
      }}
    >
      <Container maxWidth="lg">
        <Card>
          <CardContent>
            <Grid container spacing={6} wrap="wrap">
              <Grid
                item
                md={4}
                sm={6}
                sx={{
                  display: "flex",
                  flexDirection: "column",
                }}
                xs={12}
              >
                <Typography color="textPrimary" gutterBottom variant="h6">
                  Categories
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <Box sx={{ pt: 3 }}>
          <Grid container spacing={3} style={{ padding: "0 24px" }}>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Id</TableCell>
                    <TableCell align="center">describe</TableCell>
                    <TableCell align="center">created at</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {categoryList &&
                    categoryList.map((item: ICategory) => (
                      <TableRow key={item.id} className={classes.tableRow}>
                        <TableCell
                          onClick={() => handleClickCategoryById(item.id)}
                        >
                          {item.id}
                        </TableCell>
                        <TableCell
                          onClick={() => handleClickCategoryById(item.id)}
                          align="center"
                        >
                          {item.category_name}
                        </TableCell>
                        <TableCell align="center">
                          {item.created_at}
                          <div className={classes.menu}>
                            <Button
                              id="demo-positioned-button"
                              aria-controls={
                                open ? "demo-positioned-menu" : undefined
                              }
                              aria-haspopup="true"
                              aria-expanded={open ? "true" : undefined}
                              onClick={handleClick}
                            >
                              <MoreVertIcon />
                            </Button>
                            <Menu
                              id="demo-positioned-menu"
                              aria-labelledby="demo-positioned-button"
                              anchorEl={anchorEl}
                              open={open}
                              onClose={handleClose}
                              anchorOrigin={{
                                vertical: "top",
                                horizontal: "left",
                              }}
                              transformOrigin={{
                                vertical: "top",
                                horizontal: "left",
                              }}
                            >
                              <MenuItem
                                onClick={() => {
                                  handleDeleteCategory(item.id);
                                  handleClose();
                                }}
                                style={{ color: "red" }}
                              >
                                Delete
                              </MenuItem>
                            </Menu>
                          </div>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};

export default CategoriesComponent;
