import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  tableRow: {
    position: "relative",
    "& img": {
      width: 50,
      height: 50,
    },
  },
  menu: {
    position: "absolute",
    top: "50%",
    right: 0,
    transform: "translate(0, -50%)",
    "& svg": {
      color: "#000",
    },
  },
}));

export default useStyles;
