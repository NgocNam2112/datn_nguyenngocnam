import { DownloadIcon } from "@/icons/download";
import { SearchIcon } from "@/icons/search";
import { UploadIcon } from "@/icons/upload";
import { openAddNewCategoryPopup } from "@/store/category/categorySlice";
import { openAddNewUserInfo } from "@/store/user/userSlice";
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  Typography,
} from "@mui/material";
import { useDispatch } from "react-redux";

export const CategoryToolbar = (props: any) => {
  const dispatch = useDispatch();

  const handleOpenAddNewUserInfo = () => {
    dispatch(dispatch(openAddNewCategoryPopup(true)));
  };

  return (
    <Box {...props} style={{ padding: "0 48px" }}>
      <Box
        sx={{
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          flexWrap: "wrap",
          m: -1,
        }}
      >
        <Typography sx={{ m: 1 }} variant="h4">
          Categories
        </Typography>
        <Box sx={{ m: 1 }}>
          <Button
            color="primary"
            variant="contained"
            onClick={handleOpenAddNewUserInfo}
          >
            Add Category
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
