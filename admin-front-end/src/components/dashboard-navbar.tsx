/* eslint-disable @next/next/no-html-link-for-pages */
import styled from "@emotion/styled";
import {
  AppBar,
  Avatar,
  Box,
  IconButton,
  Toolbar,
  Tooltip,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import { UserCircle as UserCircleIcon } from "../icons/user-circle";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { useRouter } from "next/router";
import Link from "next/link";

const DashboardNavbarRoot = styled(AppBar)(({ theme }) => ({
  // backgroundColor: theme?.palette.background.paper,
  // boxShadow: theme?.shadows[3],
}));

const useStyles = makeStyles((theme) => ({
  wrapSignout: {
    position: "absolute",
    height: 90,
    width: 120,
    zIndex: 10,
    top: 40,
    left: -60,
    color: "#000",
    boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
    display: "flex",
    flexDirection: "column",
    alignContent: "center",
    justifyContent: "center",
    padding: "0 20px",
    "& p": {
      padding: "4px 0",
      cursor: "pointer",
      "&:last-child": {
        color: "red",
        textDecoration: "underline",
      },
    },
  },
}));

export const DashboardNavbar = (props: any) => {
  const classes = useStyles();
  const router = useRouter();
  const { onSidebarOpen, ...other } = props;
  const { userProfile } = useSelector((state: RootState) => state.user);
  const [isShowProfile, setIsShowProfile] = useState(false);

  const handleSignOut = () => {
    window.localStorage.clear();
    router.push("/login");
  };

  const myProfile = () => {
    return (
      <div className={classes.wrapSignout}>
        <Link href="/account">
          <p>My profile</p>
        </Link>
        <p onClick={handleSignOut}>Sign out</p>
      </div>
    );
  };

  return (
    <>
      <DashboardNavbarRoot
        sx={{
          left: {
            lg: 280,
          },
          width: {
            lg: "calc(100% - 280px)",
          },
        }}
        {...other}
      >
        <Toolbar
          disableGutters
          sx={{
            minHeight: 64,
            left: 0,
            px: 2,
          }}
        >
          <IconButton
            onClick={onSidebarOpen}
            sx={{
              display: {
                xs: "inline-flex",
                lg: "none",
              },
            }}
          >
            <MenuIcon fontSize="small" />
          </IconButton>
          <Tooltip title="Search">
            <IconButton sx={{ ml: 1 }}>
              <SearchIcon fontSize="small" />
            </IconButton>
          </Tooltip>
          <Box sx={{ flexGrow: 1 }} />
          <div
            onMouseOver={() => setIsShowProfile(true)}
            onMouseLeave={() => setIsShowProfile(false)}
            style={{ position: "relative" }}
          >
            <Avatar
              sx={{
                height: 40,
                width: 40,
                ml: 1,
              }}
              src={
                userProfile
                  ? userProfile.avatar
                  : "/static/images/avatars/avatar_1.png"
              }
              style={{ position: "relative" }}
            >
              <UserCircleIcon fontSize="small" />
            </Avatar>
            {isShowProfile && myProfile()}
          </div>
        </Toolbar>
      </DashboardNavbarRoot>
    </>
  );
};
