import React, { useState } from "react";
import {
  Avatar,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button,
  Menu,
  MenuItem,
} from "@mui/material";
import useStyles from "./styles";
import { IOrder } from "@/config/types";
import { useDispatch } from "react-redux";
import {
  deleteOrderIdByAdmin,
  fetchOrderIdByAdmin,
} from "@/store/order/orderActions";
import MoreVertIcon from "@mui/icons-material/MoreVert";

interface IProps {
  orderList: IOrder[];
}

const DashboardTable: React.FC<IProps> = ({ orderList }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleFetchOrderDetailById = (orderId: number, userId: number) => {
    dispatch(fetchOrderIdByAdmin({ orderId, userId }));
  };

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDeleteOrder = (orderId: number, userId: number) => {
    dispatch(deleteOrderIdByAdmin({ orderId, userId }));
  };

  return (
    <Box sx={{ minWidth: 1050 }} className={classes.wrapTable}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Order number</TableCell>
            <TableCell>Order date</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Address</TableCell>
            <TableCell>Town</TableCell>
            <TableCell>Phone number</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orderList.length &&
            orderList.map((item: IOrder) => {
              return (
                <TableRow hover key={item.id} className={classes.tableRow}>
                  <TableCell
                    onClick={() =>
                      handleFetchOrderDetailById(item.id, item.user.id)
                    }
                  >
                    <Box
                      sx={{
                        alignItems: "center",
                        display: "flex",
                      }}
                    >
                      <Avatar
                        src={
                          item.user.avatar
                            ? item.user.avatar
                            : "/default-user-image.png"
                        }
                        sx={{ mr: 2 }}
                      ></Avatar>
                      <Typography color="textPrimary" variant="body1">
                        {`${item.user.first_name} ${item.user.last_name}`}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell
                    onClick={() =>
                      handleFetchOrderDetailById(item.id, item.user.id)
                    }
                  >
                    {item.id}
                  </TableCell>
                  <TableCell
                    onClick={() =>
                      handleFetchOrderDetailById(item.id, item.user.id)
                    }
                  >
                    {item.created_at}
                  </TableCell>
                  <TableCell
                    onClick={() =>
                      handleFetchOrderDetailById(item.id, item.user.id)
                    }
                  >
                    {item.user.mail_address}
                  </TableCell>
                  <TableCell
                    onClick={() =>
                      handleFetchOrderDetailById(item.id, item.user.id)
                    }
                  >
                    {item.user.address}
                  </TableCell>
                  <TableCell
                    onClick={() =>
                      handleFetchOrderDetailById(item.id, item.user.id)
                    }
                  >
                    {item.user.town}
                  </TableCell>
                  <TableCell>
                    <span style={{ marginRight: 20 }}>
                      {item.user.phone_number}
                    </span>
                    <div className={classes.menu}>
                      <Button
                        id="demo-positioned-button"
                        aria-controls={
                          open ? "demo-positioned-menu" : undefined
                        }
                        aria-haspopup="true"
                        aria-expanded={open ? "true" : undefined}
                        onClick={handleClick}
                      >
                        <MoreVertIcon />
                      </Button>
                      <Menu
                        id="demo-positioned-menu"
                        aria-labelledby="demo-positioned-button"
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        anchorOrigin={{
                          vertical: "top",
                          horizontal: "left",
                        }}
                        transformOrigin={{
                          vertical: "top",
                          horizontal: "left",
                        }}
                      >
                        <MenuItem
                          onClick={() => {
                            handleDeleteOrder(item.id, item.user.id);
                            handleClose();
                          }}
                          style={{ color: "red" }}
                        >
                          Delete
                        </MenuItem>
                      </Menu>
                    </div>
                  </TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </Box>
  );
};

export default DashboardTable;
