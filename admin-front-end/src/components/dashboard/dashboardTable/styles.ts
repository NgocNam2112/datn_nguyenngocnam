import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  tableRow: {
    position: "relative",
  },
  menu: {
    position: "absolute",
    top: "12%",
    right: 0,
    transform: "traslate(-50%, 0)",
  },
  wrapTable: {
    maxHeight: 500,
    overflowY: "scroll",
    boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
    marginTop: 12,
  },
}));

export default useStyles;
