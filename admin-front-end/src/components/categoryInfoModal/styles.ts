import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  wrapTextField: {
    width: "100%",
    marginTop: 20,
    boxSizing: "border-box",
    "& div": {
      width: "100%",
      "& input": {
        width: "100%",
      },
    },
  },
  wrapButton: {
    marginTop: 20,
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    gap: 12,
  },
}));

export default useStyles;
