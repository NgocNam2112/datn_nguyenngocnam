import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/store";
import TextField from "@mui/material/TextField";
import {
  closeAddNewCategoryPopup,
  closeCategoryPopup,
} from "@/store/category/categorySlice";
import useStyles from "./styles";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import { createNewCategory } from "@/store/category/categoryActions";

interface FormValues {
  category_name: string;
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 500,
  bgcolor: "#95a5a6",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const AddNewCategoryInfoModal = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { isOpencateAddNewgoryPopup } = useSelector(
    (state: RootState) => state.category
  );
  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm<FormValues>();

  const handleClose = () => {
    dispatch(closeAddNewCategoryPopup(false));
  };

  const onSubmit: SubmitHandler<FormValues> = (formValues) => {
    dispatch(createNewCategory({ category_name: formValues.category_name }));
  };

  return (
    <Modal
      open={isOpencateAddNewgoryPopup}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <form>
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Category info
          </Typography>
          <div className={classes.wrapTextField}>
            <TextField
              {...register("category_name", {
                required: {
                  value: true,
                  message: "Please enter category name",
                },
              })}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              label="Category name"
            />
            {errors.category_name && (
              <ErrorMessage
                errorMessage={errors.category_name as unknown as string}
              />
            )}
          </div>
          <div className={classes.wrapButton}>
            <Button
              variant="outlined"
              type="submit"
              onClick={handleSubmit(onSubmit)}
            >
              Create
            </Button>
            <Button
              variant="outlined"
              color="error"
              onClick={() => dispatch(closeCategoryPopup(false))}
            >
              Cancel
            </Button>
          </div>
        </Box>
      </form>
    </Modal>
  );
};

export default AddNewCategoryInfoModal;
