import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/store";
import TextField from "@mui/material/TextField";
import { closeCategoryPopup } from "@/store/category/categorySlice";
import useStyles from "./styles";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import { updateCategoryById } from "@/store/category/categoryActions";

interface FormValues {
  category_name: string;
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 500,
  bgcolor: "#95a5a6",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const CategpryInfoModal = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { isOpencategoryPopup, categoryById } = useSelector(
    (state: RootState) => state.category
  );
  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm<FormValues>({
    defaultValues: {
      category_name: categoryById.category_name,
    },
  });

  const handleClose = () => {
    dispatch(closeCategoryPopup(false));
  };

  const onSubmit: SubmitHandler<FormValues> = (formValues) => {
    dispatch(
      updateCategoryById({
        params: categoryById.id,
        category_name: formValues.category_name,
      })
    );
  };

  return (
    <Modal
      open={isOpencategoryPopup}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <form>
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Category info
          </Typography>
          <div className={classes.wrapTextField}>
            <TextField
              {...register("category_name", {
                required: {
                  value: true,
                  message: "Please enter category name",
                },
              })}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
              label="Category name"
            />
            {errors.category_name && (
              <ErrorMessage
                errorMessage={errors.category_name as unknown as string}
              />
            )}
          </div>
          <div className={classes.wrapButton}>
            <Button
              variant="outlined"
              type="submit"
              onClick={handleSubmit(onSubmit)}
            >
              Update
            </Button>
            <Button
              variant="outlined"
              color="error"
              onClick={() => dispatch(closeCategoryPopup(false))}
            >
              Cancel
            </Button>
          </div>
        </Box>
      </form>
    </Modal>
  );
};

export default CategpryInfoModal;
