import React from "react";
import { makeStyles } from "@material-ui/core/styles";

interface IProps {
  errorMessage: string;
}
const useStyles = makeStyles((theme) => ({
  errorMessage: {
    fontSize: 12,
    fontFamily: "Poppins",
    color: "red",
    fontWeight: 700,
  },
}));

const ErrorMessage: React.VFC<IProps> = ({ errorMessage }) => {
  const classes = useStyles();

  return <div className={classes.errorMessage}>{errorMessage}</div>;
};

export default ErrorMessage;
