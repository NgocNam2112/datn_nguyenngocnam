/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Rating from "@mui/material/Rating";
import useStyles from "./styles";
import { useDispatch } from "react-redux";
import { openProductDialog } from "@/store/product/productSlice";
import {
  deleteProduct,
  fetchProductById,
} from "@/store/product/productActions";
import { fetchDescriptionByProductId } from "@/store/description/descriptionActions";

interface IProps {
  key: number;
  id: number;
  name: string;
  image: string;
  selling_price: number;
  discount: number;
  rate: number;
}

const CustomTableRow: React.FC<IProps> = ({
  id,
  image,
  name,
  selling_price,
  discount,
  rate,
}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpenProductDialog = () => {
    dispatch(openProductDialog(true));
    Promise.all([
      dispatch(fetchProductById({ params: id })),
      dispatch(fetchDescriptionByProductId({ params: id })),
    ]);
  };

  const handleDeleteProduct = (id: number) => {
    dispatch(deleteProduct({ params: id }));
  };

  return (
    <TableRow
      sx={{
        "&:last-child td, &:last-child th": { border: 0 },
      }}
      className={classes.tableRow}
    >
      <TableCell
        component="th"
        scope="row"
        style={{ width: 500 }}
        onClick={handleOpenProductDialog}
      >
        {name}
      </TableCell>
      <TableCell align="center" onClick={handleOpenProductDialog}>
        <img src={image} alt="product" />
      </TableCell>
      <TableCell align="center" onClick={handleOpenProductDialog}>
        {selling_price}
      </TableCell>
      <TableCell align="center" onClick={handleOpenProductDialog}>
        {discount}
      </TableCell>
      <TableCell align="center">
        <Rating name="read-only" value={rate} readOnly />

        <div className={classes.menu}>
          <Button
            id="demo-positioned-button"
            aria-controls={open ? "demo-positioned-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            onClick={handleClick}
          >
            <MoreVertIcon />
          </Button>
          <Menu
            id="demo-positioned-menu"
            aria-labelledby="demo-positioned-button"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "top",
              horizontal: "left",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "left",
            }}
          >
            <MenuItem
              onClick={() => {
                handleDeleteProduct(id);
                handleClose();
              }}
              style={{ color: "red" }}
            >
              Delete
            </MenuItem>
          </Menu>
        </div>
      </TableCell>
    </TableRow>
  );
};

export default CustomTableRow;
