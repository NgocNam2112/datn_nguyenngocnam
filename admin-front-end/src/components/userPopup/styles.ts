import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  wrapAvatar: {
    textAlign: "center",
    "& img": {
      width: 150,
      height: 150,
      borderRadius: "50%",
      objectFit: "contain",
    },
    "& p": {
      fontWeight: "700",
      fontSize: 20,
    },
  },
  wrapInput: {
    gap: 20,
    display: "flex",
    marginTop: 12,
  },
  wrapButton: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    gap: 20,
    marginTop: 20,
  },
}));

export default useStyles;
