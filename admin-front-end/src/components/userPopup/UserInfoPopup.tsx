/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/store";
import { closeUserInfo } from "@/store/user/userSlice";
import useStyles from "./styles";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import { upateUserById } from "@/store/user/userActions";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  height: 530,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  bgcolor: "#95a5a6",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

interface FormValues {
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
}

const UserInfoPopup = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { isOpenUserInfo, userProfile } = useSelector(
    (state: RootState) => state.user
  );
  const [files, setFiles] = useState();
  const [image, setImage] = useState<any>();

  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm<FormValues>({
    defaultValues: {
      first_name: userProfile.first_name,
      last_name: userProfile.last_name,
      phone_number: userProfile.phone_number,
      address: userProfile.address,
      town: userProfile.town,
    },
  });

  const handleChangeFile = (e: any) => {
    setFiles(e.target.files[0]);
    setImage(URL.createObjectURL(e.target.files[0]));
  };

  const handleClose = () => {
    dispatch(closeUserInfo(false));
  };

  const onSubmit: SubmitHandler<FormValues> = (formValues: FormValues) => {
    dispatch(
      upateUserById({
        params: userProfile.id,
        userInfo: {
          first_name: formValues.first_name,
          last_name: formValues.last_name,
          address: formValues.address,
          town: formValues.town,
          phone_number: formValues.phone_number,
          files: files,
        },
      })
    );
  };

  useEffect(() => {
    reset(userProfile);
  }, [reset, userProfile]);

  return (
    <Modal
      open={isOpenUserInfo}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <div className={classes.wrapAvatar}>
          <label htmlFor="input-avatar">
            <img src={image ? image : userProfile.avatar} alt="avatar" />
          </label>
          <input
            type="file"
            id="input-avatar"
            onChange={(e) => handleChangeFile(e)}
            style={{ display: "none" }}
          />
          <p>{userProfile.mail_address}</p>
        </div>
        <div className={classes.wrapInput}>
          <div>
            <TextField
              {...register("first_name", {
                required: {
                  value: true,
                  message: "Please enter your first name",
                },
                maxLength: {
                  value: 50,
                  message: "Enter the name from 1 to 50 characters.",
                },
              })}
              variant="outlined"
              label="First Name"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.first_name && (
              <ErrorMessage
                errorMessage={errors.first_name.message as string}
              />
            )}
          </div>
          <div>
            <TextField
              {...register("last_name", {
                required: {
                  value: true,
                  message: "Please enter your last name",
                },
                maxLength: {
                  value: 50,
                  message: "Enter the name from 1 to 50 characters.",
                },
              })}
              variant="outlined"
              label="Last Name"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.last_name && (
              <ErrorMessage errorMessage={errors.last_name.message as string} />
            )}
          </div>
        </div>

        <div className={classes.wrapInput}>
          <div>
            <TextField
              {...register("address", {
                required: {
                  value: true,
                  message: "Please enter your address",
                },
                maxLength: 45,
              })}
              variant="outlined"
              label="Address"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.address && (
              <ErrorMessage errorMessage={errors.address.message as string} />
            )}
          </div>
          <div>
            <TextField
              fullWidth
              label="Phone number"
              {...register("phone_number", {
                required: {
                  value: true,
                  message: "Please enter your phone number",
                },
                maxLength: {
                  value: 20,
                  message: "Phone number require less than 20 characters",
                },
                pattern: /\d{1,4}-\d{1,4}-\d{4}/,
              })}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.phone_number && (
              <ErrorMessage
                errorMessage={errors.phone_number.message as string}
              />
            )}
          </div>
        </div>
        <div className={classes.wrapInput}>
          <div>
            <TextField
              {...register("town", {
                required: {
                  value: true,
                  message: "Please enter your address",
                },
                maxLength: 45,
              })}
              variant="outlined"
              label="Town"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.town && (
              <ErrorMessage errorMessage={errors.town.message as string} />
            )}
          </div>
        </div>

        <div className={classes.wrapButton}>
          <Button
            variant="outlined"
            type="submit"
            onClick={handleSubmit(onSubmit)}
          >
            Update
          </Button>
          <Button color="error" variant="outlined" onClick={handleClose}>
            Cancel
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default UserInfoPopup;
