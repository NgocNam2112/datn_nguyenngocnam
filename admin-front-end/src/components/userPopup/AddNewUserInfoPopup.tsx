/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { closeAddNewUserInfo } from "@/store/user/userSlice";
import useStyles from "./styles";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import { useForm, SubmitHandler } from "react-hook-form";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { IRoles } from "@/config/types";
import { createNewUser } from "@/store/user/userActions";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  height: 530,
  display: "flex",
  flexDirection: "column",
  overflow: "scroll",
  alignItems: "center",
  bgcolor: "#95a5a6",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

interface FormValues {
  mail_address: string;
  password: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
  roles: number;
}

const AddNewUserInfoPopup = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [files, setFiles] = useState();
  const [image, setImage] = useState<any>();
  const [error, setError] = useState("");

  const { isOpenAddnewUserInfo } = useSelector(
    (state: RootState) => state.user
  );
  const { rolesList } = useSelector((state: RootState) => state.roles);
  const { message } = useSelector((state: RootState) => state.apiError);

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<FormValues>();

  const handleClose = () => {
    dispatch(closeAddNewUserInfo(true));
  };

  const handleChangeFile = (e: any) => {
    setFiles(e.target.files[0]);
    setImage(URL.createObjectURL(e.target.files[0]));
    setError("");
  };

  const onSubmit: SubmitHandler<FormValues> = (formValues) => {
    if (!files) {
      setError("Please enter user's image");
      return;
    }
    dispatch(
      createNewUser({
        userInfo: {
          mail_address: formValues.mail_address,
          password: formValues.password,
          first_name: formValues.first_name,
          last_name: formValues.last_name,
          address: formValues.address,
          phone_number: formValues.phone_number,
          rolesId: formValues.roles,
          town: formValues.town,
          files: files,
        },
      })
    );
  };

  return (
    <Modal
      open={isOpenAddnewUserInfo}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <div className={classes.wrapAvatar}>
          <label htmlFor="input-avatar">
            <img src={image ? image : "/default-user-image.png"} alt="avatar" />
          </label>
          <input
            type="file"
            id="input-avatar"
            onChange={(e) => handleChangeFile(e)}
            style={{ display: "none" }}
          />
          {error && <ErrorMessage errorMessage={error} />}
          {message && <ErrorMessage errorMessage={message} />}
        </div>

        <div className={classes.wrapInput}>
          <div>
            <TextField
              {...register("mail_address", {
                required: { value: true, message: "Email is not empty" },
                maxLength: {
                  value: 256,
                  message:
                    "Please enter one or more and 256 characters or less for the email.",
                },
                pattern: {
                  value:
                    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message: "Some characters cannot be used.",
                },
              })}
              variant="outlined"
              label="Email"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.first_name && (
              <ErrorMessage
                errorMessage={errors.first_name.message as string}
              />
            )}
          </div>
          <div>
            <TextField
              {...register("password", {
                required: {
                  value: true,
                  message: "Password is not empty",
                },
                minLength: {
                  value: 6,
                  message: "Please enter the password from 8 to 20 characters.",
                },
                maxLength: {
                  value: 20,
                  message: "Please enter the password from 8 to 20 characters.",
                },
              })}
              type="password"
              variant="outlined"
              label="Password"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.last_name && (
              <ErrorMessage errorMessage={errors.last_name.message as string} />
            )}
          </div>
        </div>
        <div className={classes.wrapInput}>
          <div>
            <TextField
              {...register("first_name", {
                required: {
                  value: true,
                  message: "Please enter your first name",
                },
                maxLength: {
                  value: 50,
                  message: "Enter the name from 1 to 50 characters.",
                },
              })}
              variant="outlined"
              label="First Name"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.first_name && (
              <ErrorMessage
                errorMessage={errors.first_name.message as string}
              />
            )}
          </div>
          <div>
            <TextField
              {...register("last_name", {
                required: {
                  value: true,
                  message: "Please enter your last name",
                },
                maxLength: {
                  value: 50,
                  message: "Enter the name from 1 to 50 characters.",
                },
              })}
              variant="outlined"
              label="Last Name"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.last_name && (
              <ErrorMessage errorMessage={errors.last_name.message as string} />
            )}
          </div>
        </div>

        <div className={classes.wrapInput}>
          <div>
            <TextField
              {...register("address", {
                required: {
                  value: true,
                  message: "Please enter your address",
                },
                maxLength: 45,
              })}
              variant="outlined"
              label="Address"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.address && (
              <ErrorMessage errorMessage={errors.address.message as string} />
            )}
          </div>
          <div>
            <TextField
              fullWidth
              label="Phone number"
              {...register("phone_number", {
                required: {
                  value: true,
                  message: "Please enter your phone number",
                },
                maxLength: {
                  value: 20,
                  message: "Phone number require less than 20 characters",
                },
                pattern: /\d{1,4}-\d{1,4}-\d{4}/,
              })}
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.phone_number && (
              <ErrorMessage
                errorMessage={errors.phone_number.message as string}
              />
            )}
          </div>
        </div>
        <div className={classes.wrapInput}>
          <div>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="category"
              {...register("roles", {
                required: {
                  value: true,
                  message: "Please enter roles for this user",
                },
              })}
            >
              {rolesList.map((item: IRoles) => {
                return (
                  <MenuItem value={item.id} key={item.id}>
                    {item.describe}
                  </MenuItem>
                );
              })}
            </Select>
          </div>
          <div>
            <TextField
              {...register("town", {
                required: {
                  value: true,
                  message: "Please enter your address",
                },
                maxLength: 45,
              })}
              variant="outlined"
              label="Town"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.town && (
              <ErrorMessage errorMessage={errors.town.message as string} />
            )}
          </div>
        </div>

        <div className={classes.wrapButton}>
          <Button
            variant="outlined"
            type="submit"
            onClick={handleSubmit(onSubmit)}
          >
            Create
          </Button>
          <Button color="error" variant="outlined" onClick={handleClose}>
            Cancel
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default AddNewUserInfoPopup;
