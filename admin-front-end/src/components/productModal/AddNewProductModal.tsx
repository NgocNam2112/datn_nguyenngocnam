/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Rating from "@mui/material/Rating";
import useStyles from "./styles";
import { useSelector, useDispatch } from "react-redux";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { fetchAllCategory } from "@/store/category/categoryActions";
import { RootState } from "@/store";
import { ICategory } from "@/config/types";
import { addNewProduct } from "@/store/product/productActions";
import { closeAddNewProduct } from "@/store/product/productSlice";

interface FormValues {
  name: string;
  selling_price: number;
  discount: number;
  stock: number;
  farm: string;
  content: string;
  space_for_discription: string;
  categoryId: number;
  delivery_area: string;

  origin: string;
  cook: string;
  vitamins: string;
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  height: 600,
  overflow: "scroll",
  bgcolor: "#95a5a6",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const AddNewProductModal = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [productImage, setProductImage] = useState();
  const [image, setImage] = useState();
  const [firstImage, setFirstImage] = useState();
  const [first, setFirst] = useState();
  const [secondImage, setSecondImage] = useState();
  const [second, setSecond] = useState();
  const [rating, setRating] = useState<number | undefined>();
  const [categoryId, setCategoryId] = useState("1");
  const [error, setError] = useState("");
  const { categoryList } = useSelector((state: RootState) => state.category);
  const { isOpenAddNewProduct } = useSelector(
    (state: RootState) => state.product
  );

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<FormValues>({});

  const handleClose = () => {
    dispatch(closeAddNewProduct(false));
  };

  const handleChangeFile = (
    e: any,
    state: any,
    setState: (value: any) => void,
    setImageURL: (value: any) => void
  ) => {
    setState(e.target.files[0]);
    setImageURL(URL.createObjectURL(e.target.files[0]));
  };

  const handleChange = (event: SelectChangeEvent) => {
    setCategoryId(event.target.value);
  };

  useEffect(() => {
    dispatch(fetchAllCategory({}));
  }, [dispatch]);

  const onSubmit: SubmitHandler<FormValues> = (formValues) => {
    if (!productImage || !firstImage || !secondImage) {
      setError("Please enter all image");
      return;
    }
    dispatch(
      addNewProduct({
        productInfo: {
          name: formValues.name,
          selling_price: formValues.selling_price,
          stock: formValues.stock,
          rate: rating as number,
          categoryId: +categoryId,
          content: formValues.content,
          files: productImage,
          farm: formValues.farm,
          discount: formValues.discount,
          origin: formValues.origin,
          cook: formValues.cook,
          vitamins: formValues.vitamins,
          delivery_area: formValues.delivery_area,
          space_for_discription: formValues.space_for_discription,
          firstImageFileDescriptions: firstImage,
          secondImageFileDescription: secondImage,
        },
      })
    );
  };

  return (
    <Modal
      open={isOpenAddNewProduct}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <form>
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            color="error"
          >
            {error}
          </Typography>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Product info
          </Typography>
          <div className={classes.wrapProduct}>
            <div className={classes.wrapImage}>
              <div>
                {image && (
                  <label htmlFor="product-image">
                    <img src={image} alt="product" />
                  </label>
                )}
                <input
                  type="file"
                  id="product-image"
                  onChange={(e) =>
                    handleChangeFile(e, productImage, setProductImage, setImage)
                  }
                />
              </div>
              <div>
                {first && (
                  <label htmlFor="first_image">
                    <img src={first} alt="product" />
                  </label>
                )}
                <input
                  type="file"
                  id="first_image"
                  onChange={(e) =>
                    handleChangeFile(e, firstImage, setFirstImage, setFirst)
                  }
                />
              </div>

              <div>
                {second && (
                  <label htmlFor="second_image">
                    <img src={second} alt="product" />
                  </label>
                )}
                <input
                  type="file"
                  id="second_image"
                  onChange={(e) =>
                    handleChangeFile(e, secondImage, setSecondImage, setSecond)
                  }
                />
              </div>
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                {...register("name", {
                  required: {
                    value: true,
                    message: "Please enter product name",
                  },
                })}
                label="Name"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                {...register("selling_price", {
                  required: {
                    value: true,
                    message: "Please enter product price",
                  },
                })}
                type={"number"}
                label="Selling price"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                type="number"
                {...register("stock", {
                  required: {
                    value: true,
                    message: "Please enter product in stock",
                  },
                })}
                label="Stock"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                {...register("farm", {
                  required: {
                    value: true,
                    message: "Please enter product farm",
                  },
                })}
                label="farm"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                type="number"
                label="Discount"
                {...register("discount", {
                  required: {
                    value: true,
                    message: "Please enter product discount",
                  },
                })}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <Box
                sx={{
                  "& > legend": { mt: 2 },
                }}
              >
                <Rating
                  name="simple-controlled"
                  value={rating}
                  onChange={(event, newValue) => {
                    setRating(newValue as number);
                  }}
                />
              </Box>
            </div>
            <div className={classes.wrapTextField}>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={categoryId}
                label="category"
                onChange={handleChange}
              >
                {categoryList.map((item: ICategory) => {
                  return (
                    <MenuItem value={item.id} key={item.id}>
                      {item.category_name}
                    </MenuItem>
                  );
                })}
              </Select>
              <TextField
                {...register("space_for_discription", {
                  required: {
                    value: true,
                    message: "Please enter product category",
                  },
                })}
                label="space_for_discription"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                {...register("content", {
                  required: {
                    value: true,
                    message: "Please enter product content",
                  },
                })}
                label="Content"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />

              <TextField
                {...register("origin", {
                  required: {
                    value: true,
                    message: "Please enter product origin",
                  },
                })}
                label="Origin"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                {...register("cook", {
                  required: {
                    value: true,
                    message: "Please enter how to cook product",
                  },
                })}
                label="Cook"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />

              <TextField
                {...register("vitamins", {
                  required: {
                    value: true,
                    message: "Please enter vitamins of product",
                  },
                })}
                label="Vitamins"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                {...register("delivery_area", {
                  required: {
                    value: true,
                    message: "Please enter product content",
                  },
                })}
                label="delivery_area"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
          </div>
          <div className={classes.wrapButton}>
            <Button
              variant="outlined"
              type="submit"
              onClick={handleSubmit(onSubmit)}
            >
              Create
            </Button>
            <Button variant="outlined" color="error" onClick={handleClose}>
              Cancel
            </Button>
          </div>
        </Box>
      </form>
    </Modal>
  );
};

export default AddNewProductModal;
