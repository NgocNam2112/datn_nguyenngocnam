import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  wrapProduct: {
    "& img": {
      width: 150,
      height: 150,
      objectFit: "contain",
    },
  },
  wrapImage: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 12,
  },
  wrapTextField: {
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
    "& > div": {
      width: "49%",
    },
  },
  wrapButton: {
    display: "flex",
    justifyContent: "flex-end",
    gap: 10,
  },
}));

export default useStyles;
