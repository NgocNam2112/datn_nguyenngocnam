/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { RootState } from "@/store";
import { useSelector, useDispatch } from "react-redux";
import { closeProductDialog } from "@/store/product/productSlice";
import useStyles from "./styles";
import Rating from "@mui/material/Rating";
import { SubmitHandler, useForm } from "react-hook-form";
import { upateDescription } from "@/store/description/descriptionActions";
import { updateProductById } from "@/store/product/productActions";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "#95a5a6",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

interface FormValues {
  name: string;
  selling_price: number;
  discount: number;
  stock: number;
  farm: string;
  content: string;

  origin: string;
  cook: string;
  vitamins: string;
}

const ProductModal = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { productById, isOpenProductDialog } = useSelector(
    (state: RootState) => state.product
  );
  const { descriptionByProductId } = useSelector(
    (state: RootState) => state.description
  );
  const [productImage, setProductImage] = useState();
  const [image, setImage] = useState();
  const [firstImage, setFirstImage] = useState();
  const [first, setFirst] = useState();
  const [secondImage, setSecondImage] = useState();
  const [second, setSecond] = useState();
  const [rating, setRating] = useState<number | undefined>(
    productById && productById.rate
  );
  const [error, setError] = useState<string | undefined>();

  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm<FormValues>({
    defaultValues: {
      name: productById.name,
      selling_price: productById.selling_price,
      discount: productById.discount,
      stock: productById.stock,
      farm: productById.farm,
      content: productById.content,
      origin: descriptionByProductId.origin,
      cook: descriptionByProductId.cook,
      vitamins: descriptionByProductId.vitamins,
    },
  });

  const handleClose = () => {
    dispatch(closeProductDialog(false));
  };

  const handleChangeFile = (
    e: any,
    state: any,
    setState: (value: any) => void,
    setImageURL: (value: any) => void
  ) => {
    setState(e.target.files[0]);
    setImageURL(URL.createObjectURL(e.target.files[0]));
  };

  const onSubmit: SubmitHandler<FormValues> = (formValues) => {
    if (!productImage || !firstImage || !secondImage) {
      setError("Please enter all image");
    }
    setError("");
    dispatch(
      upateDescription({
        descriptionInfo: {
          origin: formValues.origin,
          cook: formValues.cook,
          vitamins: formValues.vitamins,
          firstImageFile: firstImage,
          secondImageFile: secondImage,
        },
        params: productById.id,
      })
    );
    dispatch(
      updateProductById({
        productInfo: {
          selling_price: formValues.selling_price,
          discount: formValues.discount,
          stock: formValues.stock,
          content: formValues.content,
          rate: rating as number,
          farm: formValues.farm,
          delivery_area: productById.delivery_area,
          space_for_discription: productById.space_for_discription,
          name: formValues.name,
          files: productImage,
          categoryId: productById.categoryId,
        },
        params: productById.id,
      })
    );
  };

  useEffect(() => {
    reset(productById);
  }, [productById, reset]);

  useEffect(() => {
    reset(descriptionByProductId);
  }, [descriptionByProductId, reset]);

  return (
    <Modal
      open={isOpenProductDialog}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <form>
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Product info
          </Typography>
          <div className={classes.wrapProduct}>
            <div className={classes.wrapImage}>
              <div>
                <label htmlFor="product-image">
                  <img src={image ? image : productById.image} alt="product" />
                </label>
                <input
                  type="file"
                  id="product-image"
                  onChange={(e) =>
                    handleChangeFile(e, productImage, setProductImage, setImage)
                  }
                  style={{ display: "none" }}
                />
              </div>
              <div>
                <label htmlFor="first_image">
                  <img
                    src={first ? first : descriptionByProductId.first_image}
                    alt="product"
                  />
                </label>
                <input
                  type="file"
                  id="first_image"
                  onChange={(e) =>
                    handleChangeFile(e, firstImage, setFirstImage, setFirst)
                  }
                  style={{ display: "none" }}
                />
              </div>

              <div>
                <label htmlFor="second_image">
                  <img
                    src={second ? second : descriptionByProductId.second_image}
                    alt="product"
                  />
                </label>
                <input
                  type="file"
                  id="second_image"
                  onChange={(e) =>
                    handleChangeFile(e, secondImage, setSecondImage, setSecond)
                  }
                  style={{ display: "none" }}
                />
              </div>
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                {...register("name", {
                  required: {
                    value: true,
                    message: "Please enter product name",
                  },
                })}
                label="Name"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                {...register("selling_price", {
                  required: {
                    value: true,
                    message: "Please enter product price",
                  },
                })}
                label="Selling price"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                type="number"
                {...register("stock", {
                  required: {
                    value: true,
                    message: "Please enter product in stock",
                  },
                })}
                label="Stock"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                {...register("farm", {
                  required: {
                    value: true,
                    message: "Please enter product farm",
                  },
                })}
                label="farm"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                type="number"
                {...register("discount", {
                  required: {
                    value: true,
                    message: "Please enter product discount",
                  },
                })}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <Box
                sx={{
                  "& > legend": { mt: 2 },
                }}
              >
                <Rating
                  name="simple-controlled"
                  value={rating}
                  onChange={(event, newValue) => {
                    setRating(newValue as number);
                  }}
                />
              </Box>
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                {...register("content", {
                  required: {
                    value: true,
                    message: "Please enter product content",
                  },
                })}
                label="Content"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
                required
              />

              <TextField
                {...register("origin", {
                  required: {
                    value: true,
                    message: "Please enter product origin",
                  },
                })}
                label="Origin"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className={classes.wrapTextField}>
              <TextField
                {...register("cook", {
                  required: {
                    value: true,
                    message: "Please enter how to cook product",
                  },
                })}
                label="Cook"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />

              <TextField
                {...register("vitamins", {
                  required: {
                    value: true,
                    message: "Please enter vitamins of product",
                  },
                })}
                label="Vitamins"
                variant="outlined"
                multiline
                rows={2}
                maxRows={4}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
          </div>
          <div className={classes.wrapButton}>
            <Button
              variant="outlined"
              type="submit"
              onClick={handleSubmit(onSubmit)}
            >
              Update
            </Button>
            <Button
              variant="outlined"
              color="error"
              onClick={() => dispatch(closeProductDialog(false))}
            >
              Cancel
            </Button>
          </div>
        </Box>
      </form>
    </Modal>
  );
};

export default ProductModal;
