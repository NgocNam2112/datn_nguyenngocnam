import React, { useState, useEffect } from "react";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  Typography,
  Container,
  Grid,
  TextField,
  CardHeader,
} from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { RootState } from "@/store";
import { useSelector, useDispatch } from "react-redux";
import ErrorMessage from "../helper/errorMessage/ErrorMessage";
import { updateProfile } from "@/store/user/userActions";

interface FormValues {
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
}

const AccountProfileComopent = () => {
  const dispatch = useDispatch();
  const { userProfile } = useSelector((state: RootState) => state.user);
  const [files, setFiles] = useState();
  const [image, setImage] = useState<any>();
  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm<FormValues>({
    defaultValues: {
      first_name: userProfile.first_name,
      last_name: userProfile.last_name,
      address: userProfile.address,
      town: userProfile.town,
      phone_number: userProfile.phone_number,
    },
  });

  useEffect(() => {
    reset(userProfile);
  }, [reset, userProfile]);

  const onChangeFile = (e: any) => {
    setFiles(e.target.files[0]);
    setImage(URL.createObjectURL(e.target.files[0]));
  };

  const onSubmit: SubmitHandler<FormValues> = (formValue: FormValues) => {
    dispatch(updateProfile({ userProfile: { ...formValue, files } }));
  };

  return (
    <form>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="lg">
          <Typography sx={{ mb: 3 }} variant="h4">
            Account
          </Typography>
          <Grid container spacing={3}>
            <Grid item lg={4} md={6} xs={12}>
              <Card>
                <CardContent>
                  <Box
                    sx={{
                      alignItems: "center",
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <Avatar
                      src={files ? image : userProfile.avatar}
                      sx={{
                        height: 64,
                        mb: 2,
                        width: 64,
                      }}
                    />
                    <Typography color="textPrimary" gutterBottom variant="h5">
                      {userProfile.first_name} {userProfile.last_name}
                    </Typography>
                    <Typography color="textSecondary" variant="body2">
                      {`${userProfile.address} ${userProfile.town}`}
                    </Typography>
                  </Box>
                </CardContent>
                <Divider />

                <CardActions>
                  <Button color="primary" fullWidth variant="text">
                    <label htmlFor="input-file">Upload picture</label>
                  </Button>
                </CardActions>

                <input
                  type="file"
                  id="input-file"
                  onChange={(e) => onChangeFile(e)}
                  style={{ display: "none" }}
                />
              </Card>
            </Grid>
            <Grid item lg={8} md={6} xs={12}>
              <Card>
                <CardHeader
                  subheader="The information can be edited"
                  title="Profile"
                />
                <Divider />
                <CardContent>
                  <Grid container spacing={3}>
                    <Grid item md={6} xs={12}>
                      <TextField
                        fullWidth
                        label="First name"
                        {...register("first_name", {
                          required: {
                            value: true,
                            message: "Please enter your first name",
                          },
                          maxLength: {
                            value: 50,
                            message: "Enter the name from 1 to 50 characters.",
                          },
                        })}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.first_name && (
                        <ErrorMessage
                          errorMessage={errors.first_name.message as string}
                        />
                      )}
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        label="Last name"
                        {...register("last_name", {
                          required: {
                            value: true,
                            message: "Please enter your last name",
                          },
                          maxLength: {
                            value: 50,
                            message: "Enter the name from 1 to 50 characters.",
                          },
                        })}
                        fullWidth
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.last_name && (
                        <ErrorMessage
                          errorMessage={errors.last_name.message as string}
                        />
                      )}
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        fullWidth
                        label="Phone number"
                        {...register("phone_number", {
                          required: {
                            value: true,
                            message: "Please enter your phone number",
                          },
                          maxLength: {
                            value: 20,
                            message:
                              "Phone number require less than 20 characters",
                          },
                          pattern: /\d{1,4}-\d{1,4}-\d{4}/,
                        })}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.phone_number && (
                        <ErrorMessage
                          errorMessage={errors.phone_number.message as string}
                        />
                      )}
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        fullWidth
                        label="Address"
                        {...register("address", {
                          required: {
                            value: true,
                            message: "Please enter your address",
                          },
                          maxLength: 45,
                        })}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.address && (
                        <ErrorMessage
                          errorMessage={errors.address.message as string}
                        />
                      )}
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        fullWidth
                        label="Town"
                        {...register("town", {
                          required: {
                            value: true,
                            message: "Please enter your address",
                          },
                          maxLength: 45,
                        })}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.town && (
                        <ErrorMessage
                          errorMessage={errors.town.message as string}
                        />
                      )}
                    </Grid>
                  </Grid>
                </CardContent>
                <Divider />
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "flex-end",
                    p: 2,
                  }}
                >
                  <Button
                    color="primary"
                    variant="contained"
                    type="submit"
                    onClick={handleSubmit(onSubmit)}
                  >
                    Save details
                  </Button>
                </Box>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </form>
  );
};

export default AccountProfileComopent;
