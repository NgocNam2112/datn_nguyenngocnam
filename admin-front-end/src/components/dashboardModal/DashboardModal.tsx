/* eslint-disable @next/next/no-img-element */
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { closeOrderDetail } from "@/store/order/orderSlice";
import {
  Modal,
  Avatar,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button,
  TableContainer,
  Paper,
} from "@mui/material";
import useStyles from "./styles";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "#95a5a6",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const DashboardModal = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { isOpenOrderDetail, orderDetail } = useSelector(
    (state: any) => state.order
  );

  const handleClose = () => {
    dispatch(closeOrderDetail(false));
  };

  return (
    <Modal
      open={isOpenOrderDetail}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Table>
          <TableBody>
            <TableRow hover>
              <TableCell>
                <div className={classes.wrapUserAvatar}>
                  <Avatar
                    src={
                      orderDetail.user.avatar
                        ? orderDetail.user.avatar
                        : "/default-user-image.png"
                    }
                    sx={{ mr: 2 }}
                  ></Avatar>
                  <Typography color="textPrimary" variant="body1">
                    {`${orderDetail.user.first_name} 
                    ${orderDetail.user.last_name}`}
                  </Typography>
                </div>
              </TableCell>
              <TableCell>
                Phone number: {orderDetail.user.phone_number}
              </TableCell>
            </TableRow>

            <TableRow hover>
              <TableCell>Email: {orderDetail.user.mail_address}</TableCell>
              <TableCell>Address: {orderDetail.user.address}</TableCell>
            </TableRow>
            <TableRow hover>
              <TableCell>Order date: {orderDetail.created_at}</TableCell>
              <TableCell>Town: {orderDetail.user.town}</TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <Typography variant="h4" style={{ marginTop: 20 }}>
          Product list
        </Typography>

        <TableContainer
          component={Paper}
          className={classes.wrapTableContainer}
        >
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="center">Image</TableCell>
                <TableCell align="center">Quantity</TableCell>
                <TableCell align="center">Selling price</TableCell>
                <TableCell align="center">Discount</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orderDetail.order_details.length &&
                orderDetail.order_details.map((item: any) => {
                  return (
                    <TableRow key={item.id} className={classes.wrapTableRow}>
                      <TableCell>{item.product.name}</TableCell>
                      <TableCell align="center">
                        <img src={item.product.image} alt="product" />
                      </TableCell>
                      <TableCell align="center">{item.quantity}</TableCell>
                      <TableCell align="center">
                        {item.product.selling_price}
                      </TableCell>
                      <TableCell align="center">
                        {item.product.discount}
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <div className={classes.wrapButton}>
          <Button variant="outlined" onClick={handleClose} color="error">
            Close
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default DashboardModal;
