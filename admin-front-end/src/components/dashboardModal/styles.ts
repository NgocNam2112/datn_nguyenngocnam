import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  wrapList: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 12,
  },
  wrapUserAvatar: {
    display: "flex",
    alignItems: "center",
  },
  wrapTableContainer: {
    maxHeight: 350,
    overflowY: "scroll",
  },
  wrapTableRow: {
    "& img": {
      width: 100,
      height: 100,
      objectFit: "contain",
    },
  },
  wrapButton: {
    marginTop: 12,
    display: "flex",
    justifyContent: "flex-end",
  },
}));

export default useStyles;
