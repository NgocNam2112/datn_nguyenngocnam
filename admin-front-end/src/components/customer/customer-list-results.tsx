import { useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { IUser } from "@/config/types";
import { useDispatch } from "react-redux";
import { openUserInfo } from "@/store/user/userSlice";
import { deleteUser, fetchUserById } from "@/store/user/userActions";
import { makeStyles } from "@material-ui/styles";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import MoreVertIcon from "@mui/icons-material/MoreVert";

interface IProps {
  listUserProfile: IUser[];
}

const useStyles = makeStyles((theme) => ({
  tableRow: {
    position: "relative",
  },
  menu: {
    position: "absolute",
    top: "12%",
    right: 0,
    transform: "traslate(-50%, 0)",
  },
}));

export const CustomerListResults: React.FC<IProps> = ({ listUserProfile }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const handleClickUser = (userId: number) => {
    dispatch(openUserInfo(true));
    dispatch(fetchUserById({ params: userId }));
  };
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDeleteUser = (id: number) => {
    dispatch(deleteUser({ params: id }));
  };

  return (
    <Card>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>Town</TableCell>
                <TableCell>Phone number</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listUserProfile &&
                listUserProfile.map((item: IUser) => (
                  <TableRow hover key={item.id} className={classes.tableRow}>
                    <TableCell onClick={() => handleClickUser(item.id)}>
                      <Box
                        sx={{
                          alignItems: "center",
                          display: "flex",
                        }}
                      >
                        <Avatar src={item.avatar} sx={{ mr: 2 }}></Avatar>
                        <Typography color="textPrimary" variant="body1">
                          {`${item.first_name} ${item.last_name}`}
                        </Typography>
                      </Box>
                    </TableCell>
                    <TableCell onClick={() => handleClickUser(item.id)}>
                      {item.mail_address}
                    </TableCell>
                    <TableCell onClick={() => handleClickUser(item.id)}>
                      {item.address}
                    </TableCell>
                    <TableCell onClick={() => handleClickUser(item.id)}>
                      {item.town}
                    </TableCell>
                    <TableCell>
                      {item.phone_number}
                      <div className={classes.menu}>
                        <Button
                          id="demo-positioned-button"
                          aria-controls={
                            open ? "demo-positioned-menu" : undefined
                          }
                          aria-haspopup="true"
                          aria-expanded={open ? "true" : undefined}
                          onClick={handleClick}
                        >
                          <MoreVertIcon />
                        </Button>
                        <Menu
                          id="demo-positioned-menu"
                          aria-labelledby="demo-positioned-button"
                          anchorEl={anchorEl}
                          open={open}
                          onClose={handleClose}
                          anchorOrigin={{
                            vertical: "top",
                            horizontal: "left",
                          }}
                          transformOrigin={{
                            vertical: "top",
                            horizontal: "left",
                          }}
                        >
                          <MenuItem
                            onClick={() => {
                              handleDeleteUser(item.id);
                              handleClose();
                            }}
                            style={{ color: "red" }}
                          >
                            Delete
                          </MenuItem>
                        </Menu>
                      </div>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
    </Card>
  );
};
