import { openAddNewUserInfo } from "@/store/user/userSlice";
import { Box, Button, Typography } from "@mui/material";
import { useDispatch } from "react-redux";

export const CustomerListToolbar = (props: any) => {
  const dispatch = useDispatch();

  const handleOpenAddNewUserInfo = () => {
    dispatch(openAddNewUserInfo(true));
  };

  return (
    <Box {...props}>
      <Box
        sx={{
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          flexWrap: "wrap",
          m: -1,
        }}
      >
        <Typography sx={{ m: 1 }} variant="h4">
          Customers
        </Typography>
        <Box sx={{ m: 1 }}>
          <Button
            color="primary"
            variant="contained"
            onClick={handleOpenAddNewUserInfo}
          >
            Add Customers
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
