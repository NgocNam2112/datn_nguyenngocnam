import axios, { AxiosRequestConfig, CancelTokenSource } from "axios";
import { BASE_URL } from "./config.constant";

const { CancelToken } = axios;
const source = CancelToken.source();

const instance = (
  authToken: string | null,
  cancelToken?: CancelTokenSource | undefined
) => {
  const defaultOptions: AxiosRequestConfig = {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: authToken ? `Bearer ${authToken}` : "",
    },
    baseURL: BASE_URL,
    cancelToken: cancelToken ? cancelToken.token : source.token,
  };

  return {
    get: (url: string, options: AxiosRequestConfig = {}) =>
      axios.get(url, {
        ...defaultOptions,
        ...options,
        headers: {
          ...defaultOptions.headers,
          ...options?.headers,
        },
      }),

    post: (url: string, data?: any, options: AxiosRequestConfig = {}) =>
      axios.post(url, data, {
        ...defaultOptions,
        ...options,
        headers: {
          ...defaultOptions.headers,
          ...options?.headers,
        },
      }),

    put: (url: string, data?: any, options: AxiosRequestConfig = {}) =>
      axios.put(url, data, {
        ...defaultOptions,
        ...options,
        headers: {
          ...defaultOptions.headers,
          ...options?.headers,
        },
      }),

    delete: (url: string, data?: any, options: AxiosRequestConfig = {}) =>
      axios.delete(url, {
        ...defaultOptions,
        ...options,
        headers: {
          ...defaultOptions.headers,
          ...options?.headers,
        },
      }),
  };
};

export default instance;
