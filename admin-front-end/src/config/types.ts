export interface IUser {
  id: number;
  mail_address?: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
  avatar: string;
  isProfile: boolean;
  rolesId: number;
}

export interface IProduct {
  id: number;
  image: string;
  selling_price: number;
  content: string;
  discount: number;
  space_for_discription: string;
  name: string;
  stock: number;
  farm: string;
  delivery_area: string;
  categoryId: number;
  rate: number;
}

export interface IDescription {
  id: number;
  origin: string;
  cook: string;
  vitamins: string;
  first_image: string;
  second_image: string;
  productId: number;
}

export interface ICategory {
  id: number;
  category_name: string;
  created_at?: string;
  updated_at?: string;
}

export interface IRoles {
  id: number;
  describe: string;
  created_at: string;
  updated_at: string;
}

export interface IOrder {
  id: number;
  created_at: string;
  user: {
    id: number;
    mail_address: string;
    first_name: string;
    last_name: string;
    avatar: string;
    phone_number: string;
    address: string;
    town: string;
  };
}

interface IPropsDetailOrder {
  id: number;
  quantity: number;
  buyTypesId: number;
  product: {
    id: number;
    image: string;
    selling_price: number;
    discount: number;
    name: string;
  };
}

export interface IOrderDetail extends IOrder {
  order_details: IPropsDetailOrder[];
}
