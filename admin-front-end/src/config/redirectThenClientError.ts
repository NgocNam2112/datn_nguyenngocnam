import FailedToCallApiError from "./FailToCallApiError";
import Router from "next/router";

export const redirectThenClientError = (error: FailedToCallApiError): void => {
  // 準正常系のケース
  if (!error.isError) return;
  if (!error.statusCode) {
    Router.push("/error");
  }
  switch (error.statusCode) {
    case 400:
      Router.push("/error");
      break;
    case 401:
      Router.push("/");
      break;
    case 404:
      Router.push("/error");
      break;
    case 405:
      error.reason && error.reason === "Expired Token"
        ? Router.push("/")
        : Router.push("/error");
      break;
    case 405005:
      error.reason && error.reason === "Expired Token"
        ? Router.push("/")
        : Router.push("/error");
      break;
    case 500:
      Router.push("/error");
      break;
    case 503:
      Router.push("/maintenance");
      break;
    default:
      Router.push("/error");
      break;
  }
};
