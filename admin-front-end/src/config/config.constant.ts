export const BASE_URL: string = "http://localhost:3201/";

export const SIGN_IN: string = "users";
export const PRODUCTS: string = "products";
export const DESCRIPTION: string = "description";
export const CATEGORY: string = "categories";
export const ROLES: string = "roles";
export const ORDER: string = "orders";
