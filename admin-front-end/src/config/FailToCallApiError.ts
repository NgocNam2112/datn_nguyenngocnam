import BaseError from "./BaseError";

class FailedToCallApiError extends BaseError {
  constructor(
    message: string,
    public statusCode?: number,
    public isError?: boolean,
    public reason?: string,
    public messages?: string[]
  ) {
    super(message);
    this.statusCode = statusCode;
    this.isError = isError;
    this.reason = reason;
    this.messages = messages;
  }
}

export default FailedToCallApiError;
