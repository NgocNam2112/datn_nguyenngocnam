import { createSlice } from "@reduxjs/toolkit";
import { IOrder, IOrderDetail } from "@/config/types";
import { fetchOrderByAdmin, fetchOrderIdByAdmin } from "./orderActions";

interface IOrderList {
  orderList: IOrder[];
  orderDetail: IOrderDetail;
  isOpenOrderDetail: boolean;
  error?: string;
}

const initialState: IOrderList = {
  orderList: [
    {
      id: 0,
      created_at: "",
      user: {
        id: 0,
        mail_address: "",
        first_name: "",
        last_name: "",
        avatar: "",
        phone_number: "",
        address: "",
        town: "",
      },
    },
  ],
  orderDetail: {
    id: 0,
    created_at: "",
    user: {
      id: 0,
      mail_address: "",
      first_name: "",
      last_name: "",
      avatar: "",
      phone_number: "",
      address: "",
      town: "",
    },
    order_details: [
      {
        id: 0,
        quantity: 0,
        buyTypesId: 0,
        product: {
          id: 0,
          image: "",
          selling_price: 0,
          discount: 0,
          name: "",
        },
      },
    ],
  },
  isOpenOrderDetail: false,
  error: "",
};

export const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {
    openOrderDetail: (state, action) => {
      state.isOpenOrderDetail = true;
    },
    closeOrderDetail: (state, action) => {
      state.isOpenOrderDetail = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchOrderByAdmin.fulfilled, (state, action) => {
      state.orderList = action.payload;
    });
    builder.addCase(fetchOrderByAdmin.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(fetchOrderIdByAdmin.fulfilled, (state, action) => {
      state.orderDetail = action.payload;
    });
    builder.addCase(fetchOrderIdByAdmin.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const { openOrderDetail, closeOrderDetail } = orderSlice.actions;

export const orderReducer = orderSlice.reducer;
