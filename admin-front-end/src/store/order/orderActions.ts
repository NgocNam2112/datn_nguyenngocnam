import {
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "./../apiError/apiErrorSlice";
import { IOrder, IOrderDetail } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import instance from "@/config/axios.config";
import { ORDER } from "@/config/config.constant";
import { openOrderDetail } from "./orderSlice";

export const fetchOrderByAdmin = createAsyncThunk<
  IOrder[],
  {},
  { rejectValue: ValidationErrors }
>("order/fetchOrderByAdmin", async ({}, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(
      `${ORDER}/fetch-order-by-admin`
    );
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const fetchOrderIdByAdmin = createAsyncThunk<
  IOrderDetail,
  { orderId: number; userId: number },
  { rejectValue: ValidationErrors }
>(
  "order/fetchOrderIdByAdmin",
  async ({ orderId, userId }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    try {
      const result = await instance(token, undefined).get(
        `${ORDER}/fetch-order-id-by-admin/${orderId}/${userId}`
      );
      if (result.status === 200) {
        dispatch(openOrderDetail(true));
      }
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const deleteOrderIdByAdmin = createAsyncThunk<
  any,
  { orderId: number; userId: number },
  { rejectValue: ValidationErrors }
>(
  "order/deleteOrderIdByAdmin",
  async ({ orderId, userId }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    try {
      const result = await instance(token, undefined).delete(
        `${ORDER}/delete-order-by-admin/${orderId}/${userId}`
      );
      if (result.status === 200) {
        dispatch(fetchOrderByAdmin({}));
      }
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
