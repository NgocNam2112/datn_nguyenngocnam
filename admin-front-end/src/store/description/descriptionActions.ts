import instance from "@/config/axios.config";
import { IDescription } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "@/store/apiError/apiErrorSlice";
import { BASE_URL, DESCRIPTION } from "@/config/config.constant";
import axios from "axios";
import { number } from "yup";
import { fetchAllProduct } from "../product/productActions";
import { closeAddNewProduct } from "../product/productSlice";

interface IInputDescription {
  origin: string;
  cook: string;
  vitamins: string;
  firstImageFile?: any;
  secondImageFile?: any;
  productId?: number;
}

export const fetchDescriptionByProductId = createAsyncThunk<
  IDescription,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "description/fetchDescriptionByProductId",
  async ({ params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const result = await instance("", undefined).get(
        `${DESCRIPTION}/${params}`
      );
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const upateDescription = createAsyncThunk<
  IDescription,
  { descriptionInfo: IInputDescription; params: number },
  { rejectValue: ValidationErrors }
>(
  "description/updateDescription",
  async ({ descriptionInfo, params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    const token = window.localStorage.getItem("accessToken");
    const form = new FormData();
    form.append("origin", descriptionInfo.origin);
    form.append("cook", descriptionInfo.cook);
    form.append("vitamins", descriptionInfo.vitamins);
    if (descriptionInfo.firstImageFile && descriptionInfo.secondImageFile) {
      form.append("files", descriptionInfo.firstImageFile);
      form.append("files", descriptionInfo.secondImageFile);
    }
    try {
      const result = await axios.put(
        `${BASE_URL}${DESCRIPTION}/update-with-image/${params}`,
        form,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return result.data;
    } catch (error) {
      console.log(error);
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const addNewDescription = createAsyncThunk<
  IDescription,
  { descriptionInfo: IInputDescription },
  { rejectValue: ValidationErrors }
>(
  "description/addNewDescription",
  async ({ descriptionInfo }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    const token = window.localStorage.getItem("accessToken");
    const form = new FormData();
    form.append("origin", descriptionInfo.origin);
    form.append("cook", descriptionInfo.cook);
    form.append("vitamins", descriptionInfo.vitamins);
    if (descriptionInfo.productId) {
      form.append("productId", descriptionInfo.productId.toString());
    }
    form.append("files", descriptionInfo.firstImageFile);
    form.append("files", descriptionInfo.secondImageFile);

    try {
      const result = await axios.post(
        `${BASE_URL}${DESCRIPTION}/add-new-description`,
        form,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.status === 201) {
        dispatch(closeAddNewProduct(false));
        dispatch(fetchAllProduct({}));
      }
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const deleteDescription = createAsyncThunk<
  any,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "description/deleteDescripton",
  async ({ params }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");

    try {
      await instance(token, undefined).delete(
        `${DESCRIPTION}/delete-description/${params}`
      );
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
