import { BASE_URL, SIGN_IN } from "./../../config/config.constant";
import instance from "@/config/axios.config";
import { IUser } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "./../apiError/apiErrorSlice";
import axios from "axios";
import { closeAddNewUserInfo, closeUserInfo } from "./userSlice";

export interface IInputUser {
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
  files?: any;
}

interface IInputAddNewUser {
  mail_address: string;
  password: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  address: string;
  town: string;
  rolesId: number;
  files?: any;
}

export const fetchUserProfile = createAsyncThunk<
  IUser,
  {},
  { rejectValue: ValidationErrors }
>("user/fetchUserProfile", async ({}, { rejectWithValue, dispatch }) => {
  clearApiError(dispatch)();
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(`${SIGN_IN}/profile`);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const updateProfile = createAsyncThunk<
  IUser,
  { userProfile: IInputUser },
  { rejectValue: ValidationErrors }
>(
  "user/updateProfile",
  async ({ userProfile }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    const form = new FormData();
    form.append("first_name", userProfile.first_name);
    form.append("last_name", userProfile.last_name);
    form.append("phone_number", userProfile.phone_number);
    form.append("address", userProfile.address);
    form.append("town", userProfile.town);
    try {
      if (userProfile.files) {
        form.append("files", userProfile.files);
        const result = await axios.put(
          `${BASE_URL}users/update-profile-with-image`,
          form,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `Bearer ${token}`,
            },
          }
        );
        dispatch(fetchUserProfile({}));
        return result.data;
      }

      const result = await instance(token, undefined).put(
        `${SIGN_IN}/update-profile-without-image`,
        form
      );
      if (result.status === 200) {
        dispatch(fetchUserProfile({}));
      }
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const fetchAllUser = createAsyncThunk<
  IUser[],
  {},
  { rejectValue: ValidationErrors }
>("user/fetchAllUser", async ({}, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(`${SIGN_IN}`);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const fetchUserById = createAsyncThunk<
  IUser,
  { params: number },
  { rejectValue: ValidationErrors }
>("user/fetchUserByIs", async ({ params }, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).get(
      `${SIGN_IN}/fetch-user-by-id/${params}`
    );
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const upateUserById = createAsyncThunk<
  any,
  { params: number; userInfo: IInputUser },
  { rejectValue: ValidationErrors }
>(
  "user/updateUserById",
  async ({ params, userInfo }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    const form = new FormData();
    form.append("first_name", userInfo.first_name);
    form.append("last_name", userInfo.last_name);
    form.append("address", userInfo.address);
    form.append("phone_number", userInfo.phone_number);
    form.append("town", userInfo.town);
    if (userInfo.files) {
      form.append("files", userInfo.files);
    }

    try {
      const result = await axios.put(
        `${BASE_URL}${SIGN_IN}/update-user-by-id/${params}`,
        form,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.status === 200) {
        console.log("running on here");
        dispatch(closeUserInfo(false));
        dispatch(fetchAllUser({}));
      }
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
export const createNewUser = createAsyncThunk<
  IUser,
  { userInfo: IInputAddNewUser },
  { rejectValue: ValidationErrors }
>("user/createNewUser", async ({ userInfo }, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  const form = new FormData();
  form.append("mail_address", userInfo.mail_address);
  form.append("password", userInfo.password);
  form.append("first_name", userInfo.first_name);
  form.append("last_name", userInfo.last_name);
  form.append("address", userInfo.address);
  form.append("phone_number", userInfo.phone_number);
  form.append("town", userInfo.town);
  form.append("rolesId", userInfo.rolesId.toString());
  form.append("files", userInfo.files);

  try {
    const result = await axios.post(
      `${BASE_URL}${SIGN_IN}/create-new-user`,
      form,
      {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (result.status === 201) {
      dispatch(closeAddNewUserInfo(false));
      dispatch(fetchAllUser({}));
    }
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const deleteUser = createAsyncThunk<
  any,
  { params: number },
  { rejectValue: ValidationErrors }
>("user/deleteUser", async ({ params }, { rejectWithValue, dispatch }) => {
  const token = window.localStorage.getItem("accessToken");
  try {
    const result = await instance(token, undefined).delete(
      `${SIGN_IN}/delete-user/${params}`
    );
    if (result.status === 200) {
      dispatch(fetchAllUser({}));
    }
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});
