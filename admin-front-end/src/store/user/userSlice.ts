import { IUser } from "@/config/types";
import { createSlice } from "@reduxjs/toolkit";
import {
  fetchAllUser,
  fetchUserById,
  fetchUserProfile,
  upateUserById,
} from "./userActions";

interface IUserList {
  listUserProfile: IUser[];
  userProfile: IUser;
  isOpenUserInfo: boolean;
  isOpenAddnewUserInfo: boolean;
  error?: string;
}
const initialState: IUserList = {
  listUserProfile: [
    {
      id: 0,
      first_name: "",
      last_name: "",
      phone_number: "",
      address: "",
      town: "",
      avatar: "",
      rolesId: 0,
      isProfile: false,
    },
  ],
  userProfile: {
    id: 0,
    first_name: "",
    last_name: "",
    phone_number: "",
    address: "",
    town: "",
    avatar: "",
    rolesId: 0,
    isProfile: false,
  },
  isOpenUserInfo: false,
  isOpenAddnewUserInfo: false,
  error: "",
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    openUserInfo: (state, action) => {
      state.isOpenUserInfo = true;
    },

    closeUserInfo: (state, action) => {
      state.isOpenUserInfo = false;
    },

    openAddNewUserInfo: (state, action) => {
      state.isOpenAddnewUserInfo = true;
    },
    closeAddNewUserInfo: (state, action) => {
      state.isOpenAddnewUserInfo = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchUserProfile.fulfilled, (state, action) => {
      state.userProfile = action.payload;
    });

    builder.addCase(fetchUserProfile.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(fetchAllUser.fulfilled, (state, action) => {
      state.listUserProfile = action.payload;
    });
    builder.addCase(fetchAllUser.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(fetchUserById.fulfilled, (state, action) => {
      state.userProfile = action.payload;
    });
    builder.addCase(fetchUserById.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(upateUserById.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const {
  openUserInfo,
  closeUserInfo,
  closeAddNewUserInfo,
  openAddNewUserInfo,
} = userSlice.actions;

export const userReducer = userSlice.reducer;
