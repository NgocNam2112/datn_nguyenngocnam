import instance from "@/config/axios.config";
import { BASE_URL, PRODUCTS } from "@/config/config.constant";
import { IProduct } from "@/config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "../apiError/apiErrorSlice";
import {
  addNewDescription,
  deleteDescription,
} from "../description/descriptionActions";
import { closeProductDialog, openProductDialog } from "./productSlice";

interface IInputProduct {
  files?: any;
  selling_price: number;
  discount: number;
  stock: number;
  rate: number;
  farm: string;
  content: string;
  name: string;
  delivery_area: string;
  space_for_discription: string;
  categoryId: number;
}

interface IInputProductWithDescription {
  name: string;
  selling_price: number;
  stock: number;
  farm: string;
  discount: number;
  space_for_discription: string;
  rate: number;
  categoryId: number;
  delivery_area: string;
  content: string;
  origin: string;
  cook: string;
  vitamins: string;
  files?: any;
  firstImageFileDescriptions?: any;
  secondImageFileDescription?: any;
}

export const fetchAllProduct = createAsyncThunk<
  IProduct[],
  {},
  { rejectValue: ValidationErrors }
>("product/fetchAllProduct", async ({}, { rejectWithValue, dispatch }) => {
  clearApiError(dispatch)();
  try {
    const result = await instance("", undefined).get(PRODUCTS);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const fetchProductById = createAsyncThunk<
  IProduct,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "product/fetchProductById",
  async ({ params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const { data } = await instance("", undefined).get(
        `${PRODUCTS}/${params}`
      );
      dispatch(openProductDialog(true));
      return data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const updateProductById = createAsyncThunk<
  IProduct,
  { params: number; productInfo: IInputProduct },
  { rejectValue: ValidationErrors }
>(
  "product/updateProductById",
  async ({ params, productInfo }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    const form = new FormData();
    form.append("selling_price", productInfo.selling_price.toString());
    form.append("discount", productInfo.discount.toString());
    form.append("stock", productInfo.stock.toString());
    form.append("farm", productInfo.farm);
    form.append("content", productInfo.content);
    form.append("name", productInfo.name);
    form.append("delivery_area", productInfo.delivery_area);
    form.append("space_for_discription", productInfo.space_for_discription);
    form.append("categoryId", productInfo.categoryId.toString());
    form.append("rate", productInfo.rate.toString());
    if (productInfo.files) {
      form.append("files", productInfo.files);
    }
    try {
      const result = await axios.put(
        `${BASE_URL}${PRODUCTS}/update-product/${params}`,
        form,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        }
      );
      dispatch(closeProductDialog(false));
      dispatch(fetchAllProduct({}));
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const searchProduct = createAsyncThunk<
  IProduct[],
  string,
  { rejectValue: ValidationErrors }
>(
  "product/searchProduct",
  async (productName, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const { data } = await instance("", undefined).get(
        `${PRODUCTS}/search/${productName}`
      );
      return data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const addNewProduct = createAsyncThunk<
  IProduct,
  { productInfo: IInputProductWithDescription },
  { rejectValue: ValidationErrors }
>(
  "product/addNewProduct",
  async ({ productInfo }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    const form = new FormData();
    form.append("name", productInfo.name);
    form.append("selling_price", productInfo.selling_price.toString());
    form.append("stock", productInfo.stock.toString());
    form.append("rate", productInfo.rate.toString());
    form.append("categoryId", productInfo.categoryId.toString());
    form.append("content", productInfo.content);
    form.append("delivery_area", productInfo.delivery_area);
    form.append("space_for_discription", productInfo.space_for_discription);
    form.append("discount", productInfo.discount.toString());
    form.append("farm", productInfo.farm);
    form.append("files", productInfo.files);
    try {
      const result = await axios.post(
        `${BASE_URL}${PRODUCTS}/new-product`,
        form,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
          },
        }
      );
      dispatch(
        addNewDescription({
          descriptionInfo: {
            origin: productInfo.origin,
            cook: productInfo.cook,
            vitamins: productInfo.vitamins,
            productId: +result.data.id,
            firstImageFile: productInfo.firstImageFileDescriptions,
            secondImageFile: productInfo.secondImageFileDescription,
          },
        })
      );
      return result.data;
    } catch (error) {
      console.log("error", error);
    }
  }
);

export const deleteProduct = createAsyncThunk<
  any,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "product/deleteProduct",
  async ({ params }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");

    try {
      dispatch(deleteDescription({ params: params }));
      const result = await instance(token, undefined).delete(
        `${PRODUCTS}/delete-product/${params}`
      );

      dispatch(fetchAllProduct({}));
    } catch (error) {}
  }
);
