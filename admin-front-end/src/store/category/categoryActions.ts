import {
  closeAddNewCategoryPopup,
  closeCategoryPopup,
} from "@/store/category/categorySlice";
import { CATEGORY } from "../../config/config.constant";
import {
  clearApiError,
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "@/store/apiError/apiErrorSlice";
import { ICategory } from "../../config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import instance from "@/config/axios.config";

export const fetchAllCategory = createAsyncThunk<
  ICategory[],
  {},
  { rejectValue: ValidationErrors }
>("category/fetchAllCategory", async ({}, { rejectWithValue, dispatch }) => {
  clearApiError(dispatch)();
  try {
    const result = await instance("", undefined).get(CATEGORY);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});

export const fetchCategoryById = createAsyncThunk<
  ICategory,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "category/fetchCategoryById",
  async ({ params }, { rejectWithValue, dispatch }) => {
    clearApiError(dispatch)();
    try {
      const result = await instance("", undefined).get(`${CATEGORY}/${params}`);
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const updateCategoryById = createAsyncThunk<
  ICategory,
  { params: number; category_name: string },
  { rejectValue: ValidationErrors }
>(
  "category/updateCategoryById",
  async ({ params, category_name }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    try {
      const result = await instance(token, undefined).put(
        `${CATEGORY}/update-category/${params}`,
        { category_name }
      );
      if (result.status === 200) {
        dispatch(closeCategoryPopup(false));
        dispatch(fetchAllCategory({}));
      }
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const createNewCategory = createAsyncThunk<
  ICategory,
  { category_name: string },
  { rejectValue: ValidationErrors }
>(
  "category/createNewCategory",
  async ({ category_name }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    try {
      const result = await instance(token, undefined).post(
        `${CATEGORY}/new-category`,
        { category_name: category_name }
      );
      if (result.status === 201) {
        dispatch(closeAddNewCategoryPopup(false));
        dispatch(fetchAllCategory({}));
      }
      return result.data;
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);

export const deleteCategoryById = createAsyncThunk<
  any,
  { params: number },
  { rejectValue: ValidationErrors }
>(
  "category/deleteCategoryById",
  async ({ params }, { rejectWithValue, dispatch }) => {
    const token = window.localStorage.getItem("accessToken");
    try {
      const result = await instance(token, undefined).delete(
        `${CATEGORY}/delete-category/${params}`
      );
      if (result.status === 200) {
        dispatch(fetchAllCategory({}));
      }
    } catch (error) {
      return defaultCallApiErrorHandler(error, {
        rejectWithValue,
        dispatch,
      });
    }
  }
);
