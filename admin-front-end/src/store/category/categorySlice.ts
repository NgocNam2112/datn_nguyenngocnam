import { Satellite } from "@material-ui/icons";
import { createSlice } from "@reduxjs/toolkit";
import { ICategory } from "./../../config/types";
import { fetchAllCategory, fetchCategoryById } from "./categoryActions";

interface ICategoryList {
  categoryList: ICategory[];
  categoryById: ICategory;
  isOpencategoryPopup: boolean;
  isOpencateAddNewgoryPopup: boolean;
  error?: string;
}

const initialState: ICategoryList = {
  categoryList: [
    {
      id: 0,
      category_name: "",
    },
  ],
  categoryById: {
    id: 0,
    category_name: "",
  },
  isOpencategoryPopup: false,
  isOpencateAddNewgoryPopup: false,
  error: "",
};

export const categorySlice = createSlice({
  name: "category",
  initialState,
  reducers: {
    openCategoryPopup: (state, action) => {
      state.isOpencategoryPopup = true;
    },
    closeCategoryPopup: (state, action) => {
      state.isOpencategoryPopup = false;
    },

    openAddNewCategoryPopup: (state, action) => {
      state.isOpencateAddNewgoryPopup = true;
    },
    closeAddNewCategoryPopup: (state, action) => {
      state.isOpencateAddNewgoryPopup = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAllCategory.fulfilled, (state, action) => {
      state.categoryList = action.payload;
    });
    builder.addCase(fetchAllCategory.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });

    builder.addCase(fetchCategoryById.fulfilled, (state, action) => {
      state.categoryById = action.payload;
    });
    builder.addCase(fetchCategoryById.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const {
  openCategoryPopup,
  closeCategoryPopup,
  openAddNewCategoryPopup,
  closeAddNewCategoryPopup,
} = categorySlice.actions;

export const categoryReducer = categorySlice.reducer;
