import { orderReducer } from "./order/orderSlice";
import { configureStore } from "@reduxjs/toolkit";
import { apiErrorReducer } from "./apiError/apiErrorSlice";
import { userReducer } from "./user/userSlice";
import { createWrapper } from "next-redux-wrapper";
import { productReducer } from "./product/productSlice";
import { descriptionReducer } from "./description/descriptionSlice";
import { categoryReducer } from "./category/categorySlice";
import { rolesReducer } from "./roles/rolesSlice";

const store = configureStore({
  reducer: {
    user: userReducer,
    category: categoryReducer,
    product: productReducer,
    description: descriptionReducer,
    apiError: apiErrorReducer,
    order: orderReducer,
    roles: rolesReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

const makeStore = () => store;
export type AppStore = ReturnType<typeof makeStore>;
export const wrapper = createWrapper<AppStore>(makeStore);
export type RootState = ReturnType<typeof store.getState>;

export default store;
