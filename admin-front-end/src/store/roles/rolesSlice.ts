import { createSlice } from "@reduxjs/toolkit";
import { IRoles } from "@/config/types";
import { fetchAllRoles } from "./rolesActions";

interface IRolesList {
  rolesList: IRoles[];
  error?: string;
}

const initialState: IRolesList = {
  rolesList: [{ id: 0, describe: "", created_at: "", updated_at: "" }],
  error: "",
};

export const rolesSlice = createSlice({
  name: "roles",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAllRoles.fulfilled, (state, action) => {
      state.rolesList = action.payload;
    });
    builder.addCase(fetchAllRoles.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message as string;
      }
    });
  },
});

export const rolesReducer = rolesSlice.reducer;
