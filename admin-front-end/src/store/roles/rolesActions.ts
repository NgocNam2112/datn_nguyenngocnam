import instance from "@/config/axios.config";
import {
  defaultCallApiErrorHandler,
  ValidationErrors,
} from "./../apiError/apiErrorSlice";
import { IRoles } from "./../../config/types";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { ROLES } from "@/config/config.constant";

export const fetchAllRoles = createAsyncThunk<
  IRoles[],
  {},
  { rejectValue: ValidationErrors }
>("roles/fetchAllRoles", async ({}, { rejectWithValue, dispatch }) => {
  try {
    const result = await instance("", undefined).get(`${ROLES}`);
    return result.data;
  } catch (error) {
    return defaultCallApiErrorHandler(error, {
      rejectWithValue,
      dispatch,
    });
  }
});
