import Head from "next/head";
import { Box, Container } from "@mui/material";
import { CustomerListResults } from "../components/customer/customer-list-results";
import { CustomerListToolbar } from "../components/customer/customer-list-toolbar";
import { DashboardLayout } from "../components/dashboard-layout";
import UserInfoPopup from "@/components/userPopup/UserInfoPopup";
import { RootState } from "@/store";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchAllUser } from "@/store/user/userActions";
import AddNewUserInfoPopup from "@/components/userPopup/AddNewUserInfoPopup";
import { fetchAllRoles } from "@/store/roles/rolesActions";

const Customers = () => {
  const dispatch = useDispatch();
  const { isOpenUserInfo, listUserProfile, isOpenAddnewUserInfo } = useSelector(
    (state: RootState) => state.user
  );

  useEffect(() => {
    Promise.all([dispatch(fetchAllUser({})), dispatch(fetchAllRoles({}))]);
  }, [dispatch]);

  return (
    <>
      <Head>
        <title>Customers</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth={false}>
          <CustomerListToolbar />
          <Box
            sx={{ mt: 3 }}
            style={{
              maxHeight: 500,
              overflowY: "scroll",
              boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
              marginTop: 12,
            }}
          >
            <CustomerListResults listUserProfile={listUserProfile} />
          </Box>
        </Container>
      </Box>
      {isOpenUserInfo && <UserInfoPopup />}
      {isOpenAddnewUserInfo && <AddNewUserInfoPopup />}
    </>
  );
};
Customers.getLayout = (page: any) => <DashboardLayout>{page}</DashboardLayout>;

export default Customers;
