import Head from "next/head";
import { Box, Container, Grid } from "@mui/material";
import { DashboardLayout } from "../components/dashboard-layout";
import { Budget } from "@/components/dashboard/budget";
import { TotalCustomers } from "@/components/dashboard/total-customers";
import { TasksProgress } from "@/components/dashboard/tasks-progress";
import { TotalProfit } from "@/components/dashboard/total-profit";
import DashboardTable from "@/components/dashboard/dashboardTable/dashboardTable";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/store";
import { useEffect } from "react";
import { fetchOrderByAdmin } from "@/store/order/orderActions";
import DashboardModal from "@/components/dashboardModal/DashboardModal";

const Dashboard = () => {
  const dispatch = useDispatch();
  const { orderList, isOpenOrderDetail } = useSelector(
    (state: RootState) => state.order
  );

  useEffect(() => {
    dispatch(fetchOrderByAdmin({}));
  }, [dispatch]);

  return (
    <>
      <Head>
        <title>Dashboard</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth={false}>
          {/* <Grid container spacing={3}>
            <Grid item lg={3} sm={6} xl={3} xs={12}>
              <Budget />
            </Grid>
            <Grid item xl={3} lg={3} sm={6} xs={12}>
              <TotalCustomers />
            </Grid>
            <Grid item xl={3} lg={3} sm={6} xs={12}>
              <TasksProgress />
            </Grid>
            <Grid item xl={3} lg={3} sm={6} xs={12}>
              <TotalProfit sx={{ height: "100%" }} />
            </Grid>
          </Grid> */}
          <DashboardTable orderList={orderList} />
        </Container>
      </Box>
      {isOpenOrderDetail && <DashboardModal />}
    </>
  );
};

Dashboard.getLayout = (page: any) => <DashboardLayout>{page}</DashboardLayout>;

export default Dashboard;
