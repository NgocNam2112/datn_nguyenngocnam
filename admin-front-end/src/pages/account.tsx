import Head from "next/head";
import { DashboardLayout } from "../components/dashboard-layout";
import AccountProfileComopent from "@/components/account/AccountProfile";

const Account = () => {
  return (
    <>
      <Head>
        <title>Account | Material Kit</title>
      </Head>
      <AccountProfileComopent />
    </>
  );
};

Account.getLayout = (page: any) => <DashboardLayout>{page}</DashboardLayout>;

export default Account;
