import Head from "next/head";
import NextLink from "next/link";
import { useRouter } from "next/router";
import {
  Box,
  Button,
  Container,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { SubmitHandler, useForm } from "react-hook-form";
import ErrorMessage from "@/components/helper/errorMessage/ErrorMessage";
import instance from "@/config/axios.config";
import { SIGN_IN } from "@/config/config.constant";
import { useState } from "react";

interface FormValues {
  mail_address: string;
  password: string;
}

const Login = () => {
  const router = useRouter();
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<FormValues>();
  const [error, setError] = useState<string | undefined>();

  const onSubmit: SubmitHandler<FormValues> = async (formData) => {
    try {
      const result = await instance("", undefined).post(
        `${SIGN_IN}/sign-in-admin`,
        {
          mail_address: formData.mail_address,
          password: formData.password,
        }
      );
      if (result.status === 201) {
        window.localStorage.setItem("accessToken", result.data.accessToken);
        setError("");
        router.push("/");
      }
    } catch (error: any) {
      setError(error?.response.data.message);
    }
  };

  return (
    <>
      <Head>
        <title>Login | Admin</title>
      </Head>
      <Box
        component="main"
        sx={{
          alignItems: "center",
          display: "flex",
          flexGrow: 1,
          minHeight: "100%",
        }}
      >
        <Container maxWidth="sm">
          <NextLink href="/" passHref>
            <Button
              component="a"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Dashboard
            </Button>
          </NextLink>
          <form>
            <Box sx={{ my: 3 }}>
              <Typography color="textPrimary" variant="h4">
                Sign in
              </Typography>
            </Box>
            {error && <ErrorMessage errorMessage={error as string} />}
            <TextField
              fullWidth
              label="Email Address"
              margin="normal"
              variant="outlined"
              {...register("mail_address", {
                required: { value: true, message: "Email is not empty" },
                maxLength: {
                  value: 256,
                  message:
                    "Please enter one or more and 256 characters or less for the email.",
                },
                pattern: {
                  value:
                    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message: "Some characters cannot be used.",
                },
              })}
            />
            {errors.mail_address && (
              <ErrorMessage
                errorMessage={errors.mail_address.message as string}
              />
            )}
            <TextField
              fullWidth
              label="Password"
              type="password"
              variant="outlined"
              {...register("password", {
                required: {
                  value: true,
                  message: "Password is not empty",
                },
                minLength: {
                  value: 6,
                  message: "Please enter the password from 8 to 20 characters.",
                },
                maxLength: {
                  value: 20,
                  message: "Please enter the password from 8 to 20 characters.",
                },
              })}
            />
            {errors.password && (
              <ErrorMessage errorMessage={errors.password.message as string} />
            )}
            <Box sx={{ py: 2 }}>
              <Button
                color="primary"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                onClick={handleSubmit(onSubmit)}
              >
                Sign In Now
              </Button>
            </Box>
            <Typography color="textSecondary" variant="body2">
              Don&apos;t have an account?{" "}
              <NextLink href="/register">
                <Link
                  href="/register"
                  variant="subtitle2"
                  underline="hover"
                  sx={{
                    cursor: "pointer",
                  }}
                >
                  Sign Up
                </Link>
              </NextLink>
            </Typography>
          </form>
        </Container>
      </Box>
    </>
  );
};

export default Login;
