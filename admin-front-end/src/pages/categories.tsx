import CategoriesComponent from "@/components/categories/categories";
import Head from "next/head";
import { DashboardLayout } from "../components/dashboard-layout";
import Box from "@mui/material/Box";
import { useDispatch, useSelector } from "react-redux";
import { fetchAllCategory } from "@/store/category/categoryActions";
import React, { useEffect } from "react";
import { RootState } from "@/store";
import CategpryInfoModal from "@/components/categoryInfoModal/CategpryInfoModal";
import { CategoryToolbar } from "@/components/categories/category-list-toolbar";
import AddNewCategoryInfoModal from "@/components/categoryInfoModal/AddNewCategoryInfoModal";

const Categories = () => {
  const dispatch = useDispatch();
  const { categoryList, isOpencategoryPopup, isOpencateAddNewgoryPopup } =
    useSelector((state: RootState) => state.category);

  useEffect(() => {
    dispatch(fetchAllCategory({}));
  }, [dispatch]);

  return (
    <>
      <Head>
        <title>Categories</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <CategoryToolbar />
        <CategoriesComponent categoryList={categoryList} />
      </Box>
      {isOpencategoryPopup && <CategpryInfoModal />}
      {isOpencateAddNewgoryPopup && <AddNewCategoryInfoModal />}
    </>
  );
};

Categories.getLayout = (page: any) => <DashboardLayout>{page}</DashboardLayout>;

export default Categories;
