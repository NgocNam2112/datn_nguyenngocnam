import Head from "next/head";
import { Box, Container, Grid, Pagination } from "@mui/material";
import { DashboardLayout } from "../components/dashboard-layout";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import CustomTableRow from "@/components/helper/customTableRow/CustomTableRow";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { fetchAllProduct } from "@/store/product/productActions";
import { RootState } from "@/store";
import { IProduct } from "@/config/types";
import ProductModal from "@/components/productModal/ProductModal";
import AddNewProductModal from "@/components/productModal/AddNewProductModal";
import { Button, Typography } from "@mui/material";
import { openAddNewProduct } from "@/store/product/productSlice";

const Products = () => {
  const dispatch = useDispatch();
  const { productList, isOpenProductDialog, isOpenAddNewProduct } = useSelector(
    (state: RootState) => state.product
  );

  useEffect(() => {
    dispatch(fetchAllProduct({}));
  }, [dispatch]);

  return (
    <>
      <Head>
        <title>Products | Material Kit</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth={false}>
          <Box>
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                flexWrap: "wrap",
                m: -1,
              }}
            >
              <Typography sx={{ m: 1 }} variant="h4">
                Products
              </Typography>
              <Box sx={{ m: 1 }}>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={() => dispatch(openAddNewProduct(true))}
                >
                  Add products
                </Button>
              </Box>
            </Box>
          </Box>
          <Box
            sx={{ pt: 3 }}
            style={{
              maxHeight: 500,
              overflowY: "scroll",
              boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
              marginTop: 12,
            }}
          >
            <Grid container spacing={3} style={{ padding: "0 24px" }}>
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell align="center">Image</TableCell>
                      <TableCell align="center">Selling price</TableCell>
                      <TableCell align="center">Discount</TableCell>
                      <TableCell align="center">Rate</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {productList &&
                      (productList as IProduct[]).map((item: IProduct) => (
                        <CustomTableRow
                          key={item.id}
                          id={item.id}
                          name={item.name}
                          image={item.image}
                          selling_price={item.selling_price}
                          discount={item.discount}
                          rate={item.rate}
                        />
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Box>
        </Container>
      </Box>

      {isOpenProductDialog && <ProductModal />}
      {isOpenAddNewProduct && <AddNewProductModal />}
    </>
  );
};

Products.getLayout = (page: any) => <DashboardLayout>{page}</DashboardLayout>;

export default Products;
