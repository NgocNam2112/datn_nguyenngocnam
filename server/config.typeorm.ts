// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
import { TypeOrmModule } from '@nestjs/typeorm';

export const config = TypeOrmModule.forRoot({
  type: 'postgres',
  // host: process.env.HOST,
  port: +process.env.PORT,
  username: process.env.USER_NAME,
  password: process.env.PASSWORD,
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: true,
});
