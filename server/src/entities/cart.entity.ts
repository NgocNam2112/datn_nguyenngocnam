import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { BuyTypes } from './buy-type.entity';
import { Products } from './products.entity';
import { Users } from './users.entity';

@Entity('carts')
export class Cart {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  quantity: number;

  @CreateDateColumn()
  create_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  userId: number;

  @Column()
  productId: number;

  @ManyToOne(() => Users, (users) => users.carts)
  user: Users;

  @ManyToOne(() => Products, (products) => products.carts)
  product: Products;

  @Column()
  buyTypesId: number;

  @ManyToOne(() => BuyTypes, (buyTypes) => buyTypes.carts)
  buy_types: BuyTypes;
}
