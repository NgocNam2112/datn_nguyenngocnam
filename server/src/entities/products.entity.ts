import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Cart } from './cart.entity';
import { Categories } from './category.entity';
import { DeliveryTypes } from './delivery-type.entity';
import { OrderDetails } from './order-details.entity';
import { Questions } from './questions.entity';
import { Reviews } from './reviews.entity';

@Entity('products')
export class Products {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column()
  image: string;

  @Column()
  selling_price: number;

  @Column()
  discount: number;

  @Column()
  space_for_discription: string;

  @Column()
  name: string;

  @Column()
  stock: number;

  @Column()
  farm: string;

  @Column()
  rate: number;

  @Column()
  delivery_area: string;

  @Column()
  content: string;

  @Column({ nullable: true })
  cloudinary_id: string;

  @Column()
  categoryId: number;

  @ManyToOne(() => Categories, (category) => category.products)
  category: Categories;

  @OneToMany(() => DeliveryTypes, (delivery_types) => delivery_types.product)
  delivery_types: DeliveryTypes[];

  @OneToMany(() => Reviews, (reviews) => reviews.product)
  reviews: Reviews[];

  @OneToMany(() => Questions, (questions) => questions.product)
  questions: Questions[];

  @OneToMany(() => Cart, (carts) => carts.product)
  carts: Cart[];

  @OneToMany(() => OrderDetails, (order_details) => order_details.product)
  order_details: OrderDetails[];

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
