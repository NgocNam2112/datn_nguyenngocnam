import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { Users } from './users.entity';

@Entity('countries')
export class Countries {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column()
  country_name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => Users, (user) => user.country)
  users: Users[];
}
