import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { OrderDetails } from './order-details.entity';
import { Users } from './users.entity';

@Entity('orders')
export class Order {
  @PrimaryGeneratedColumn()
  public id: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  userId: number;

  @ManyToOne(() => Users, (users) => users.orders)
  user: Users;

  @OneToMany(() => OrderDetails, (order_details) => order_details.order)
  order_details: OrderDetails[];
}
