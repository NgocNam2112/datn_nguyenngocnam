import {
  PrimaryGeneratedColumn,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Blog } from './blog.entity';
import { Categories } from './category.entity';

@Entity('blogs_category')
export class BlogsCategory {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  upadted_at: Date;

  @ManyToOne(() => Categories, (category) => category.blog_categories)
  category: Categories;

  @OneToMany(() => Blog, (blog) => blog.blog_category)
  blogs: Blog[];
}
