import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { Products } from './products.entity';
import { Users } from './users.entity';

@Entity('questions')
export class Questions {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column()
  content: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Products, (products) => products.questions)
  product: Products;

  @ManyToOne(() => Users, (user) => user.questions)
  user: Users;
}
