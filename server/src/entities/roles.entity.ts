import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  JoinColumn,
  Column,
} from 'typeorm';
import { Users } from './users.entity';

@Entity('roles')
export class Roles {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: true })
  describe: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => Users, (user) => user.roles)
  @JoinColumn()
  users: Users[];
}
