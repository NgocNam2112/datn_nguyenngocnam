import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Products } from './products.entity';

@Entity('delivery_types')
export class DeliveryTypes {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column()
  type_name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Products, (products) => products.delivery_types)
  product: Products;
}
