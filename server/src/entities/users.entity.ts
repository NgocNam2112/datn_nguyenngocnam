// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { Reviews } from './reviews.entity';
import { Questions } from './questions.entity';
import { Roles } from './roles.entity';
import { Countries } from './country.entity';
import { Cart } from './cart.entity';
import { Order } from './order.entity';

@Entity('users')
export class Users {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    unique: true,
  })
  mail_address: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  avatar: string;

  @Column({ nullable: true })
  cloudinary_id: string;

  @Column({ nullable: true })
  token: string;

  @Column({
    nullable: true,
  })
  token_expired: Date;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column({
    nullable: true,
    unique: true,
  })
  tmp_otp: string;

  @Column({ nullable: true })
  otpExpired: Date;

  @Column()
  rolesId: number;

  @ManyToOne(() => Roles, (roles) => roles.users)
  @JoinColumn()
  roles: Roles;

  @Column({ nullable: true })
  isProfile: boolean;

  @Column({ nullable: true })
  first_name: string;

  @Column({ nullable: true })
  last_name: string;

  @Column({ nullable: true })
  phone_number: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  town: string;

  @BeforeInsert()
  hashPassword() {
    this.password = bcrypt.hashSync(this.password, process.env.SALT_PASSWORD);
  }

  comparePassword(enteredPassword: string) {
    return bcrypt.compareSync(enteredPassword, this.password);
  }

  getRefreshToken = function () {
    const token_expired = new Date(24 * 60 * 60 * 1000);
    const token = jwt.sign({ id: this.id }, process.env.SECRET_TOKEN, {
      expiresIn: 24 * 60 * 60 * 1000,
    });
    return { token_expired, token };
  };

  @OneToMany(() => Reviews, (reviews) => reviews.user)
  reviews: Reviews[];

  @OneToMany(() => Questions, (question) => question.user)
  questions: Questions[];

  @ManyToOne(() => Countries, (country) => country.users)
  @JoinColumn()
  country: Countries;

  @OneToMany(() => Cart, (carts) => carts.user)
  carts: Cart[];

  @OneToMany(() => Order, (orders) => orders.user)
  orders: Order[];
}
