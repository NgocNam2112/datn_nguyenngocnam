import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { BlogsCategory } from './blog-category.entity';
import { Products } from './products.entity';

@Entity('categories')
export class Categories {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  category_name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  upadted_at: Date;

  @OneToMany(() => Products, (products) => products.category)
  products: Products[];

  @OneToMany(() => BlogsCategory, (blog_category) => blog_category.category)
  blog_categories: BlogsCategory[];
}
