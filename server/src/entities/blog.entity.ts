import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { BlogsCategory } from './blog-category.entity';

@Entity('blogs')
export class Blog {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column()
  title: string;

  @Column()
  content: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => BlogsCategory, (blog_category) => blog_category.blogs)
  blog_category: BlogsCategory;
}
