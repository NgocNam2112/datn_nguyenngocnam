import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { Cart } from './cart.entity';
import { OrderDetails } from './order-details.entity';
import { Order } from './order.entity';
import { Products } from './products.entity';

@Entity('buy_types')
export class BuyTypes {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column()
  unit_type: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => Cart, (carts) => carts.buy_types)
  carts: Cart[];

  @OneToMany(() => OrderDetails, (order_detail) => order_detail.buy_types)
  orders: Order[];
}
