import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  UpdateDateColumn,
  CreateDateColumn,
} from 'typeorm';
import { Products } from './products.entity';
import { Users } from './users.entity';

@Entity('reviews')
export class Reviews {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column()
  content: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  productId: number;

  @Column()
  userId: number;

  @ManyToOne(() => Products, (products) => products.reviews)
  product: Products;

  @ManyToOne(() => Users, (users) => users.reviews)
  user: Users;
}
