import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BuyTypes } from './buy-type.entity';
import { Order } from './order.entity';
import { Products } from './products.entity';

@Entity('order-details')
export class OrderDetails {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  quantity: number;

  @ManyToOne(() => Order, (order) => order.order_details)
  order: Order;

  @ManyToOne(() => Products, (product) => product.order_details)
  product: Products;

  @Column()
  buyTypesId: number;

  @Column()
  productId: number;

  @Column()
  orderId: number;

  @ManyToOne(() => BuyTypes, (buyTypes) => buyTypes.orders)
  buy_types: BuyTypes;

  @CreateDateColumn()
  create_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
