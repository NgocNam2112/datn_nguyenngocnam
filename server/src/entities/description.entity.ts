import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
} from 'typeorm';
import { Products } from './products.entity';
import 'reflect-metadata';

@Entity('descriptions')
export class Descriptions {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  origin: string;

  @Column()
  cook: string;

  @Column()
  vitamins: string;

  @CreateDateColumn()
  created_at: Date;

  @Column({ nullable: true })
  first_image: string;

  @Column({ nullable: true })
  first_image_cloudinary_id: string;

  @Column({ nullable: true })
  second_image: string;

  @Column({ nullable: true })
  second_image_cloudinary_id: string;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  productId: number;

  @OneToOne(() => Products)
  product: Products;
}
