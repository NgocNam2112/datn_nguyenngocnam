import { HttpException } from '@nestjs/common';

export const globalResponse = (status, error) => {
  throw new HttpException(
    {
      status: status,
      message: error,
    },
    status,
  );
};
