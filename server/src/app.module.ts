import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { config } from 'config.typeorm';
import { AuthModule } from './components/auth/auth.module';
import { BuyTypeModule } from './components/byType/buyType.module';
import { Cartmodule } from './components/cart/cart.module';
import { CategoryModule } from './components/category/category.module';
import { CountriesModule } from './components/countries/country.module';
import { DescriptionModule } from './components/description/description.module';
import { OrderModule } from './components/order/order.module';
import { ProductModule } from './components/product/product.module';
import { ReviewModule } from './components/review/review.module';
import { RolesModule } from './components/roles/roles.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: true,
      envFilePath: ['.env'],
    }),
    config,
    BuyTypeModule,
    CountriesModule,
    ProductModule,
    AuthModule,
    CategoryModule,
    Cartmodule,
    DescriptionModule,
    ReviewModule,
    OrderModule,
    RolesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
