import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reviews } from 'src/entities/reviews.entity';
import { ReviewController } from './review.controller';
import { ReviewService } from './review.service';

@Module({
  imports: [TypeOrmModule.forFeature([Reviews])],
  controllers: [ReviewController],
  providers: [ReviewService],
})
export class ReviewModule {}
