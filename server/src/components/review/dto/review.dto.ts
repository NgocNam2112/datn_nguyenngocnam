import { IsNumber, IsString } from 'class-validator';

export class ReviewDto {
  @IsNumber()
  userId: number;

  @IsString()
  content: string;

  @IsNumber()
  productId: number;
}
