import { AuthorizationGuard } from './../auth/guard/auth.guard';
import {
  Controller,
  Get,
  Body,
  Post,
  UseGuards,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { Reviews } from 'src/entities/reviews.entity';
import { ReviewService } from './review.service';
import { Auth } from '../auth/guard/auth.decorator';

interface INewReview {
  content: string;
  productId: number;
}

@Controller('reviews')
export class ReviewController {
  constructor(private reviewService: ReviewService) {}
  @Get()
  fetchAllReviews() {
    return this.reviewService.fetchAllReviews();
  }

  @Get('/:productId')
  fetchReviewsByProductId(@Param() param) {
    return this.reviewService.fetchReviewsByProductId(+param.productId);
  }

  //   input: content, productId
  @UseGuards(AuthorizationGuard)
  @Post('add-new-review')
  createNewReview(
    @Body() reviewInfo: INewReview,
    @Auth() authUsers: any,
  ): Promise<Reviews> {
    return this.reviewService.createNewReviews(reviewInfo, authUsers);
  }

  //   input: content
  @UseGuards(AuthorizationGuard)
  @Put('/update-view/:id')
  updateReiew(@Body() reviewInfo, @Auth() authUsers: any, @Param() param) {
    return this.reviewService.updateReiew(reviewInfo, authUsers, param.id);
  }

  @UseGuards(AuthorizationGuard)
  @Delete('/delete-review/:id')
  deleteReview(@Param() param) {
    return this.reviewService.deleteReview(+param.id);
  }
}
