import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { globalResponse } from 'src/config/response.config';
import { Reviews } from 'src/entities/reviews.entity';
import { ReviewDto } from './dto/review.dto';
import { createQueryBuilder, Repository } from 'typeorm';

@Injectable()
export class ReviewService {
  constructor(
    @InjectRepository(Reviews) private reviewRepository: Repository<Reviews>,
  ) {}

  private createEntity(req: ReviewDto, userId): Reviews {
    const review = new Reviews();
    review.content = req.content;
    review.userId = userId;
    review.productId = req.productId;
    return review;
  }

  async fetchAllReviews() {
    return await createQueryBuilder(Reviews, 'reviews')
      .leftJoin('reviews.user', 'users')
      .leftJoin('reviews.product', 'products')
      .select([
        'reviews.id',
        'reviews.content',
        'users.id',
        'users.avatar',
        'products.id',
      ])
      .getMany();
  }

  async fetchReviewsByProductId(productId) {
    return await createQueryBuilder(Reviews, 'reviews')
      .leftJoin('reviews.user', 'users')
      .leftJoin('reviews.product', 'products')
      .select([
        'reviews.id',
        'users.first_name',
        'users.last_name',
        'reviews.content',
        'users.id',
        'users.avatar',
        'products.id',
        'reviews.updated_at',
      ])
      .where('products.id = :productId', { productId: productId })
      .getMany();
  }

  async createNewReviews(reviewInfo, authUsers): Promise<Reviews> {
    const review = this.createEntity(reviewInfo, authUsers.id);
    return await this.reviewRepository.save(review);
  }

  async updateReiew(reviewInfo, authUsers, reviewId): Promise<Reviews> {
    const findReview = await this.reviewRepository.findOne({
      id: reviewId,
    });

    if (!findReview) {
      throw new BadRequestException(401, 'Invalid review');
    }

    if (
      authUsers.id !== findReview.userId ||
      authUsers.roles !== 2 ||
      authUsers.roles !== 1
    ) {
      throw new BadRequestException(401, 'Invalid authorization');
    }

    findReview.content = reviewInfo.content;
    return await this.reviewRepository.save(findReview);
  }

  async deleteReview(reviewId: number) {
    const review = await this.reviewRepository.findOne({ id: reviewId });
    if (!review) {
      throw new BadRequestException(401, 'Invalid review');
    }

    await this.reviewRepository.remove(review);

    return globalResponse(HttpStatus.OK, 'Delete review successfully.');
  }
}
