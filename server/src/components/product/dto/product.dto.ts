import { IsNotEmpty, IsString } from 'class-validator';
import { Column } from 'typeorm';

export class ProductDto {
  @IsNotEmpty()
  selling_price: number;

  @IsNotEmpty()
  discount: number;

  @IsNotEmpty()
  @IsString()
  space_for_discription: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  stock: number;

  @IsString()
  farm: string;

  @IsNotEmpty()
  @IsString()
  delivery_area: string;

  @Column()
  rate: number;

  @IsString()
  content: string;

  @IsNotEmpty()
  categoryId: number;
}
