import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { createQueryBuilder, getConnection, Repository } from 'typeorm';
import { Products } from 'src/entities/products.entity';
import { ProductDto } from './dto/product.dto';
import { globalResponse } from 'src/config/response.config';
import { Categories } from 'src/entities/category.entity';
import { CloudinaryService } from 'src/utils/cloudinary/cloudinary.service';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Products) private productRepository: Repository<Products>,
    @InjectRepository(Categories)
    private categoryRepository: Repository<Categories>,
    private cloudinary: CloudinaryService,
  ) {}

  private createEntity(
    req: ProductDto,
    category: Categories | undefined,
    resultUpload: any,
  ): Products {
    const product = new Products();
    product.image = resultUpload.url;
    product.name = req.name;
    product.selling_price = +req.selling_price;
    product.discount = +req.discount;
    product.space_for_discription = req.space_for_discription;
    product.stock = +req.stock;
    product.farm = req.farm;
    product.delivery_area = req.delivery_area;
    product.content = req.content;
    product.category = category;
    product.cloudinary_id = resultUpload.public_id;
    product.rate = +req.rate;
    return product;
  }

  private async getCategoryById(id: number): Promise<Categories> {
    const category = await this.categoryRepository.findOne({ id: id });
    if (!category) {
      return globalResponse(HttpStatus.FORBIDDEN, `No category by id: ${id}`);
    }
    return category;
  }

  private async uploadImageToCloudinary(file): Promise<any> {
    return await this.cloudinary.uploadImage(file).catch(() => {
      throw new BadRequestException('Invalid file type.');
    });
  }

  //Get all products
  async fetchAllProduct(): Promise<Products[]> {
    return await this.productRepository.find({
      select: [
        'id',
        'selling_price',
        'discount',
        'space_for_discription',
        'name',
        'stock',
        'farm',
        'delivery_area',
        'content',
        'categoryId',
        'image',
        'rate',
      ],
    });
  }

  async fetchProductByCategoryId(categoryId: number): Promise<Products[]> {
    return await this.productRepository.find({
      where: { categoryId: categoryId },
    });
  }

  //Get product for each id
  async fetchProductById(param): Promise<Products> {
    return await getConnection()
      .createQueryBuilder()
      .select('products')
      .from(Products, 'products')
      .where('products.id = :id', { id: param })
      .getOne();
  }

  //Create new product.
  async createNewProduct(product, file: Express.Multer.File) {
    const category = await this.getCategoryById(+product.categoryId);
    const result = await this.uploadImageToCloudinary(file);
    const productReq = this.createEntity(product, category, result);

    return await this.productRepository.save(productReq);
  }

  // Update product by id
  async updateProductById(param, body: ProductDto, file) {
    const productById = await this.productRepository.findOne({ id: param.id });
    if (!productById) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `Product not found with id: ${param.id}`,
      );
    }
    if (productById.cloudinary_id && file) {
      await this.cloudinary.destroyImage(productById.cloudinary_id);
    }

    if (file) {
      const result = await this.uploadImageToCloudinary(file);
      productById.image = result.url;
      productById.cloudinary_id = result.public_id;
    }
    productById.name = body.name;
    productById.selling_price = +body.selling_price;
    productById.discount = +body.discount;
    productById.space_for_discription = body.space_for_discription;
    productById.stock = +body.stock;
    productById.farm = body.farm;
    productById.delivery_area = body.delivery_area;
    productById.content = body.content;
    productById.rate = +body.rate;

    return await this.productRepository.save(productById);
  }

  async deleteProductById(param) {
    const productById = await this.productRepository.findOne({ id: param.id });
    if (!productById) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `Product not found with id: ${param.id}`,
      );
    }

    if (productById.cloudinary_id) {
      await this.cloudinary.destroyImage(productById.cloudinary_id);
    }
    await this.productRepository.remove(productById);
    return globalResponse(HttpStatus.OK, 'Delete success');
  }

  async searchProductByName(name) {
    const productList = await this.productRepository.find();
    return productList.filter((item) =>
      item.name.toLowerCase().includes(name.toLowerCase()),
    );
  }
}
