import { Module } from '@nestjs/common';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { Products } from 'src/entities/products.entity';
import { Categories } from 'src/entities/category.entity';
import { CloudinaryModule } from 'src/utils/cloudinary/cloudinary.module';

@Module({
  imports: [CloudinaryModule, TypeOrmModule.forFeature([Products, Categories])],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule {}
