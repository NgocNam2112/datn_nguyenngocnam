import {
  Controller,
  Get,
  Body,
  Post,
  Request,
  Param,
  Put,
  Delete,
  UseInterceptors,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Products } from 'src/entities/products.entity';
import { AuthorizationGuard } from '../auth/guard/auth.guard';
import { ProductDto } from './dto/product.dto';
import { ProductService } from './product.service';

@Controller('products')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Get()
  fetchAllProduct(): Promise<Products[]> {
    return this.productService.fetchAllProduct();
  }

  @Get('/:id')
  fetProductId(@Param() param): Promise<Products> {
    return this.productService.fetchProductById(param.id);
  }

  @Get('/by-category/:id')
  fetchProductByCategoryId(@Param() categoryId: number): Promise<Products[]> {
    return this.productService.fetchProductByCategoryId(categoryId);
  }

  @UseGuards(AuthorizationGuard)
  @Post('new-product')
  @UseInterceptors(FileInterceptor('files'))
  createNewProduct(
    @Request() req,
    @Body() product,
    @UploadedFile() files: Express.Multer.File,
  ) {
    return this.productService.createNewProduct(product, files);
  }

  @UseGuards(AuthorizationGuard)
  @Put('update-product/:id')
  @UseInterceptors(FileInterceptor('files'))
  updateProductById(
    @Param() param,
    @Body() productReq: ProductDto,
    @UploadedFile() files: Express.Multer.File,
  ) {
    return this.productService.updateProductById(param, productReq, files);
  }

  @UseGuards(AuthorizationGuard)
  @Delete('delete-product/:id')
  deleteProductById(@Param() param) {
    return this.productService.deleteProductById(param);
  }

  @Get('/search/:name')
  searchProductByName(@Param() name) {
    return this.productService.searchProductByName(name.name);
  }
}
