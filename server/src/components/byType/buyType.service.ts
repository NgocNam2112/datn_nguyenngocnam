import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BuyTypes } from 'src/entities/buy-type.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BuyTypeService {
  constructor(
    @InjectRepository(BuyTypes) private buyTypeRepository: Repository<BuyTypes>,
  ) {}

  async fetchAllBuyType(): Promise<BuyTypes[]> {
    return await this.buyTypeRepository.find();
  }
}
