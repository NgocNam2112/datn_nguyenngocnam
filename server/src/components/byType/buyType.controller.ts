import { Controller, Get } from '@nestjs/common';
import { BuyTypeService } from './buyType.service';

@Controller('buy-types')
export class BuyTypeController {
  constructor(private buyTypeService: BuyTypeService) {}

  @Get()
  fetchAllBuyType() {
    return this.buyTypeService.fetchAllBuyType();
  }
}
