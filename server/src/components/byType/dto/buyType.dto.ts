import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class BuyTypesDto {
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @IsNotEmpty()
  @IsString()
  unit_type: string;
}
