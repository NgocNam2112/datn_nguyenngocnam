import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BuyTypes } from 'src/entities/buy-type.entity';
import { BuyTypeController } from './buyType.controller';
import { BuyTypeService } from './buyType.service';

@Module({
  imports: [TypeOrmModule.forFeature([BuyTypes])],
  controllers: [BuyTypeController],
  providers: [BuyTypeService],
})
export class BuyTypeModule {}
