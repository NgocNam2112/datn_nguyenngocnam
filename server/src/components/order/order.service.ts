import {
  BadRequestException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { globalResponse } from 'src/config/response.config';
import { Cart } from 'src/entities/cart.entity';
import { OrderDetails } from 'src/entities/order-details.entity';
import { Order } from 'src/entities/order.entity';
import { Users } from 'src/entities/users.entity';
import { createQueryBuilder, Repository } from 'typeorm';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order) private orderRepository: Repository<Order>,
    @InjectRepository(OrderDetails)
    private orderDetailsRepository: Repository<OrderDetails>,
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    @InjectRepository(Users) private userRepository: Repository<Users>,
  ) {}

  async createOrder(authUsers) {
    // create new record in order table
    const order = new Order();
    order.userId = authUsers.id;
    const productInCart = await this.cartRepository.find({
      userId: authUsers.id,
    });
    if (!productInCart.length) {
      return { msg: 'No product in cart' };
    }
    // create new instance to get id of record order just create.
    const instanceOfOrder = await this.orderRepository.save(order);

    //loop to create many records in order-details table.
    productInCart.forEach(async (item) => {
      const orderDetail = new OrderDetails();
      orderDetail.productId = item.productId;
      orderDetail.orderId = instanceOfOrder.id;
      orderDetail.quantity = +item.quantity;
      orderDetail.buyTypesId = item.buyTypesId;
      await this.orderDetailsRepository.save(orderDetail);
    });

    // done order then delete all product in cart
    await createQueryBuilder(Cart, 'cart').delete().execute();
    return { orderId: instanceOfOrder.id };
  }

  async fetchOrderByUserId(authUsers): Promise<Order[]> {
    return await createQueryBuilder(Order, 'order')
      .leftJoinAndSelect('order.order_details', 'order_details')
      .where('order.userId = :userId', { userId: authUsers.id })
      .orderBy('created_at', 'DESC')
      .getMany();
  }

  async fetchAllOrder(authUsers) {
    if (authUsers.roles === 3) {
      new UnauthorizedException('Unauthorization to access this route');
    }
    return await createQueryBuilder(Order, 'order')
      .leftJoinAndSelect('order.order_details', 'order_details')
      .leftJoinAndSelect('order_details.product', 'products')
      .getMany();
  }

  async fetchOrderByUserIdAndOrderId(orderId, authUsers) {
    const order = await this.orderRepository.findOne({ id: orderId });
    if (!order) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `No order with id ${orderId}`,
      );
    }
    return await createQueryBuilder(Order, 'order')
      .leftJoinAndSelect('order.order_details', 'order_details')
      .leftJoinAndSelect('order_details.product', 'products')
      .where('order.userId = :userId', { userId: authUsers.id })
      .andWhere('order.id = :orderId', { orderId })
      .select([
        'order.id',
        'order.created_at',
        'order_details.quantity',
        'order_details.id',
        'order_details.buyTypesId',
        'products.id',
        'products.image',
        'products.discount',
        'products.name',
        'products.selling_price',
      ])
      .getOne();
  }

  async fetchOrderByAdmin() {
    return await createQueryBuilder(Order, 'order')
      .leftJoinAndSelect('order.user', 'users')
      .select([
        'order.id',
        'order.created_at',
        'users.id',
        'users.mail_address',
        'users.avatar',
        'users.first_name',
        'users.last_name',
        'users.phone_number',
        'users.address',
        'users.town',
      ])
      .getMany();
  }

  async fetchOrderIdByAdmin(userId, orderId) {
    const order = await createQueryBuilder(Order, 'order')
      .leftJoinAndSelect('order.user', 'users')
      .where('order.userId = :userId', { userId: userId })
      .andWhere('order.id = :orderId', { orderId });
    if (!order) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `No order with id ${orderId}`,
      );
    }
    return await createQueryBuilder(Order, 'order')
      .leftJoinAndSelect('order.user', 'users')
      .leftJoinAndSelect('order.order_details', 'order_details')
      .leftJoinAndSelect('order_details.product', 'products')
      .where('order.userId = :userId', { userId: userId })
      .andWhere('order.id = :orderId', { orderId })
      .select([
        'order.id',
        'order.created_at',
        'users.id',
        'users.mail_address',
        'users.avatar',
        'users.first_name',
        'users.last_name',
        'users.phone_number',
        'users.address',
        'users.town',
        'order_details.quantity',
        'order_details.id',
        'order_details.buyTypesId',
        'products.id',
        'products.image',
        'products.discount',
        'products.name',
        'products.selling_price',
      ])
      .getOne();
  }

  async deleteOrderIdByAdmin(userId, orderId) {
    const order = await createQueryBuilder(Order, 'order')
      .leftJoinAndSelect('order.user', 'users')
      .leftJoinAndSelect('order.order_details', 'order_details')
      .where('order.user.id = :userId', { userId: userId })
      .andWhere('order.id = :orderId', { orderId: orderId })
      .getOne();
    if (!order) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `No order with id ${orderId}`,
      );
    }
    await this.orderDetailsRepository.remove(order.order_details);
    await this.orderRepository.remove(order);
    return { msg: 'Delete success' };
  }
}
