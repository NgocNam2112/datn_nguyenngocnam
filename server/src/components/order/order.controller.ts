import {
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { Auth } from '../auth/guard/auth.decorator';
import { AuthorizationGuard } from '../auth/guard/auth.guard';
import { OrderService } from './order.service';

@Controller('orders')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @UseGuards(AuthorizationGuard)
  @Post('add-new-order')
  createOrder(@Auth() authUsers) {
    return this.orderService.createOrder(authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Get()
  fetchOrderByUserId(@Auth() authUsers) {
    return this.orderService.fetchOrderByUserId(authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Get('fetch-all')
  fetchAllOrder(@Auth() authUsers) {
    return this.orderService.fetchAllOrder(authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Get('fetch-newest-order/:orderId')
  fetchOrderByUserIdAndOrderId(@Param() orderId, @Auth() authUsers) {
    return this.orderService.fetchOrderByUserIdAndOrderId(
      +orderId.orderId,
      authUsers,
    );
  }

  @UseGuards(AuthorizationGuard)
  @Get('/fetch-order-by-admin')
  fetchOrderByAdmin() {
    return this.orderService.fetchOrderByAdmin();
  }

  @UseGuards(AuthorizationGuard)
  @Get('fetch-order-id-by-admin/:orderId/:userId')
  fetchOrderidByAdmin(@Param() params) {
    return this.orderService.fetchOrderIdByAdmin(params.userId, params.orderId);
  }

  @UseGuards(AuthorizationGuard)
  @Delete('/delete-order-by-admin/:orderId/:userId')
  DeleteOrderIdByAdmin(@Param() params) {
    return this.orderService.deleteOrderIdByAdmin(
      params.userId,
      params.orderId,
    );
  }
}
