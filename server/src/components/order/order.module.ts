import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cart } from 'src/entities/cart.entity';
import { OrderDetails } from 'src/entities/order-details.entity';
import { Order } from 'src/entities/order.entity';
import { Users } from 'src/entities/users.entity';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderDetails, Cart, Users])],
  controllers: [OrderController],
  providers: [OrderService],
})
export class OrderModule {}
