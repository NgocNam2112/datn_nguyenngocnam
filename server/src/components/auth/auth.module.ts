import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Roles } from 'src/entities/roles.entity';
import { Users } from 'src/entities/users.entity';
import { CloudinaryModule } from 'src/utils/cloudinary/cloudinary.module';
import { UserController } from './auth.controller';
import { AuthService } from './auth.service';
import { JsonwebTokenStrategy } from './strategies/auth.strategy';

@Module({
  imports: [CloudinaryModule, TypeOrmModule.forFeature([Users, Roles])],
  controllers: [UserController],
  providers: [AuthService, JsonwebTokenStrategy],
  exports: [AuthService],
})
export class AuthModule {}
