import { FileInterceptor } from '@nestjs/platform-express';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Users } from 'src/entities/users.entity';
import { AuthService } from './auth.service';
import { Request } from 'express';
import { AuthorizationGuard } from './guard/auth.guard';
import { Auth } from './guard/auth.decorator';

export interface ISignIn {
  mail_address: string;
  password: string;
}

export interface ISignInResult {
  accessToken: string;
}

@Controller('users')
export class UserController {
  constructor(private userService: AuthService) {}

  @UseGuards(AuthorizationGuard)
  @Get()
  async fetchAllUser(
    @Req() req: Request,
    @Auth() authUsers: any,
  ): Promise<Users[]> {
    return await this.userService.fetchAllUsers(req, authUsers);
  }

  @Post('register')
  @UseInterceptors(FileInterceptor('files'))
  async register(
    @Req() req: Request,
    @UploadedFile() files: Express.Multer.File,
    @Body() user,
  ) {
    return await this.userService.register(user, files);
  }

  @UseInterceptors(FileInterceptor('files'))
  @Post('sign-in')
  async signIn(
    @Req() req: Request,
    @Body() userInfo: ISignIn,
  ): Promise<ISignInResult> {
    return await this.userService.signIn(userInfo);
  }

  @UseGuards(AuthorizationGuard)
  @Post('change-password')
  async changePassword(@Body() userInfo, @Auth() authUsers) {
    return this.userService.changePassword(userInfo, authUsers);
  }

  @Post('sign-in-admin')
  async signInAdmin(
    @Req() req: Request,
    @Body() userInfo: ISignIn,
  ): Promise<ISignInResult> {
    return await this.userService.signInAmin(userInfo);
  }

  @Post('forgot-password')
  async forgotPassword(@Req() req: Request, @Body() mail_address) {
    return await this.userService.forgotPassword(mail_address.mail_address);
  }

  @Put('/reset-password/:otp')
  async resetPassword(@Body() body, @Param() param) {
    return this.userService.resetPassword(body.newPassword, param.otp);
  }

  @UseGuards(AuthorizationGuard)
  @Put('update-image')
  @UseInterceptors(FileInterceptor('files'))
  async updateUserImage(
    @UploadedFile() files: Express.Multer.File,
    @Auth() authUsers,
  ): Promise<Users> {
    return await this.userService.updateUser(files, authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Get('profile')
  getUserProfile(@Auth() authUsers) {
    return this.userService.getUserProfile(authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Put('update-profile-without-image')
  @UseInterceptors(FileInterceptor('files'))
  updateProfileWithoutImage(@Body() userProfile, @Auth() authUsers) {
    return this.userService.updateProfileWithoutImage(userProfile, authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Put('update-profile-with-image')
  @UseInterceptors(FileInterceptor('files'))
  updateProfileWithImage(
    @Body() userProfile,
    @Auth() authUsers,
    @UploadedFile() files: Express.Multer.File,
  ) {
    return this.userService.updateProfileWithImage(
      userProfile,
      authUsers,
      files,
    );
  }

  @UseGuards(AuthorizationGuard)
  @Get('fetch-user-by-id/:id')
  fetchUserById(@Param() userId, @Auth() authUsers) {
    return this.userService.fetchUserById(+userId.id, authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Put('update-user-by-id/:id')
  @UseInterceptors(FileInterceptor('files'))
  updateUserById(
    @Param() userId,
    @Auth() authUsers,
    @Body() userInfo,
    @UploadedFile() files: Express.Multer.File,
  ) {
    return this.userService.updateUserById(
      +userId.id,
      userInfo,
      authUsers,
      files,
    );
  }
  @UseGuards(AuthorizationGuard)
  @Post('create-new-user')
  @UseInterceptors(FileInterceptor('files'))
  createNewUserAdmin(
    @Body() userInfo,
    @Auth() authUsers,
    @UploadedFile() files: Express.Multer.File,
  ) {
    return this.userService.createNewUser(userInfo, authUsers, files);
  }

  @UseGuards(AuthorizationGuard)
  @Delete('delete-user/:id')
  deleteUser(@Param() userId, @Auth() authUsers) {
    return this.userService.deleteUser(userId.id, authUsers);
  }
}
