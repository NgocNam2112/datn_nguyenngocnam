import {
  Injectable,
  HttpStatus,
  UseGuards,
  UnauthorizedException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { globalResponse } from 'src/config/response.config';
import { Roles } from 'src/entities/roles.entity';
import { Users } from 'src/entities/users.entity';
import sendEmail from 'src/utils/sendOtp';
import { createQueryBuilder, Repository } from 'typeorm';
import { ISignIn, ISignInResult } from './auth.controller';
import { AuthDto } from './dto/auth.dto';
import * as bcrypt from 'bcryptjs';
import { AuthorizationGuard } from './guard/auth.guard';
import { CloudinaryService } from 'src/utils/cloudinary/cloudinary.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(Users) private userRepository: Repository<Users>,
    @InjectRepository(Roles) private roleRepository: Repository<Roles>,
    private cloudinary: CloudinaryService,
    private config: ConfigService,
  ) {}

  async validateUser(id: number): Promise<any> {
    const user = await this.userRepository.findOne(id);
    if (user) {
      return user;
    }
    return null;
  }

  private async uploadImageToCloudinary(file): Promise<any> {
    return await this.cloudinary.uploadImage(file).catch(() => {
      throw new BadRequestException('Invalid file type.');
    });
  }

  @UseGuards(AuthorizationGuard)
  async fetchAllUsers(req, user): Promise<Users[]> {
    if (user.rolesId === 3 || user.rolesId === 2) {
      throw new UnauthorizedException(401, 'Unauthorized access');
    }

    return await createQueryBuilder(Users, 'users')
      .leftJoinAndSelect('users.roles', 'roles')
      .where('users.rolesId =:rolesId', { rolesId: 2 })
      .orWhere('roles.id =:id', { id: 3 })
      .select([
        'users.id',
        'users.mail_address',
        'users.avatar',
        'users.first_name',
        'users.last_name',
        'users.phone_number',
        'users.address',
        'users.rolesId',
        'users.town',
      ])
      .getMany();
  }

  private async createEntity(req: AuthDto, resultUpload): Promise<Users> {
    const role = await this.roleRepository.findOne({ id: +req.roles });
    if (!role) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `No roles with id ${req.roles}`,
      );
    }
    const user = new Users();
    user.mail_address = req.mail_address;
    user.password = req.password;
    user.roles = role;
    user.avatar = resultUpload.url;
    user.cloudinary_id = resultUpload.public_id;
    return user;
  }

  async register(req, files): Promise<any> {
    const resultUpload = await this.uploadImageToCloudinary(files);
    const user = new Users();
    user.first_name = req.first_name;
    user.last_name = req.last_name;
    user.rolesId = req.rolesId;
    user.avatar = resultUpload.url;
    user.cloudinary_id = resultUpload.public_id;
    user.mail_address = req.mail_address;
    user.password = req.password;

    const createUser = await this.userRepository.save(user);
    const { token, token_expired } = await createUser.getRefreshToken();
    user.token = token;
    user.token_expired = token_expired;
    return {
      accessToken: token,
    };
  }

  async signIn(req: ISignIn): Promise<ISignInResult> {
    const user = await this.userRepository.findOne({
      mail_address: req.mail_address,
    });
    if (!user) {
      return globalResponse(HttpStatus.NOT_FOUND, 'Email is not exist!');
    }
    const passwordMatching = user.comparePassword(req.password);
    if (!passwordMatching) {
      return globalResponse(HttpStatus.BAD_REQUEST, 'Password is not matched');
    }
    const { token, token_expired } = await user.getRefreshToken();
    user.token = token;
    user.token_expired = token_expired;
    await this.userRepository.save(user);
    return {
      accessToken: token,
    };
  }

  async changePassword(userInfo, authUsers) {
    const hashPassword = await bcrypt.hashSync(
      userInfo.old_password,
      this.config.get('SALT_PASSWORD'),
    );
    if (authUsers.password !== hashPassword) {
      return globalResponse(HttpStatus.BAD_REQUEST, 'Password not matched');
    }
    authUsers.password = await bcrypt.hashSync(
      userInfo.new_password,
      this.config.get('SALT_PASSWORD'),
    );
    await this.userRepository.save(authUsers);
    return { msg: 'Change password succeed' };
  }

  async signInAmin(req: ISignIn): Promise<ISignInResult> {
    const user = await this.userRepository.findOne({
      mail_address: req.mail_address,
    });
    if (!user) {
      return globalResponse(HttpStatus.NOT_FOUND, 'Email is not exist!');
    }

    if (user.rolesId === 3) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        'You have no roles to access this route',
      );
    }
    const passwordMatching = user.comparePassword(req.password);
    if (!passwordMatching) {
      return globalResponse(HttpStatus.BAD_REQUEST, 'Password is not matched');
    }
    const { token, token_expired } = await user.getRefreshToken();
    user.token = token;
    user.token_expired = token_expired;
    await this.userRepository.save(user);
    return {
      accessToken: token,
    };
  }

  async forgotPassword(req: string) {
    const user = await this.userRepository.findOne({ mail_address: req });
    if (!user) {
      return globalResponse(HttpStatus.NOT_FOUND, 'Email is not exist');
    }

    const otpCode = Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;
    const otpCodeExpired = new Date(Date.now() + 10 * 60 * 1000);

    user.tmp_otp = otpCode.toString();
    user.otpExpired = otpCodeExpired;
    await this.userRepository.save(user);

    await sendEmail(user.mail_address, user.tmp_otp);
    return { msg: 'Otp was sent' };
  }

  async resetPassword(newPassword: string, otp: string) {
    const user = await this.userRepository.findOne({ tmp_otp: otp });

    if (!user) {
      return globalResponse(HttpStatus.NOT_FOUND, 'Email is not exist');
    }

    if (Date.now() > +user.otpExpired) {
      return globalResponse(
        HttpStatus.FORBIDDEN,
        'Otp was expired, please try again',
      );
    }
    user.password = bcrypt.hashSync(
      newPassword,
      this.config.get('SALT_PASSWORD'),
    );
    await this.userRepository.save(user);

    return { msg: 'Reset password succeed' };
  }

  async updateUser(files, authUsers): Promise<Users> {
    const resultUpload = await this.uploadImageToCloudinary(files);
    if (authUsers.cloudinary_id) {
      await this.cloudinary.destroyImage(authUsers.cloudinary_id);
    }
    authUsers.avatar = resultUpload.url;
    authUsers.cloudinary_id = resultUpload.public_id;
    return this.userRepository.save(authUsers);
  }

  getUserProfile(authUsers) {
    return {
      first_name: authUsers.first_name,
      last_name: authUsers.last_name,
      phone_number: authUsers.phone_number,
      address: authUsers.address,
      town: authUsers.town,
      avatar: authUsers.avatar,
      rolesId: authUsers.rolesId,
      isProfile: authUsers.isProfile,
    };
  }

  async updateProfileWithoutImage(userProfile, authUsers) {
    authUsers.first_name = userProfile.first_name;
    authUsers.last_name = userProfile.last_name;
    authUsers.phone_number = userProfile.phone_number;
    authUsers.town = userProfile.town;
    authUsers.address = userProfile.address;

    await this.userRepository.save(authUsers);

    return {
      first_name: authUsers.first_name,
      last_name: authUsers.last_name,
      phone_number: authUsers.phone_number,
      address: authUsers.address,
      town: authUsers.town,
      avatar: authUsers.avatar,
      roles: authUsers.roles,
      isProfile: authUsers.isProfile,
    };
  }

  async updateProfileWithImage(userProfile, authUsers, files) {
    const resultUpload = await this.uploadImageToCloudinary(files);
    if (authUsers.cloudinary_id) {
      await this.cloudinary.destroyImage(authUsers.cloudinary_id);
    }
    authUsers.avatar = resultUpload.url;
    authUsers.cloudinary_id = resultUpload.public_id;
    authUsers.first_name = userProfile.first_name;
    authUsers.last_name = userProfile.last_name;
    authUsers.phone_number = userProfile.phone_number;
    authUsers.town = userProfile.town;
    authUsers.address = userProfile.address;

    await this.userRepository.save(authUsers);

    return {
      avatar: authUsers.avatar,
      first_name: authUsers.first_name,
      last_name: authUsers.last_name,
      phone_number: authUsers.phone_number,
      address: authUsers.address,
      town: authUsers.town,
      roles: authUsers.roles,
      isProfile: authUsers.isProfile,
    };
  }

  async fetchUserById(id, authUsers) {
    if (authUsers.rolesId === 2 || authUsers.rolesId === 3) {
      return globalResponse(
        HttpStatus.BAD_REQUEST,
        'You have no roles to access this route',
      );
    }
    return await createQueryBuilder(Users, 'users')
      .leftJoinAndSelect('users.roles', 'roles')
      .where('users.id =:userId', { userId: id })
      .select([
        'users.id',
        'users.mail_address',
        'users.avatar',
        'users.first_name',
        'users.last_name',
        'users.address',
        'users.phone_number',
        'users.rolesId',
        'users.town',
      ])
      .getOne();
  }

  async updateUserById(id, userInfo, authUsers, files) {
    if (authUsers.rolesId === 2 || authUsers.rolesId === 3) {
      return globalResponse(
        HttpStatus.BAD_REQUEST,
        'You have no roles to access this route',
      );
    }
    const user = await this.userRepository.findOne({ id: id });
    if (!user) {
      globalResponse(HttpStatus.BAD_REQUEST, 'User is not exist');
    }

    user.first_name = userInfo.first_name;
    user.last_name = userInfo.last_name;
    user.town = userInfo.town;
    user.address = userInfo.address;
    user.phone_number = userInfo.phone_number;
    if (files) {
      await this.cloudinary.destroyImage(user.cloudinary_id);
      const resultUploadImage = await this.uploadImageToCloudinary(files);
      user.avatar = resultUploadImage.url;
      user.cloudinary_id = resultUploadImage.public_id;
    }
    return await this.userRepository.save(user);
  }

  async createNewUser(userInfo, authUsers, files): Promise<Users> {
    if (authUsers.rolesId === 2 || authUsers.rolesId === 3) {
      return globalResponse(
        HttpStatus.BAD_REQUEST,
        'You have no roles to access this route',
      );
    }
    const isExistingMail = await this.userRepository.findOne({
      mail_address: userInfo.mail_address,
    });
    if (isExistingMail) {
      return globalResponse(HttpStatus.BAD_REQUEST, 'Email is existing');
    }
    const resultUpload = await this.uploadImageToCloudinary(files);
    const user = new Users();
    user.first_name = userInfo.first_name;
    user.last_name = userInfo.last_name;
    user.phone_number = userInfo.phone_number;
    user.address = userInfo.address;
    user.town = userInfo.town;
    user.rolesId = +userInfo.rolesId;
    user.mail_address = userInfo.mail_address;
    user.password = userInfo.password;
    user.avatar = resultUpload.url;
    user.cloudinary_id = resultUpload.public_id;
    return await this.userRepository.save(user);
  }

  async deleteUser(userId, authUsers) {
    if (authUsers.rolesId === 2 || authUsers.rolesId === 3) {
      return globalResponse(
        HttpStatus.BAD_REQUEST,
        'You have no roles to access this route',
      );
    }
    const user = await this.userRepository.findOne({ id: +userId });

    if (!user) {
      return globalResponse(HttpStatus.BAD_REQUEST, 'Email is not existing');
    }

    if (user.cloudinary_id) {
      await this.cloudinary.destroyImage(user.cloudinary_id);
    }

    return await this.userRepository.remove(user);
  }
}
