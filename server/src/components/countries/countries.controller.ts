import { Controller, Get } from '@nestjs/common';
import { Countries } from 'src/entities/country.entity';
import { CountriesService } from './countries.service';

@Controller('countries')
export class CountriesController {
  constructor(private countriesService: CountriesService) {}

  @Get()
  async fetchAllCountries(): Promise<Countries[]> {
    return this.countriesService.fetchAllCountry();
  }
}
