import { AnyFilesInterceptor, FileInterceptor } from '@nestjs/platform-express';
import {
  Controller,
  Get,
  Param,
  Put,
  Body,
  UseInterceptors,
  UploadedFiles,
  UseGuards,
  Post,
  Delete,
} from '@nestjs/common';
import { DescriptionService } from './description.service';
import { AuthorizationGuard } from '../auth/guard/auth.guard';

@Controller('description')
export class DescriptionController {
  constructor(private descriptionService: DescriptionService) {}

  @Get('/:productId')
  getDesBuyProductId(@Param() productId) {
    return this.descriptionService.getDesBuyProductId(+productId.productId);
  }

  @UseGuards(AuthorizationGuard)
  @Put('/update-with-image/:id')
  @UseInterceptors(AnyFilesInterceptor())
  upateWithImage(
    @Param() productId,
    @Body() descriptionInfo,
    @UploadedFiles() files: Array<Express.Multer.File>,
  ) {
    return this.descriptionService.upateWithImage(
      +productId.id,
      descriptionInfo,
      files,
    );
  }

  @UseGuards(AuthorizationGuard)
  @Post('add-new-description')
  @UseInterceptors(AnyFilesInterceptor())
  addNewDescription(
    @Body() descriptionInfo,
    @UploadedFiles() files: Array<Express.Multer.File>,
  ) {
    return this.descriptionService.addNewDescription(descriptionInfo, files);
  }

  @UseGuards(AuthorizationGuard)
  @Delete('delete-description/:productId')
  deleteDescription(@Param() id) {
    return this.descriptionService.deleteDescription(+id.productId);
  }
}
