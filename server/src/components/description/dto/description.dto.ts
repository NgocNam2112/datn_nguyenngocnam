import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class DescriptionDto {
  @IsString()
  origin: string;

  @IsString()
  vitamins: string;

  @IsString()
  cook: string;

  @IsString()
  first_image: string;

  @IsString()
  second_image: string;
}
