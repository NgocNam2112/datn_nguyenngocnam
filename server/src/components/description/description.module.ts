import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Descriptions } from 'src/entities/description.entity';
import { CloudinaryModule } from 'src/utils/cloudinary/cloudinary.module';
import { DescriptionController } from './description.controller';
import { DescriptionService } from './description.service';

@Module({
  imports: [CloudinaryModule, TypeOrmModule.forFeature([Descriptions])],
  controllers: [DescriptionController],
  providers: [DescriptionService],
})
export class DescriptionModule {}
