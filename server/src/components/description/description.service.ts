import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { globalResponse } from 'src/config/response.config';
import { Descriptions } from 'src/entities/description.entity';
import { Repository } from 'typeorm';
import { CloudinaryService } from 'src/utils/cloudinary/cloudinary.service';
import 'reflect-metadata';

@Injectable()
export class DescriptionService {
  constructor(
    @InjectRepository(Descriptions)
    private desRepository: Repository<Descriptions>,
    private cloudinary: CloudinaryService,
  ) {}

  private async uploadImageToCloudinary(file): Promise<any> {
    return await this.cloudinary.uploadImage(file).catch(() => {
      throw new BadRequestException('Invalid file type.');
    });
  }

  async getDesBuyProductId(productId: number) {
    const des = await this.desRepository.findOne({ productId: productId });

    if (!des) {
      return globalResponse(HttpStatus.NOT_FOUND, `Description not found`);
    }

    return des;
  }

  async upateWithImage(productId: number, descriptionInfo, files) {
    // search description by productId if exist
    const description = await this.desRepository.findOne({
      productId: productId,
    });
    if (!description) {
      return globalResponse(HttpStatus.NOT_FOUND, `Description not found`);
    }

    // upload image to cloudinary
    if (files.length) {
      const resultUploadFirstImage = await this.uploadImageToCloudinary(
        files[0],
      );
      const resultUploadSecondImage = await this.uploadImageToCloudinary(
        files[1],
      );

      description.first_image = resultUploadFirstImage.url;
      description.second_image = resultUploadSecondImage.url;
    }

    //destroy image on cloudinary
    if (description.first_image_cloudinary_id && files.length) {
      await this.cloudinary.destroyImage(description.first_image_cloudinary_id);
    }
    if (description.second_image_cloudinary_id && files.length) {
      await this.cloudinary.destroyImage(
        description.second_image_cloudinary_id,
      );
    }

    description.origin = descriptionInfo.origin;
    description.cook = descriptionInfo.cook;
    description.vitamins = descriptionInfo.vitamins;

    return await this.desRepository.save(description);
  }

  async updateWithoutImage(productId, descriptionInfo) {
    const description = await this.desRepository.findOne({
      productId: productId,
    });

    if (!description) {
      return globalResponse(HttpStatus.NOT_FOUND, `Description not found`);
    }

    description.origin = descriptionInfo.origin;
    description.cook = descriptionInfo.cook;
    description.vitamins = descriptionInfo.vitamins;

    return await this.desRepository.save(description);
  }

  async addNewDescription(descriptionInfo, files) {
    // upload image to cloudinary
    const resultUploadFirstImage = await this.uploadImageToCloudinary(files[0]);
    const resultUploadSecondImage = await this.uploadImageToCloudinary(
      files[1],
    );

    const description = new Descriptions();

    description.productId = +descriptionInfo.productId;
    description.first_image = resultUploadFirstImage.url;
    description.second_image = resultUploadSecondImage.url;
    description.origin = descriptionInfo.origin;
    description.cook = descriptionInfo.cook;
    description.vitamins = descriptionInfo.vitamins;
    description.first_image_cloudinary_id = resultUploadFirstImage.public_id;
    description.second_image_cloudinary_id = resultUploadSecondImage.public_id;

    return await this.desRepository.save(description);
  }

  async deleteDescription(id) {
    const descriptionExist = await this.desRepository.findOne({
      productId: id,
    });
    if (!descriptionExist) {
      return globalResponse(HttpStatus.NOT_FOUND, `Description not found`);
    }
    if (descriptionExist.first_image_cloudinary_id) {
      await this.cloudinary.destroyImage(
        descriptionExist.first_image_cloudinary_id,
      );
    }
    if (descriptionExist.second_image_cloudinary_id) {
      await this.cloudinary.destroyImage(
        descriptionExist.second_image_cloudinary_id,
      );
    }

    return await this.desRepository.remove(descriptionExist);
  }
}
