import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CartDto {
  @IsNumber()
  productId: number;

  @IsNotEmpty()
  @IsNumber()
  quantity: number;

  @IsNotEmpty()
  @IsNumber()
  buyTypeId: number;
}
