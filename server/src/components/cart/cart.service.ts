import { InjectRepository } from '@nestjs/typeorm';
import { BadRequestException, Injectable } from '@nestjs/common';
import { Cart } from 'src/entities/cart.entity';
import { createQueryBuilder, Repository } from 'typeorm';
import { Products } from 'src/entities/products.entity';

@Injectable()
export class CartService {
  constructor(
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    @InjectRepository(Products)
    private productRepository: Repository<Products>,
  ) {}

  private createEntity(req, authUsers): Cart {
    const cart = new Cart();
    cart.userId = authUsers.id;
    cart.productId = req.productId;
    cart.buyTypesId = req.buyTypeId;
    cart.quantity = req.quantity;

    return cart;
  }

  async fetchAllProductInCart(authUsers) {
    const cartList = await createQueryBuilder(Cart, 'cart')
      .leftJoinAndSelect('cart.product', 'products')
      .where('cart.userId = :userId', { userId: authUsers.id })
      .getMany();
    return cartList;
  }

  async addProductToCart(product, authUser): Promise<Cart> {
    const productExist = await this.productRepository.findOne({
      id: +product.productId,
    });
    if (!productExist) {
      throw new BadRequestException(401, 'Product not found');
    }

    if (+product.quantity > +productExist.stock) {
      throw new BadRequestException(401, 'Quantity exceeds stock');
    }

    const productExistOnCart = await createQueryBuilder(Cart, 'cart')
      .where('cart.userId = :userId', { userId: +authUser.id })
      .andWhere('cart.productId = :productId', {
        productId: +product.productId,
      })
      .getOne();

    if (productExistOnCart) {
      productExist.stock += productExistOnCart.quantity - product.quantity;
      productExistOnCart.quantity = product.quantity;
      await this.productRepository.save(productExist);
      return await this.cartRepository.save(productExistOnCart);
    }

    const cart = this.createEntity(product, authUser);
    productExist.stock = productExist.stock - product.quantity;
    await this.productRepository.save(productExist);

    return await this.cartRepository.save(cart);
  }

  async deleteCartItem(param, authUsers) {
    const productExistOnCart = await createQueryBuilder(Cart, 'cart')
      .where('cart.userId = :userId', { userId: +authUsers.id })
      .andWhere('cart.productId = :productId', {
        productId: param,
      })
      .getOne();

    if (!productExistOnCart) {
      throw new BadRequestException(401, 'Product not found');
    }
    await this.cartRepository.remove(productExistOnCart);

    return {
      msg: 'Remove product success',
    };
  }
}
