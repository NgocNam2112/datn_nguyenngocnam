import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { Cart } from 'src/entities/cart.entity';
import { Auth } from '../auth/guard/auth.decorator';
import { AuthorizationGuard } from '../auth/guard/auth.guard';
import { CartService } from './cart.service';

interface IProduct {
  productId: number;
  quantity: number;
  buyTypeId: number;
}

@Controller('cart')
export class CartController {
  constructor(private cartService: CartService) {}

  @UseGuards(AuthorizationGuard)
  @Get()
  async fetchAllProductInCart(@Auth() authUsers) {
    return await this.cartService.fetchAllProductInCart(authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Post('add-to-cart')
  async addToCart(@Body() product: IProduct, @Auth() authUsers): Promise<Cart> {
    return await this.cartService.addProductToCart(product, authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Delete('/delete-cart-item/:productId')
  deleteCartItem(@Param() param, @Auth() authUsers) {
    return this.cartService.deleteCartItem(+param.productId, authUsers);
  }
}
