import { Categories } from 'src/entities/category.entity';
import { HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CategoryDto } from './dto/category.dto';
import { globalResponse } from 'src/config/response.config';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Categories)
    private categoryRepository: Repository<Categories>,
  ) {}

  private createEntity(req: CategoryDto): Categories {
    const category = new Categories();
    category.category_name = req.category_name;
    return category;
  }

  private guardUser(authUser) {
    if (authUser.rolesId === 3) {
      throw new UnauthorizedException(
        401,
        'You are not allowed to access this route',
      );
    }
  }

  async fetchAllCategory(req): Promise<Categories[]> {
    return await this.categoryRepository.find();
  }

  async fetchCategoryById(param): Promise<Categories> {
    return await this.categoryRepository.findOne({ where: { id: param.id } });
  }

  async createNewCategory(categoryInfo, authUsers): Promise<Categories> {
    this.guardUser(authUsers);
    const category = this.createEntity(categoryInfo);
    return await this.categoryRepository.save(category);
  }

  async updateCategory(categoryId, categoryInfo, authUsers) {
    this.guardUser(authUsers);
    const category = this.createEntity(categoryInfo);
    const getCategoryById = await this.categoryRepository.findOne({
      id: categoryId,
    });
    if (!getCategoryById) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `No category with id: ${categoryId}`,
      );
    }
    return await this.categoryRepository.save({
      ...getCategoryById,
      ...category,
    });
  }

  async deleteCategory(categoryId, authUsers) {
    this.guardUser(authUsers);
    const categoryById = await this.categoryRepository.findOne({
      id: categoryId,
    });
    if (!categoryById) {
      return globalResponse(
        HttpStatus.NOT_FOUND,
        `No category with id: ${categoryId}`,
      );
    }
    await this.categoryRepository.remove(categoryById);
    return globalResponse(HttpStatus.OK, 'Delete success');
  }
}
