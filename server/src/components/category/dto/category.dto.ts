import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CategoryDto {
  @IsNumber()
  id: number;

  @IsNotEmpty()
  @IsString()
  category_name: string;
}
