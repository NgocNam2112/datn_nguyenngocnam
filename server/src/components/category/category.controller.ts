import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { Auth } from '../auth/guard/auth.decorator';
import { AuthorizationGuard } from '../auth/guard/auth.guard';
import { CategoryService } from './category.service';
@Controller('categories')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Get()
  fetchAllCategory(@Req() req: Request) {
    return this.categoryService.fetchAllCategory(req);
  }

  @Get('/:id')
  fetchCategoryById(@Param() param) {
    return this.categoryService.fetchCategoryById(param);
  }

  @UseGuards(AuthorizationGuard)
  @Post('new-category')
  createNewCategory(@Body() categoryInfo, @Auth() authUsers: any) {
    return this.categoryService.createNewCategory(categoryInfo, authUsers);
  }

  @UseGuards(AuthorizationGuard)
  @Put('update-category/:id')
  updateCategory(
    @Param() categoryId,
    @Body() categoryInfo,
    @Auth() authUsers: any,
  ) {
    return this.categoryService.updateCategory(
      +categoryId.id,
      categoryInfo,
      authUsers,
    );
  }

  @UseGuards(AuthorizationGuard)
  @Delete('delete-category/:id')
  deleteCategory(@Param() categoryId, @Auth() authUsers: any) {
    return this.categoryService.deleteCategory(+categoryId.id, authUsers);
  }
}
