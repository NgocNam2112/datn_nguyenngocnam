// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
import * as nodemailer from 'nodemailer';

const sendEmail = async (email, otp) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    service: 'gmail',
    secure: true,
    auth: {
      user: process.env.SEND_MAIL_USER,
      pass: process.env.SEND_MAIL_PASSWORD,
    },
  });

  const options = {
    from: 'admin@admin.com',
    to: email,
    subject: 'Otp reset password',
    text: `
    <h2>Request forgot password</h2>
    <p>Otp for reset password: ${otp}</p>`,
  };
  await transporter.sendMail(options, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent' + info.response);
    }
  });
};

export default sendEmail;
